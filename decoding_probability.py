import numpy as np
from matplotlib import pyplot as plt

"""
Here we look at the plots of successful decoding probability vs quantity eta (not to be confused with spontaneous 
emission cross loss parameter eta). 
"""

def decode_probability(branching_vector, eta_d, L, L_att=20, direct=False, n=1):

	if not len(branching_vector) == 3:
		raise ValueError("We are only looking at trees with 3 levels.")


	if not direct:
		b0, b1, b2 = branching_vector

		eta = np.exp(-L/L_att)
		print(f'DEBUG: eta {eta} and  etd {eta_d}')
		mu = 1 - eta*eta_d
		R1 = 1-(1-(1-mu)*(1-mu)**b2)**b1
		R2 = 1-(1-(1-mu))**b2
		eta_e = ((1-mu+mu*R1)**b0 - (mu*R1)**b0)*(1-mu + mu*R2)**b1
		print(f'Debugging: mu is {mu}, R1 is {R1}, R2 is {R2}')
		print(f'Left bracket in the eta e expression is {((1-mu+mu*R1)**b0 - (mu*R1)**b0)}, right bracket {(1-mu + mu*R2)}')

		return eta_e, eta

	else:
		eta = np.exp(-L / L_att)
		eta_dir = 1-(1-eta_d*eta)**n
		return eta_dir, eta


# l_list = np.arange(100,1100,100)
l_list = np.arange(0,11,0.5)
etad = 0.95

# a = decode_probability([2,3,2], etad, 100)
# b = decode_probability([4,14,4], etad, 100)
# print(f'Are they of similar order of magnitude? {a[0], b[0]}')


# Plot direct transmission line
plt.figure(figsize=(10,6))
direct_decode_probs, eta = decode_probability([1,1,1], etad, l_list, direct=True)
plt.plot(eta, direct_decode_probs, label='Direct Transmission')

# Now plot different branching vectors:
# vectors = [[2,3,2], [2,8,4], [3,15,4], [3,12,4], [3,10,4], [4,14,4], [4,12,4]]
vectors = [[2,3,2], [3,10,4], [3,15,4], [4,14,4]]
# vectors = [[2,2,2], [2,3,2]]
# vectors = [[3,15,4], [3,12,4], [3,10,4]]
# vectors = [[4,14,4], [4,12,4], [4,11,4]]
for tree in range(len(vectors)):
	decode_prob, eta = decode_probability(vectors[tree], etad, l_list)
	# print(f'Decoding probabilities {decode_prob} compared to the direct one {direct_decode_probs}')
	plt.plot(eta, decode_prob, label=str(vectors[tree]))

# plt.yscale('log')
# plt.xscale('log')
# plt.title('Theory Branching Vector Curves')
plt.grid()
plt.xlabel("Transmission probability (1 - Epsilon0)")
plt.ylabel("Decoding probability")
# plt.legend(loc='best', bbox_to_anchor=(0.5, -0.235, 0.5, 0.5))
plt.show()
