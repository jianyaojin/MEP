
# from PTCS_Monte_Carlo_Last_Decode import generate_tree
# from PTCS_Monte_Carlo_Last_Decode import encode_decode
# from PTCS_Monte_Carlo_Last_Decode import gate_coefficients
from PTCS_Monte_Carlo_5qubit import generate_tree
from PTCS_Monte_Carlo_5qubit import encode_decode
from PTCS_Monte_Carlo_5qubit import gate_coefficients
# from PTCS_Monte_Carlo_5qubit_dumb import generate_tree
# from PTCS_Monte_Carlo_5qubit_dumb import encode_decode
# from PTCS_Monte_Carlo_5qubit_dumb import gate_coefficients
import multiprocessing as mp
import time
import numpy as np
from matplotlib import pyplot as plt
from qutip import *
import random
from qutip.measurement import measure, measurement_statistics
from collections import Counter as Majoritycount


def monte_carlo(N, coefficients, branching_vector, msg_qubit, q):
	print(f'this worker has {N} iterations')
	a = []
	for i in range(N):
		processed_tree_state, l1measurements, l2measurements = generate_tree(branching_vector, coefficients)
		# print(f'Reached the end of gen_tree ------------------------------------------------------------------------')
		resukt = encode_decode(processed_tree_state, l1measurements, l2measurements, msg_qubit, coefficients)
		# print(
			# f'Reached the end of encode_decode ------------------------------------------------------------------------')
		# print(f'VVVVVVVVVVVVVVVVVVVVVVVVVVV')
		# print(f'Results are: {resukt}')
		# print(f'^^^^^^^^^^^^^^^^^^^^^^^^^^^')
		q.put(resukt, block=False)
		# print(f'Queue size is {q.qsize()}')
		# a.append(resukt)

	# return a

# Multiprocessing

def run_multiprocess(M, coefficients, branching_vector, msg_qubit):
	processes = mp.cpu_count()
	mp_queue = mp.Queue()
	workers = []
	for process in range(processes):
		workers.append(mp.Process(target=monte_carlo, args=(M, coefficients, branching_vector, msg_qubit, mp_queue)))
	print(f'Starting {processes} workers')
	# print(f'In total we should have {processes*M} results')
	for worker in workers:
		worker.start()
	print(f'All workers started')
	outputs = []
	# time.sleep(10)
	for i in range(M*processes):
		# print(f'Saving process {i}')
		outputs.append(mp_queue.get())
	# print(f'All outputs saved')
	for worker in workers:
		# outputs.append(mp_queue.get())
		worker.join()
	return outputs, processes



#TODO Investigate Apply_Async() and https://www.digitalocean.com/community/tutorials/python-multiprocessing-example
#TODO and https://stackoverflow.com/questions/10415028/how-to-get-the-return-value-of-a-function-passed-to-multiprocessing-process
#TODO Siddhant code: https://codefile.io/f/2d53G3Br4N2u90cuT9fK



if __name__ == '__main__':

	# Define array of parameters to try
	# egtl_array = np.arange(0, 0.35, 0.01)
	# prob_rrtl = 0

	# prob_egtl = 0
	# rrtl_array = np.arange(0, 0.35, 0.01)
	# rrtl_array = [0.1]

	sds = np.logspace(-2,0.5,num=20)/4


	res = []
	starttime = time.time()
	for errorsd in sds:
	# for prob_egtl in egtl_array:
	# for prob_rrtl in rrtl_array:
		print(f'Started for errorsd {errorsd}')
		dont_include_error = False
		dont_include_loss = False
		#
		# # Given in order of: omega_d/gamma, delta1/gamma, cooperativity, kappa/gamma, kappa alpha / kappa, Delta / gamma
		sysconsts = [119.0, -74.0, 100, 50, 0.98, 20]
		#
		# Error categories split into three: Emitter error, Single Qubit Gate Errors, Multi Qubit Gate Errors, Nuclear dephasing error
		error_emitter = errorsd
		error_single = errorsd
		error_multi = errorsd
		n_dephasing_sd = errorsd

		# Additional gate parameters
		# r_0, r_1, l_0, l_1 = gate_coefficients(sysconsts, dont_include_loss)
		r_0, r_1, l_0, l_1 = -1, 1, 0, 0

		# Scattering parameters
		prob_egtl = 0
		prob_rrtl = 0.2
		cross_loss_rate = 0.1

		# Detection efficiency
		etad = 1.0

		# Coefficients are: r0, r1, l0, l1, pegtl, prrtl, eta (cross loss parameter) and error categories as above.
		coeffs = [r_0, r_1, l_0, l_1, prob_egtl, prob_rrtl, cross_loss_rate, error_emitter, error_single, error_multi,
				  n_dephasing_sd, etad]
		# # print(f'These are our r0: {r_0} and r1: {r_1}')
		#
		# # Choice of message qubit
		message_qubit = (basis(2, 0) + basis(2, 1)).unit()

		# Number of runs
		M = 20

		# Tree
		tree = [2,3,2]

		# Make the runs
		time1 = time.time()
		multiprocessed, cpus = run_multiprocess(M, coeffs, tree, message_qubit)
		results = np.array(multiprocessed)
		print(f'output of multiprocessing is: {results}')
		runtime = time.time()-time1
		print(f'Running {M} processes took {runtime} seconds')

		mask = np.invert(np.isnan(np.array(results,dtype=float)))
		nonnan = results[mask]

		print(f'Nonnan results: {nonnan}')
		print(f'Full results: {results}')
		# print(f'The shapes of the fidelity results array and the array with nans removed: {fidelity_results.shape[0]}, {nonnan.shape[0]}')

		transmission_success_rate = (1-prob_egtl)*(np.abs(r_0))*(1-prob_rrtl) * etad
		# Output format: Average fidelity, fidelity standard deviation, success probability, transmission success rate, number of runs total, cpus used, runtime per set of parameters
		res.append([np.mean(nonnan), np.std(nonnan), nonnan.shape[0] / results.shape[0], transmission_success_rate, int(results.shape[0]), cpus, runtime])

	res.append([time.time()-starttime,232,r_0,r_1,0,0,0])
	print(f'Reached the end')
	# np.savetxt("results_2,3,2_5qshuffle_errors_perfect.csv", np.array(res), delimiter=",")

	# np.savetxt("result.csv", np.array([[1,2,3],[1,3,15]]), delimiter=',')
	# np.savetxt('output.csv', results, delimiter=',', fmt='%f')
