
# from PTCS_Monte_Carlo_Last_Decode import generate_tree
# from PTCS_Monte_Carlo_Last_Decode import encode_decode
# from PTCS_Monte_Carlo_Last_Decode import gate_coefficients
# from PTCS_Monte_Carlo_5qubit import generate_tree
# from PTCS_Monte_Carlo_5qubit import encode_decode
# from PTCS_Monte_Carlo_5qubit import gate_coefficients
from PTCS_Monte_Carlo_5qubit_dumb import generate_tree
from PTCS_Monte_Carlo_5qubit_dumb import encode_decode
from PTCS_Monte_Carlo_5qubit_dumb import gate_coefficients
import multiprocessing as mp
import time
import numpy as np
from matplotlib import pyplot as plt
from qutip import *
import random
from qutip.measurement import measure, measurement_statistics
from collections import Counter as Majoritycount


def monte_carlo(N, coefficients, branching_vector, msg_qubit, q):
	print(f'this worker has {N} iterations')
	a = []

	Nm = 0
	Ns = 0
	Ndm = 0
	Nds = 0

	Nc = 0
	Na = 0
	Ndc = 0
	Nda = 0

	for i in range(N):
		processed_tree_state, l1measurements, l2measurements, debugflags = generate_tree(branching_vector, coefficients)
		resukt, debugarr = encode_decode(processed_tree_state, l1measurements, l2measurements, msg_qubit,coefficients)

		if debugarr[0]:
			Nc += 1
		if debugarr[1]:
			Na += 1

		Ndc += debugarr[2]
		Nda += debugarr[3]

		if debugflags[0]:
			Nm += 1
		if debugflags[1]:
			Ns += 1

		Ndm += debugflags[2]
		Nds += debugflags[3]

		q.put(resukt)
		a.append(resukt)


	# r_0, r_1 = coefficients[0], coefficients[1]
	# prob_egtl, prob_rrtl = coefficients[4], coefficients[5]
	#
	# mask = np.invert(np.isnan(np.array(a)))
	# nonnan = np.array(a)[mask]
	# eps0 = 1 - ((1 - prob_egtl) * (np.abs(r_0)) * (1 - prob_rrtl))
	# print(
	# 	f'The theoretical value for successful indirect aux Z measurement is given by: {1 - (1 - (1 - eps0) * (1 - eps0) ** 2) ** 3}')
	# print(
	# 	f'DEBUGGING[][][[][][][][[][][][][] The probability for successful indirect aux Z measurement is given by: {Ns / Nm}')
	#
	# print(
	# 	f'The theoretical value for successful indirect decode Z measurement is given by: {1 - (eps0) ** 2}')
	# print(
	# 	f'DEBUGGING[][][[][][][][[][][][][] The probability for successful indirect decode Z measurement is given by: {Nds / Ndm}')
	#
	# print(
	# 	f'The theoretical value for successful Z measurement on an aux level 1 photon is given by: {1 - eps0 + eps0 * (1 - (1 - (1 - eps0) * (1 - eps0) ** 2) ** 3)}')
	# print(
	# 	f'We find that our simulation gives the probability for such a successful Z measurement on l1 aux photon to be {Na / Nc}')
	#
	# print(
	# 	f'The theoretical value for successful Z measurement on a decoding level 2 photon is given by: {1 - eps0 + eps0 * (1 - (eps0) ** 2)}')
	# print(
	# 	f'We find that our simulation gives the probability for such a successful Z measurement on l2 decode photon to be {Nda / Ndc}')
	#
	# R1 = 1 - (1 - (1 - eps0) * (1 - eps0) ** 2) ** 3
	# R2 = 1 - (eps0) ** 2
	# print(
	# 	f'Overall we predict a decoding probability of {((1 - eps0 + eps0 * R1) ** 2 - (eps0 * R1) ** 2) * (1 - eps0 + eps0 * R2) ** 3}')
	# print(
	# 	f'What we find from the simulation is a decoding probability of {nonnan.shape[0] / np.array(a).shape[0]}')

	return a
# Multiprocessing

def run_multiprocess(M, coefficients, branching_vector, msg_qubit):
	processes = mp.cpu_count()
	mp_queue = mp.Queue()
	workers = []
	for process in range(processes):
		workers.append(mp.Process(target=monte_carlo, args=(M, coefficients, branching_vector, msg_qubit, mp_queue)))
	print(f'Starting {processes} workers')
	print(f'In total we should have {processes*M} results')
	for worker in workers:
		worker.start()
	print(f'All workers started')
	outputs = []
	for i in range(M*processes):
		outputs.append(mp_queue.get())

	for worker in workers:
		# outputs.append(mp_queue.get())
		worker.join()

	print(f'---------------------------- Outputs {len(outputs)} ')
	return outputs, processes



#TODO Investigate Apply_Async() and https://www.digitalocean.com/community/tutorials/python-multiprocessing-example
#TODO and https://stackoverflow.com/questions/10415028/how-to-get-the-return-value-of-a-function-passed-to-multiprocessing-process
#TODO Siddhant code: https://codefile.io/f/2d53G3Br4N2u90cuT9fK



if __name__ == '__main__':

	# Define array of parameters to try
	# egtl_array = np.arange(0, 0.35, 0.01)
	# prob_rrtl = 0

	prob_egtl = 0
	# rrtl_array = np.arange(0, 0.35, 0.01)
	rrtl_array = [0.2]


	res = []
	starttime = time.time()
	for prob_rrtl in rrtl_array:
		dont_include_error = False
		dont_include_loss = False
		#
		# # Given in order of: omega_d/gamma, delta1/gamma, cooperativity, kappa/gamma, kappa alpha / kappa, Delta / gamma
		sysconsts = [119.0, -74.0, 100, 50, 0.95, 20]
		#
		# Error categories split into three: Emitter error, Single Qubit Gate Errors, Multi Qubit Gate Errors, Nuclear dephasing error
		error_emitter = 0.1
		error_single = 0.1
		error_multi = 0.1
		n_dephasing_sd = 0.1

		# Additional gate parameters
		# r_0, r_1, l_0, l_1 = gate_coefficients(sysconsts, dont_include_loss)
		r_0, r_1, l_0, l_1 = -1, 1, 0, 0

		# Scattering parameters
		# prob_egtl = 0.2
		# prob_rrtl = 0.2
		cross_loss_rate = 0.1

		# Detection efficiency
		etad = 1.0

		# Coefficients are: r0, r1, l0, l1, pegtl, prrtl, eta (cross loss parameter) and error categories as above.
		coeffs = [r_0, r_1, l_0, l_1, prob_egtl, prob_rrtl, cross_loss_rate, error_emitter, error_single, error_multi,
				  n_dephasing_sd, etad]
		# # print(f'These are our r0: {r_0} and r1: {r_1}')
		#
		# # Choice of message qubit
		message_qubit = (basis(2, 0) + basis(2, 1)).unit()

		# Number of runs
		M = 250

		# Tree
		tree = [2,3,2]

		# Make the runs
		time1 = time.time()
		multiprocessed, cpus = run_multiprocess(M, coeffs, tree, message_qubit)
		results = np.array(multiprocessed)
		print(f'output of multiprocessing is: {results}')
		runtime = time.time()-time1
		print(f'Running {M} processes took {runtime} seconds')

		mask = np.invert(np.isnan(np.array(results,dtype=float)))
		nonnan = results[mask]


		print(f'Nonnan results: {nonnan}')
		print(f'Full results: {results}')

		eps0 = 1 - ((1 - prob_egtl) * (np.abs(r_0)) * (1 - 0.2))
		R1 = 1 - (1 - (1 - eps0) * (1 - eps0) ** 2) ** 3
		R2 = 1 - (eps0) ** 2
		print(
			f'Overall we predict a decoding probability of {((1 - eps0 + eps0 * R1) ** 2 - (eps0 * R1) ** 2) * (1 - eps0 + eps0 * R2) ** 3}')
		print(
			f'What we find from the simulation is a decoding probability of {nonnan.shape[0] / np.array(results,dtype=float).shape[0]}')


		# print(f'The shapes of the fidelity results array and the array with nans removed: {fidelity_results.shape[0]}, {nonnan.shape[0]}')

		transmission_success_rate = (1-prob_egtl)*(np.abs(r_0))*(1-prob_rrtl) * etad
		# Output format: Average fidelity, fidelity standard deviation, success probability, transmission success rate, number of runs total, cpus used, runtime per set of parameters
		res.append([np.mean(nonnan), np.std(nonnan), nonnan.shape[0] / results.shape[0], transmission_success_rate, int(results.shape[0]), cpus, runtime])


	res.append([time.time()-starttime,232,r_0,r_1,0,0,0])
	# np.savetxt("results1.csv", np.array(res), delimiter=",")

	# np.savetxt("result.csv", np.array([[1,2,3],[1,3,15]]), delimiter=',')
	# np.savetxt('output.csv', results, delimiter=',', fmt='%f')