import numpy as np
from sympy import *
import random

def basis_append_switch(state_vec, state_basis, to_add_state, desired_basis):
    """
    Function tensors a new state to an existing state, and rewrites resulting state vector in terms of desired new basis

    Inputs:
    -------
    state_vec: array (preferably sympy 4x1 Matrix)
        vector describing the first state, in the basis given by state_basis.

    state_basis: numpy array (dimension 4x2)
        numpy array (matrix) of the basis of state_vec. each row of state_basis corresponds to the same row of state_vec,
        the left and right columns describe different qubits. Example: np.array([["A","E1"],["A","L1"],["B","E1"],["B","L1"]])

    to_add_state: Linear combination of Symbols (sympy)
        Example: e*Symbol("C") + f*Symbol("D"). Make sure that the linear combination is written in terms of

    desired_basis: numpy array (dimension 4x2)
        same formatting as state_basis. Note that this function will look and operate in terms of a shared qubit basis
        between this array and state_basis, and if there is none it will no longer function. Example:
        np.array([["A","C"],["A","D"],["B","C"],["B","D"]]), compared to the earlier basis, rearranging will happen internally
        in tems of the electron spin, or ["A","B"] basis.

    Outputs:
    --------
    New state vector along with its basis.

    """

    def which_basis(basis1, basis2):
        """
        Function finds the basis that is constant between the original and desired basis, returns that as the basis to
        rearrange in terms of.
        """
        possible_basis = {"A": "B", "C": "D", "F": "G", "E": "L"}
        this_one = 0

        for i in range(len(basis1[0])):
            current_basis = basis1[0][i]
            if current_basis == basis2[0][0] or current_basis == basis2[0][1]:
                this_one = np.copy(current_basis)
                break
            elif current_basis == basis2[2][0] or current_basis == basis2[2][1]:
                this_one = np.copy(current_basis)
                break
            else:
                continue
        if this_one == 0:
            raise ValueError("Your input state basis doesn't include any basis that is in the desired basis")

        # print(type(this_one))
        # print(f'here {possible_basis[str(this_one)]}')
        if str(this_one)[0] == "E":
            return([str(this_one), str(possible_basis[str(this_one)[0]]) + str(this_one)[1]])
        else:
            return([str(this_one), str(possible_basis[str(this_one)])])


    focus_basis = which_basis(state_basis, desired_basis)
    ### Rearrange
    rearranged_vector = 0
    db_pos = np.where(state_basis[0] == focus_basis[0])[0][0]               # Not foolproof but it works for now.
    # print(f"We have determined that the important basis here is {focus_basis} and its position in the original state basis is {db_pos}")
    for basis in focus_basis:
        temp = 0
        for i in range(len(state_basis)):
            # print(state_basis[i][db_pos])
            # print("separator")
            # print(desired_basis)
            if state_basis[i][db_pos] == basis:
                # print(f'We checking it aaaaalll')
                # print(f'state basis"s ith entry{state_basis[i]}')
                # print(f'this is dbpos: {db_pos}')
                # print(f'state basis"s ith entry, and then dbposth entry: {state_basis[i][db_pos]}')
                temp += state_vec[i] * Symbol(state_basis[i][1 - db_pos])
        rearranged_vector += temp * Symbol(basis)
    # print(f"after rearranging, our rearranged_vector looks like {rearranged_vector}")


    pos_expr1_basis = np.where(desired_basis[0] == focus_basis[0])[0][0]
    ### Rewrite in new basis
    new_state_vector = Matrix([[0], [0], [0], [0]])
    # new_state_basis = np.zeros([1,4])
    new_state_basis = []
    for basis_i in range(len(desired_basis)):
        ket1 = desired_basis[basis_i][0]
        ket2 = desired_basis[basis_i][1]
        if pos_expr1_basis == 0:
            # new_state_basis[basis_i] = new_basis[basis_i]
            new_state_basis.append(desired_basis[basis_i])
            new_state_vector[basis_i] = rearranged_vector.coeff(Symbol(ket1), 1) * to_add_state.coeff(Symbol(ket2), 1)
        elif pos_expr1_basis == 1:
            # new_state_basis[basis_i] = new_basis[basis_i][::-1]
            new_state_basis.append(desired_basis[basis_i][::-1])
            new_state_vector[basis_i] = to_add_state.coeff(Symbol(ket2), 1) * rearranged_vector.coeff(Symbol(ket1), 1)
        else:
            raise ValueError("Some error is occurring idk anymore")
    # print(f'Just before output we have {new_state_vector}')
    return new_state_vector, np.array(new_state_basis)

def basis_dig_switch(state_vector, state_basis, desired_basis):
     """
     Function takes a state in a given basis and a desired basis, digs out part of the desired basis from the state vector
     and rewrites the state vector in terms of this new desired basis.
     """
     full_expression = 0
     for i in range(len(state_vector)):
         full_expression += (state_vector[i])*Symbol(state_basis[i][0])*Symbol(state_basis[i][1])

     rewritten_vector = Matrix([[0],[0],[0],[0]])
     for i in range(len(state_basis)):
         rewritten_vector[i] = full_expression.coeff(Symbol(desired_basis[i][0])*Symbol(desired_basis[i][1]),1)

     return rewritten_vector


def swapXmeasure(state_vector):
    """
    Accepts input in the spin tensor photon basis (state_vector), performs X measurement and returns resulting state.
    """
    [a, b, c, d] = state_vector
    if np.random.uniform(0, 1) < 0.5:
        sign = 1
    else:
        sign = -1
    return 1/np.sqrt(2)*Matrix([[a + c],[b + d],[sign*(a+c)],[sign*(b+d)]])

def EmitterGate(state_vector, state_basis, photon_number, loss_coeffs):
    # Assemble the state "equation" from the given state vector
    state_expression = 0
    for i in range(len(state_vector)):
        state_expression += state_vector[i] * Symbol(state_basis[i][0]) * Symbol(state_basis[i][1])

    # Assemble list of basis that are involved here
    basis_list = ["A", "B", "C", "D", "F", "G"]
    for k in range(photon_number):
        basis_list.append("E" + str(k + 1))
        basis_list.append("L" + str(k + 1))

    # Assemble alpha and beta, for ease of use in calculating |alpha|^2 and |beta|^2 (All this next bit does is remove the Symbols that
    # represent states. For use in probability calculations
    alpha_check = state_expression.coeff(Symbol("A"), 1)
    beta_check = state_expression.coeff(Symbol("B"), 1)

    symbs = list(alpha_check.free_symbols)
    basis_list1 = [symbols(v) for v in basis_list]
    to_remove = list(set(symbs).intersection(basis_list1))
    subs_dict = {}
    for letter in to_remove:
        subs_dict[letter] = 1
    alpha = alpha_check.subs(subs_dict)

    symbs = list(beta_check.free_symbols)
    basis_list1 = [symbols(v) for v in basis_list]
    to_remove = list(set(symbs).intersection(basis_list1))
    subs_dict = {}
    for letter in to_remove:
        subs_dict[letter] = 1
    beta = beta_check.subs(subs_dict)

    # Pnl =
    # Pcl =
    # Pse =
    # List of possible basis


    # Check probabilities
    # print(f'Do the probabilities add up to 1? {Pnl + Pcl + Pse == 1}')
    # if not Pnl + Pcl + Pse == 1:
    #     raise ValueError("Your probabilities dont even add up to one")
    #
    # situation = random.choices([1,2,3],[Pnl*100,Pcl*100,Pse*100])
    situation = 1                                                                                           # overwrite for now!
    # No loss
    if situation == 1:
        return Entangling_op @ current_state, state_basis

    # Cavity Loss
    elif situation == 2:
        alpha_cl = state_expression.coeff(Symbol("A"),1)
        beta_cl = state_expression.coeff(Symbol("B"),1)
        new_state = (alpha_cl*loss_coeffs[0]*Symbol("A") + beta_cl*loss_coeffs[1]*Symbol("B"))*Symbol("Vac1")
        new_vector = np.zeros(4)
        new_vector[0] = new_state.coeff(Symbol("A") * Symbol("C"))
        new_vector[1] = new_state.coeff(Symbol("A") * Symbol("D"))
        new_vector[2] = new_state.coeff(Symbol("B") * Symbol("C"))
        new_vector[3] = new_state.coeff(Symbol("B") * Symbol("D"))
        return new_vector, np.array([["A","C"],["A","D"],["B","C"],["B","D"]])


    # Spontaneous Loss
    elif situation == 3:
        # First we need to find alpha and beta. This is a little different than with cavity loss because the probabilities
        # are now magnitude squared, so we need to get rid of any states disguising themselves as letters.

        # Select one of four decay possibilities and return with the output state: THIS IS WRONG BECAUSE ALPHA^2 INCLUDES CROSS TERMS.
        decay_type = random.choices([1,2,3,4],[(0.5*Pse0*abs(alpha)**2)*100,(0.25*Pse0*abs(alpha+beta)**2)*100,(0.5*Pse1*abs(beta)**2)*100,(0.25*Pse1*abs(alpha-beta)**2)])
        # if decay_type == 1:


    else:
        raise ValueError("None of the probabilities actually happened!")



# def Xmeasure(state_vector, state_basis, measurement_basis):
#     """
#     Performs X measurement on given state vector, in that it causes the state to collapse with 50%/50% chance.
#     measurement_basis has four possibilities: "Photon", "Electron", "Carbon", "Nuclear"
#     """
#     basis_dict = {"Photon":["E","L"],"Electron":["A","B"],"Nuclear":["C","D"],"Carbon":["F","G"]}
#     basis = basis_dict[measurement_basis]
#     coeffs = np.zeros(2)
#     for i in range(len(state_basis)):
#         if measurement_basis == "Electron":
#             raise NotImplementedError
#         else:
#             for k in range(len(basis)):
#                 if state_basis[i][0] == basis[k]:
#                     coeffs[k] += state_vector[i]*Symbol(state_basis[i][1])
#                 elif state_basis[i][1] == basis[k]:
#                     coeffs[k] += state_vector[i]*Symbol(state_basis[i][0])
#
#     # At this point we should have coeffs = ["alpha","beta"] for alpha|A> + beta|B>
#     if np.random.uniform(0,1) < 0.5:
#
#     else:





#######################################################################################################
##### Testing area
#######################################################################################################
    
# a,b,c,d = Symbol("a"), Symbol("b"), Symbol("c"), Symbol("d")
# e,f = Symbol("e"), Symbol("f")
# state_vector = Matrix([[a],[b],[c],[d]])
# state_basis = np.array(["AE","AL","BE","BL"])
# CeNOTph = Matrix([[1,0,0,0],[0,1,0,0],[0,0,0,1],[0,0,1,0]])
# nuclear_state = e*Symbol("C") + f*Symbol("D")
#
# state_vector_cnot = CeNOTph*state_vector
# print(f'after CNOT gate: {state_vector_cnot}')
# rearr = rearrange(state_vector_cnot, state_basis, ["A","B"], 0)
# print(f'rearranged state is {rearr}')
# state_vector1, state_basis1 = setup_new_vector(rearr, nuclear_state, ["AC","AD","BC","BD"])
# print(f'CHECK THIS: The old function state vector: {state_vector1}')
# print(f'CHECK THIS: The old function state basis: {state_basis1}')
# state_vector2, state_basis2 = basis_append_switch(state_vector_cnot, np.array([["A","E1"],["A","L1"],["B","E1"],["B","L1"]]), nuclear_state, np.array([["A","C"],["A","D"],["B","C"],["B","D"]]))
# print(f'CHECK THIS: The new function state vector: {state_vector2}')
# print(f'CHECK THIS: The new function state basis: {state_basis2}')


#######################################################################################################
##### Code base starts here
#######################################################################################################
""" Basis notice: Photon basis: ["E","L"], electron basis: ["A","B"], nuclear spin basis: ["C","D"] """
""" Carbon spin basis: ["F","G"] """
""" Definitions """
# Memory Memory spin entanglement
T1 = Symbol("T1")
CcNOTe = 1/2*np.array([symbols("M(1:17)")]).reshape(4,4)
CeNOTn = np.array([[cos(T1/2),0,0, -sin(T1/2)*1j],[0,0,-sin(T1/2)*1j+cos(T1/2),0],[0,-sin(T1/2)*1j+cos(T1/2),0,0],[-sin(T1/2)*1j,0,0,cos(T1/2)]])
# print(CeNOTn)

# Nuclear Electron CPhase
H = 1/np.sqrt(2)*np.array([symbols("H(1:5)")]).reshape(2,2)
H_basis = np.kron(H,np.identity(2))
CnNOTe = np.array([[1,0,0,0],[0,0,0,1],[0,0,1,0],[0,1,0,0]])
NECPhase = H_basis @ CnNOTe @ H_basis
# print(NECPhase)

# Photon Electron Entangling operation (No loss)
r0, r1 = Symbol("r0"), Symbol("r1")
Rx = 1/np.sqrt(2)*np.array([symbols("R(1:5)")]).reshape(2,2)
Rx_basis = np.kron(Rx,np.identity(2))
Z = np.array([symbols("Z(1:5)")]).reshape(2,2)
Z_basis = np.kron(Z,np.identity(2))
ES = np.array([[r0,0,0,0],[0,1,0,0],[0,0,r1,0],[0,0,0,1]])
LS = np.array([[1,0,0,0],[0,r0,0,0],[0,0,1,0],[0,0,0,r1]])
Entangling_op = Z_basis@Rx_basis@H_basis@LS@H_basis@ES@Rx_basis
# print(Entangling_op)

# Carbon Electron Swap: 
T2 = Symbol("T2")
CeNOTc = np.array([[cos(T2/2),0,0, -sin(T2/2)*1j],[0,0,-sin(T2/2)*1j+cos(T2/2),0],[0,-sin(T2/2)*1j+cos(T2/2),0,0],[-sin(T2/2)*1j,0,0,cos(T2/2)]])

# Hadamard for photon-electron teleport/swap (Acts on Photon, in Photon-Electron spin basis)
H_teleport = np.kron(np.identity(2), H)

""" [1,1,1] Tree with both memory spins still in place"""

### Step 1 Initialization
init_state = 1/2*Matrix([[1],[1],[1],[1]])
init_basis = np.array([["A","F"],["A","G"],["B","F"],["B","G"]])
# init_basis1 = np.array(["AF", "AG", "BF", "BG"])

### Step 2 Memory spin entangling
current_state = CcNOTe * init_state
print(f'Look here! {current_state}')
current_state, current_basis = basis_append_switch(current_state, init_basis, 1/np.sqrt(2)*(Symbol("C") + Symbol("D")), np.array([["A","C"],["A","D"],["B","C"],["B","D"]]))
current_state = NECPhase @ current_state

### Step 3 - Emitter Gate step
photon1 = 1/np.sqrt(2)*(Symbol("E1") - Symbol("L1")*1j)
current_state, current_basis = basis_append_switch(current_state, current_basis, photon1, np.array([["A","E1"],["A","L1"],["B","E1"],["B","L1"]]))

current_state, current_basis = EmitterGate(current_state, current_basis, 1, [Symbol("l0"),Symbol("l1")])




### Step 4 - Swapping spin state
photon2 = 1/np.sqrt(2)*(Symbol("E2") - Symbol("L2")*1j)
current_state, current_basis = basis_append_switch(current_state, current_basis, photon2, np.array([["A","E2"],["A","L2"],["B","E2"],["B","L2"]]))
current_state = H_teleport @ Entangling_op @ current_state
current_state = swapXmeasure(current_state)

# current_state = basis_dig_switch(current_state,current_basis,np.array([["A","C"],["A","D"],["B","C"],["B","D"]]))
# current_state = NECPhase @ current_state






            
    

