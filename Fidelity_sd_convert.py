import numpy as np
import qutip as qp
from PTCS_Monte_Carlo import draw_errvals, errored_H, errored_Z, errored_xrotate
import random

# def find_fidelity(sd, gate, runs):
#
#     def fidelity(message_qubit, decoded_qubit):
#
#         arr = np.array(decoded_qubit).T[0]
#         alph = arr[0]
#         bet = arr[1]
#
#         arr1 = np.array(message_qubit).T[0]
#         alpha = arr1[0]
#         beta = arr1[1]
#
#         return abs(np.conj(alpha) * alph + np.conj(beta) * bet) ** 2
#
#     def fidelity_multi(message_qubit, decoded_qubit):
#
#         arr = np.array(decoded_qubit).T[0]
#         arr1 = np.array(message_qubit).T[0]
#         # print(f'arr {arr} arr1 {arr1}')
#         print(f'debug {np.dot(np.conj(arr),arr1)}')
#         return abs(np.dot(np.conj(arr),arr1)) ** 2
#
#     zero = qp.basis(2,0)
#     plus = (qp.basis(2,0) + qp.basis(2,1)).unit()
#     minus = (qp.basis(2,0) - qp.basis(2,1)).unit()
#
#     spec = qp.Qobj([[1+1j], [1+1j]])*1/2
#     # multi1 = qp.Qobj(np.array([4,3,1,2]),dims=[[2,2],[1,1]]).unit()
#     multi1 = qp.Qobj(np.array([1,2,4,3]),dims=[[2,2],[1,1]]).unit()
#
#     CeNOTn = qp.Qobj(np.array([[1,0,0,0],[0,1,0,0],[0,0,0,1],[0,0,1,0]]))
#
#     fid_list = []
#     if gate == "H":
#         for run in range(runs):
#             H = errored_H(draw_errvals(theta_sd=sd))
#             result = H*zero
#             fid_list.append(fidelity(plus, result))
#
#     elif gate == "Z":
#         for run in range(runs):
#             Z = errored_Z(draw_errvals(theta_sd=sd))
#             result = Z*plus
#             fid_list.append(fidelity(minus, result))
#
#     elif gate == "R":
#         for run in range(runs):
#             R = errored_xrotate(draw_errvals(theta_sd=sd))
#             result = R*plus
#             fid_list.append(fidelity(spec, result))
#
#     elif gate == "CeNOTn":
#         for run in range(runs):
#             theta = draw_errvals(theta_sd=sd)[0]
#             gate = np.array([[np.cos(theta/2),-1j*np.sin(theta/2),0,0],[-1j*np.sin(theta/2), np.cos(theta/2),0,0],[0, 0, -1j*np.sin(theta/2) + np.cos(theta/2), 0],[0,0,0, -1j*np.sin(theta/2) + np.cos(theta/2)]])
#             # init_state = qp.Qobj(np.array([4,3,2,1]), dims=[[2,2],[1,1]]).unit()
#             init_state = qp.Qobj(np.array([1,2,3,4]), dims=[[2, 2], [1, 1]]).unit()
#             result = gate*CeNOTn*init_state
#             print(f'result is {result}, init state is {init_state}')
#             fidresult = fidelity_multi(multi1,result)
#             # print(f'fidresult is {fidresult}')
#             fid_list.append(fidresult)
#
#     else:
#         raise NotImplementedError
#     print(f'Results {fid_list}')
#     return np.mean(fid_list), np.std(fid_list)
#
# def find_dephase_fidelity(standard_dev, runs):
#     def fidelity_multi(message_qubit, decoded_qubit):
#
#         arr = np.array(decoded_qubit).T[0]
#         arr1 = np.array(message_qubit).T[0]
#         # print(f'arr {arr} arr1 {arr1}')
#         print(f'debug {np.dot(np.conj(arr),arr1)}')
#         return abs(np.dot(np.conj(arr),arr1)) ** 2
#
#     fid_list = []
#     comparison_state = qp.Qobj(np.array([1,1,1,1]),dims=[[2,2],[1,1]]).unit()
#     for run in range(runs):
#         init_state_probabilities = np.array([1,1,1,1])
#         theta00 = np.random.normal(0, standard_dev)
#         theta01 = np.random.normal(0, standard_dev)
#         theta10 = np.random.normal(0, standard_dev)
#         theta11 = np.random.normal(0, standard_dev)
#         dephased_probabilities = np.exp(np.array([theta00, theta01, theta10, theta11]))
#         init_state = qp.Qobj(init_state_probabilities * dephased_probabilities, dims = [[2, 2], [1, 1]]).unit()
#         fid_list.append(fidelity_multi(comparison_state, init_state))
#
#     return np.mean(fid_list), np.std(fid_list)


def convert_errorsd_to_fidelity(error_sd=None, runs=10000, depol_rate=None):
    """
    Function takes input error standard deviation and performs a bell state preparation operation, with an errored CNOT
    gate that uses the input error_sd to calculate its error. The function performs this calculation a number of times,
    indicated by parameter 'runs' (defaults to 10000) and calculates the fidelity loss caused due to this error. The
    function outputs the infidelity of the prepared bell state, which is a good indicator of how large the error caused
    by error_sd is.

    The bell state preparation is: |+>|0> ---->CNOT----> |00> + |11>
    """
    def fidelity_multi(message_qubit, decoded_qubit):

        arr = np.array(decoded_qubit).T[0]
        arr1 = np.array(message_qubit).T[0]
        # print(f'arr {arr} arr1 {arr1}')
        # print(f'debug {np.dot(np.conj(arr),arr1)}')
        return abs(np.dot(np.conj(arr),arr1)) ** 2

    if error_sd is None and depol_rate is None:
        raise ValueError("Are you looking at correlated or uncorrelated error?")

    zero = qp.basis(2, 0)
    plus = (qp.basis(2, 0) + qp.basis(2, 1)).unit()

    sx = qp.sigmax()
    sz = qp.sigmaz()
    sy = qp.sigmay()

    if depol_rate is None and error_sd is not None:
        fid_list = []
        CNOT = qp.Qobj(np.array([[1, 0, 0, 0], [0, 1, 0, 0], [0, 0, 0, 1], [0, 0, 1, 0]]), dims=[[2, 2], [2, 2]])
        CNOT_bar = qp.Qobj(np.array([[0, 1, 0, 0], [1, 0, 0, 0], [0, 0, 1, 0], [0, 0, 0, 1]]), dims=[[2, 2], [2, 2]])

        init_state = qp.tensor(plus, zero)
        perf_state = qp.Qobj(np.array([1, 0, 0, 1]), dims=[[2, 2], [1, 1]]).unit()

        for i in range(runs):
            theta = draw_errvals(theta_sd=error_sd)[0]
            gate = (np.cos(theta / 2) * qp.tensor(qp.qeye(2), qp.qeye(2)) - 1j * np.sin(theta / 2) * CNOT_bar) * CNOT

            bell_state = gate*init_state
            fid_list.append(fidelity_multi(perf_state, bell_state))

    elif depol_rate is not None and error_sd is None:

        runs = 10000000
        msg = (qp.basis(2, 0) + qp.basis(2, 1)).unit()
        fid_list = []
        for i in range(runs):
            depol = random.choices([True, False], [depol_rate, 1 - depol_rate])[0]
            if depol:
                error_type = random.choices([1, 2, 3], [0,0.5,0.5])[0]
                if error_type == 1:
                    outputstate = sx * msg
                elif error_type == 2:
                    outputstate = sy * msg
                elif error_type == 3:
                    outputstate = sz * msg
            else:
                outputstate = msg
            fid_list.append(fidelity_multi(msg, outputstate))
    else:
        raise NotImplementedError



    #     # print(f'yelp')
    #     fid_list = []
    #     perf_state = qp.Qobj(np.array([1, 0, 0, 1]), dims=[[2, 2], [1, 1]]).unit()
    #     CNOT = qp.Qobj(np.array([[1, 0, 0, 0], [0, 1, 0, 0], [0, 0, 0, 1], [0, 0, 1, 0]]), dims=[[2, 2], [2, 2]])
    #     for i in range(runs):
    #         depol = random.choices([True, False], [depol_rate, 1 - depol_rate])[0]
    #         init_state = qp.tensor(plus, zero)
    #         if depol:
    #             error_type = random.choices([1, 2, 3], [0,0.5, 0.5])[0]
    #             if error_type == 1:
    #                 outputstate = qp.tensor(sx, qp.qeye(2)) * init_state
    #             elif error_type == 2:
    #                 outputstate = qp.tensor(sy, qp.qeye(2)) * init_state
    #             elif error_type == 3:
    #                 outputstate = qp.tensor(sz, qp.qeye(2)) * init_state
    #         else:
    #             outputstate = init_state
    #         outputstate = CNOT*outputstate
    #         fid = fidelity_multi(perf_state, outputstate)
    #         print(f'INfid> {1-fid}')
    #         fid_list.append(fid)
    #         # print(f'Output state is {outputstate}')
    # else:
    #     raise NotImplementedError


    return 1-np.mean(fid_list)

# # Producing the X-axis for fidelity plots
# # sds = np.arange(0,0.35,0.05)
# sds = np.logspace(-2,0.5,num=20)/4
# # sds = np.arange(0,0.04,0.02)
# infid_list = []
# for sd in sds:
#     infid = convert_errorsd_to_fidelity(error_sd=sd)
#     infid_list.append(infid)
#     print(f'Infidelity of standard deviation {sd} is: {infid}')
#
# np.savetxt("infidelities20.csv", np.array([np.array(infid_list), sds]), delimiter=",")
# # print(sds)
# # print(infid_list)


# Producing the X-axis for the theory depol curve
# drs = np.array([0.10])
drs = np.logspace(-5, -0.7447274949, num=21)
infid_list = []
for dr in drs:
    # print(f'depol rate {dr}')
    infid = convert_errorsd_to_fidelity(depol_rate=dr)
    print(f'Infid {infid}')
    infid_list.append(infid)
print(f'Infidelity of depol rates {drs} are {infid_list}')

np.savetxt("infidelities_depolarization_10mil.csv", np.array([np.array(infid_list), drs]), delimiter=",")

