This is the repository for the Master's Thesis Project on the topic of:

"Investigating the propagation of correlcated error in photonic tree cluster states" at Qutech, TU Delft, as part of the Borregaard group.

## Abstract

In this project, we look to investigate the effects of correlated operational errors and loss errors in photonic tree cluster state type encoding, for applications such as the transmission of quantum information via third generation quantum repeaters. We will be looking to construct a numerical simulation written in python, which can take into account many types of operational and loss errors and perform a monte carlo simulation that gives us statistical estimates for total tree fidelity and decoding probability of an encoded message. 

For tree generation, we assume the use of a separated emitter-gate system, used to generate photons (from the emitter) and scatter them (off the gate system) in order to create entanglement between the photons and an electron spin. The electron spin is itself entangled to two memory spins sourced from a SiV colour center, which allows us to construct a full tree cluster state branch by branch.

Simulating over 10,000 runs for each error parameter combination , we find the dephasing of the electron and memory spins during scattering, and loss of photons during the scattering process to most negatively impact transmission fidelity across a distance $L$ spanned by $m$ repeater stations. We also find that correlated errors contribute more to tree infidelity than uncorrelated errors and that in general, the $[4,14,4]$ tree structure is best suited for long transmission, provided we have an efficient gate system. 

## Repository Description

The thesis report attached to this project can be found in the main repository file, titled "Investigating_the_propagation_of_correlated_error_in_photonic_tree_cluster_states.pdf". This report contains all details of the project, including assumptions made, methodology and summary / discussion of results. The sections below in this README file will summarize briefly the main points.

The file "PTCS_Monte_Carlo_5qubit.py" contains the main simulation code. It uses the "shuffle method" to choose a decoding photon and therefore runs its Monte Carlo simulations in a 4 / 5 qubit basis state vector environment.

Due to the large amounts of operator matrices involved in the simulation, a supercomputing cluster was necessary to produce the results graphs. Here we used the Dutch national supercomputer "Snellius" https://www.surf.nl/en/dutch-national-supercomputer-snellius for this purpose, where "jobscript.sh" in this repository was the job script that was sent to the supercomputer. To enable the simulation code of "PTCS_Monte_Carlo_5qubit.py" to be able to be run on multiple cores in parallel, we used the code in "Multiprocessing_Monte_Carlo.py".

## Simulation Details

For detailed descriptions of the exact simulation process, please refer to the thesis report in this repository. Here we will gloss over the most important points for users who might consider using these simulation results. 

### Inputs & Outputs

The user must provide first and foremost:

- The branching vector of a depth 3 photonic tree cluster state they would like to simulate
- The number of Monte Carlo runs to simulate. Note that N runs in the "PTCS_Monte_Carlo_5qubit.py" file and M runs in the "Multiprocessing_Monte_Carlo.py" file total up to NxM runs in total. Example: 80 runs split across 128 cores = 80 x 128 = 10,240 runs in total
- The message qubit to be encoded and transmitted (This is not very important, and can be just left as the $|+\rangle$ state)

The user must next provide inputs for the system parameters that control operational errors and loss errors:

- Emitter-Gate system Gate parameters (e.g. cooperativity, detuning of the excited 1 state, etc. etc.). These will control the values of the reflection and loss coefficients that describe the spin-cavity Gate system.
- Emitter error standard deviation
- Single qubit gate error standard deviation
- Multi qubit gate error standard deviation
- Nuclear dephasing error standard deviation
- Probability for emitter-gate transmission loss
- Probability for repeater-repeater transmission loss
- Spontaneous emission cross loss parameter 
- Total detection efficiency (Leave this at 1 unless a special situation calls for otherwise)

The file "PTCS_Monte_Carlo_5qubit.py" will output an average fidelity, standard deviation of the average fidelity and successful decoding probability in the terminal prompt, as well as a runtime for N runs. The file "Multiprocessing_Monte_Carlo.py" will attempt to save the results for an array of inputs in a csv file in the same directory.

### Assumptions

Here follows the general assumptions made in the making of this simulation:

- The two photon detuning in the emitter $\delta = 0$
- Spontaneous emissions are caused by interaction between the spin and the vacuum state of the environment.
- We are in the weak driving scheme, far detuned from the level $\Omega << \Delta$
- Tthe rate of photon emission from the emitter is large compared to the rate of generation of photons inside the cavity. The rate of spontaneous emissions is negligible compared to the single photon Rabi frequency $\gamma << g << \kappa$. This is the Purcell regime.
- The cooperativity of the gate system is defined as $C = |g|^2/\kappa\gamma$.
- The time between early and late driving on the emitter should be large enough such that the emitter states $|e0\rangle$ and $|f1\rangle$ are negligible at $t=T_2$, where $T_2$ is the time when the late time bin drive begins. 
- Multiple decay event probability in the emitter is negligible.
- For the derivation of the photonic time bins, assume that the driving laser is a continuous block pulse instead of two blocks separated by a period of non-driving.
- Assume lost emitter photonic population (non-usable modes) end up in a dump state and therefore do not accumulate in the other emitter states $|g\rangle$ and $|f\rangle$.

Other assumptions:

- We are in the strong coupling regime
- Errors are modelled using the bloch sphere rotation matrix. Error terms (in particular those for CNOT drives) contain an inseparable phase accumulation term "i". In other words: The way we model driving errors between energy levels assumes that every type of driving error inherently causes a phase accumulation of pi/2 on the erroneous state.
- We assume the X measurement during the spin photon swap is a perfect operation.
- Each single gate operation (Hadamard, Z gate, Rotation gate) has its own separate error. 
- Each multi qubit gate operation (CeNOTn etc.) has its own separate error. The orientation of target and control qubit matters for error. For particular combinations of CNOT gates (e.g. CnNOTe - CeNOTn - CnNOTe), the error of one of the operations (CeNOTn) may dominate that of the others. 

