import numpy as np
from matplotlib import pyplot as plt
import scipy

infid_data17 = np.genfromtxt('infidelities17.csv', delimiter=',', dtype=None)
gate_infidelity17 = infid_data17[0]

infid_data20 = np.genfromtxt('infidelities20.csv', delimiter=',', dtype=None)
gate_infidelity20 = infid_data20[0]

infid_data22 = np.genfromtxt('infidelities22.csv', delimiter=',', dtype=None)
gate_infidelity22 = infid_data22[0]

infid_data20d = np.genfromtxt('infidelities_depolarization.csv', delimiter=',', dtype=None)
gate_infidelity20d = infid_data20d[0]

# ##################################################################################
# ################################################################################## [2,3,2] Error investigation
# ##################################################################################
plt.figure(figsize=(10, 6))

# ######################### No loss case
# # Obtain x axis
# sim_data = np.genfromtxt('errors_results_2,3,2_shuffle_noloss.csv', delimiter=',', dtype=None)
# fids = sim_data[:,0][:-1]
# errs = sim_data[:,1][:-1]
#
# # plt.errorbar(gate_infidelity[:-8], 1-fids[:-8], yerr=errs[:-8], marker='.', label='[2,3,2] Perfect Loss Case', linestyle='', markersize=8, capsize=2, elinewidth=0.5)
# plt.plot(gate_infidelity22, 1-fids, '.', label='No Loss Case')


######################### Depol error case
# Obtain x axis
# sim_data = np.genfromtxt('errors_results_2,3,2_shuffle_depol.csv', delimiter=',', dtype=None)
sim_data = np.genfromtxt('errors_2,3,2_depol.csv', delimiter=',', dtype=None)
fids = sim_data[:,0][:-1]
errs = sim_data[:,1][:-1]

# plt.errorbar(gate_infidelity[:-8], 1-fids[:-8], yerr=errs[:-8], marker='.', label='[2,3,2] Perfect Loss Case', linestyle='', markersize=8, capsize=2, elinewidth=0.5)
plt.plot(gate_infidelity20d, 1-fids, 'x', label='Depol Errors')


######################### only emitter error
# Obtain x axis
sim_data = np.genfromtxt('errors_results_2,3,2_shuffle_emittererror.csv', delimiter=',', dtype=None)
fids = sim_data[:,0][:-1]
errs = sim_data[:,1][:-1]

# plt.errorbar(gate_infidelity20, 1-fids, yerr=errs, marker='.', label='only emitter error', linestyle='', markersize=8, capsize=2, elinewidth=0.5)
plt.plot(gate_infidelity20, 1-fids, '.', label='only emitter error')
#
#
######################### only single gate error
# Obtain x axis
sim_data = np.genfromtxt('errors_results_2,3,2_shuffle_singleerrors.csv', delimiter=',', dtype=None)
fids = sim_data[:,0][:-1]
errs = sim_data[:,1][:-1]

# plt.errorbar(gate_infidelity20, 1-fids, yerr=errs, marker='.', label='only emitter error', linestyle='', markersize=8, capsize=2, elinewidth=0.5)
plt.plot(gate_infidelity20, 1-fids, '.', label='only single gate errors')


######################### only multi gate error
# Obtain x axis
sim_data = np.genfromtxt('errors_results_2,3,2_shuffle_multierror.csv', delimiter=',', dtype=None)
fids = sim_data[:,0][:-1]
errs = sim_data[:,1][:-1]

# plt.errorbar(gate_infidelity[:-8], 1-fids[:-8], yerr=errs[:-8], marker='.', label='[2,3,2] Perfect Loss Case', linestyle='', markersize=8, capsize=2, elinewidth=0.5)
plt.plot(gate_infidelity20, 1-fids, '.', label='only multi gate errors')
#
######################### only nuclear dephase error
# Obtain x axis
sim_data = np.genfromtxt('errors_results_2,3,2_shuffle_dephaseerror.csv', delimiter=',', dtype=None)
fids = sim_data[:,0][:-1]
errs = sim_data[:,1][:-1]

# plt.errorbar(gate_infidelity[:-8], 1-fids[:-8], yerr=errs[:-8], marker='.', label='[2,3,2] Perfect Loss Case', linestyle='', markersize=8, capsize=2, elinewidth=0.5)
plt.plot(gate_infidelity20, 1-fids, '.', label='only nuclear dephase errors')


plt.yscale('log')
plt.xscale('log')

plt.xlabel('Gate Infidelity (Individual)')
plt.ylabel('Tree Infidelity')
plt.grid()
# plt.legend()
plt.show()


# ##################################################################################
# ################################################################################## [2,3,2] Errors with losses (no nuclear dephase)
# ##################################################################################
plt.figure(figsize=(10, 6))

######################### No loss case
# Obtain x axis
sim_data = np.genfromtxt('errors_results_2,3,2_shuffle_3e_noloss.csv', delimiter=',', dtype=None)
fids = sim_data[:,0][:-1]
errs = sim_data[:,1][:-1]

# plt.errorbar(gate_infidelity[:-8], 1-fids[:-8], yerr=errs[:-8], marker='.', label='[2,3,2] Perfect Loss Case', linestyle='', markersize=8, capsize=2, elinewidth=0.5)
plt.plot(gate_infidelity20, 1-fids, '.', label='No Loss Case')


# ######################### case 1 rrtl 0.01
# # Obtain x axis
# sim_data = np.genfromtxt('errors_2,3,2_shuffle_2e_rrtl0.01.csv', delimiter=',', dtype=None)
# fids = sim_data[:,0][:-1]
# errs = sim_data[:,1][:-1]
#
# # plt.errorbar(gate_infidelity[:-8], 1-fids[:-8], yerr=errs[:-8], marker='.', label='[2,3,2] Perfect Loss Case', linestyle='', markersize=8, capsize=2, elinewidth=0.5)
# plt.plot(gate_infidelity20, 1-fids, 'x', label='2 errors rrtl 0.01')

######################### case 1 rrtl 0.01
# Obtain x axis
sim_data = np.genfromtxt('errors_2,3,2_shuffle_2e_rrtl0.01_case2.csv', delimiter=',', dtype=None)
fids = sim_data[:,0][:-1]
errs = sim_data[:,1][:-1]

# plt.errorbar(gate_infidelity[:-8], 1-fids[:-8], yerr=errs[:-8], marker='.', label='[2,3,2] Perfect Loss Case', linestyle='', markersize=8, capsize=2, elinewidth=0.5)
plt.plot(gate_infidelity20, 1-fids, 'x', label='2 errors case 2 rrtl 0.01')

######################### case 1 rrtl 0.01
# Obtain x axis
sim_data = np.genfromtxt('PPT_errors_2,3,2_2e_rrtl0.1.csv', delimiter=',', dtype=None)
fids = sim_data[:,0][:-1]
errs = sim_data[:,1][:-1]

# plt.errorbar(gate_infidelity[:-8], 1-fids[:-8], yerr=errs[:-8], marker='.', label='[2,3,2] Perfect Loss Case', linestyle='', markersize=8, capsize=2, elinewidth=0.5)
plt.plot(gate_infidelity20, 1-fids, 'x', label='2 errors new rrtl 0.01')

# ######################### case 1 rrtl 0.01
# # Obtain x axis
# sim_data = np.genfromtxt('errors_2,3,2_shuffle_3e_rrtl0.01_case1.csv', delimiter=',', dtype=None)
# fids = sim_data[:,0][:-1]
# errs = sim_data[:,1][:-1]
#
# # plt.errorbar(gate_infidelity[:-8], 1-fids[:-8], yerr=errs[:-8], marker='.', label='[2,3,2] Perfect Loss Case', linestyle='', markersize=8, capsize=2, elinewidth=0.5)
# plt.plot(gate_infidelity20, 1-fids, 'x', label='3 errors case 1 rrtl 0.01')
#
# ######################### case 2 rrtl 0.01
# # Obtain x axis
# sim_data = np.genfromtxt('errors_2,3,2_shuffle_3e_rrtl0.01_case2.csv', delimiter=',', dtype=None)
# fids = sim_data[:,0][:-1]
# errs = sim_data[:,1][:-1]
#
# # plt.errorbar(gate_infidelity[:-8], 1-fids[:-8], yerr=errs[:-8], marker='.', label='[2,3,2] Perfect Loss Case', linestyle='', markersize=8, capsize=2, elinewidth=0.5)
# plt.plot(gate_infidelity20, 1-fids, 'x', label='3 errors case 2 rrtl 0.01')
#
# ######################### case 2 rrtl 0.01
# # Obtain x axis
# sim_data = np.genfromtxt('errors_2,3,2_shuffle_3e_rrtl0.01_case4.csv', delimiter=',', dtype=None)
# fids = sim_data[:,0][:-1]
# errs = sim_data[:,1][:-1]
#
# # plt.errorbar(gate_infidelity[:-8], 1-fids[:-8], yerr=errs[:-8], marker='.', label='[2,3,2] Perfect Loss Case', linestyle='', markersize=8, capsize=2, elinewidth=0.5)
# plt.plot(gate_infidelity20, 1-fids, 'x', label='better gate rrtl 0.01')
#
# ######################### case 2 rrtl 0.01
# # Obtain x axis
# sim_data = np.genfromtxt('errors_2,3,2_shuffle_3e_rrtl0.01_case5.csv', delimiter=',', dtype=None)
# fids = sim_data[:,0][:-1]
# errs = sim_data[:,1][:-1]
#
# # plt.errorbar(gate_infidelity[:-8], 1-fids[:-8], yerr=errs[:-8], marker='.', label='[2,3,2] Perfect Loss Case', linestyle='', markersize=8, capsize=2, elinewidth=0.5)
# plt.plot(gate_infidelity20, 1-fids, 'x', label='EVEN better gate rrtl 0.01')
#
# ######################### case 2 rrtl 0.01
# # Obtain x axis
# sim_data = np.genfromtxt('errors_2,3,2_case6_rrtl0.01.csv', delimiter=',', dtype=None)
# fids = sim_data[:,0][:-1]
# errs = sim_data[:,1][:-1]
#
# # plt.errorbar(gate_infidelity[:-8], 1-fids[:-8], yerr=errs[:-8], marker='.', label='[2,3,2] Perfect Loss Case', linestyle='', markersize=8, capsize=2, elinewidth=0.5)
# plt.plot(gate_infidelity20, 1-fids, 'x', label='ridiculously better gate rrtl 0.01')

# ######################### case 2 rrtl 0.01
# # Obtain x axis
# sim_data = np.genfromtxt('errors_2,3,2_shuffle_2e_rrtl0.01_case2.csv', delimiter=',', dtype=None)
# fids = sim_data[:,0][:-1]
# errs = sim_data[:,1][:-1]
#
# # plt.errorbar(gate_infidelity[:-8], 1-fids[:-8], yerr=errs[:-8], marker='.', label='[2,3,2] Perfect Loss Case', linestyle='', markersize=8, capsize=2, elinewidth=0.5)
# plt.plot(gate_infidelity20, 1-fids, 'x', label='2 errors case 2')

plt.yscale('log')
plt.xscale('log')

plt.xlabel('Gate Infidelity (Individual)')
plt.ylabel('Tree Infidelity')
plt.grid()
plt.legend()
plt.show()




# ##################################################################################
# ################################################################################## [2,3,2] Errors with losses (no nuclear dephase)
# ##################################################################################
plt.figure(figsize=(10, 6))

######################### No loss case
# Obtain x axis
sim_data = np.genfromtxt('errors_results_2,3,2_shuffle_3e_noloss.csv', delimiter=',', dtype=None)
fids = sim_data[:,0][:-1]
errs = sim_data[:,1][:-1]

# plt.errorbar(gate_infidelity[:-8], 1-fids[:-8], yerr=errs[:-8], marker='.', label='[2,3,2] Perfect Loss Case', linestyle='', markersize=8, capsize=2, elinewidth=0.5)
plt.plot(gate_infidelity20, 1-fids, '.', label='No Loss Case')

######################### case 2 rrtl 0.01
# Obtain x axis
sim_data = np.genfromtxt('errors_2,3,2_shuffle_3e_rrtl0.2.csv', delimiter=',', dtype=None)
fids = sim_data[:,0][:-1]
errs = sim_data[:,1][:-1]

# plt.errorbar(gate_infidelity[:-8], 1-fids[:-8], yerr=errs[:-8], marker='.', label='[2,3,2] Perfect Loss Case', linestyle='', markersize=8, capsize=2, elinewidth=0.5)
plt.plot(gate_infidelity20, 1-fids, 'x', label='rrtl0.2')

######################### case 2 rrtl 0.01
# Obtain x axis
sim_data = np.genfromtxt('errors_2,3,2_shuffle_3e_rrtl0.1.csv', delimiter=',', dtype=None)
fids = sim_data[:,0][:-1]
errs = sim_data[:,1][:-1]

# plt.errorbar(gate_infidelity[:-8], 1-fids[:-8], yerr=errs[:-8], marker='.', label='[2,3,2] Perfect Loss Case', linestyle='', markersize=8, capsize=2, elinewidth=0.5)
plt.plot(gate_infidelity20, 1-fids, 'x', label='rrtl0.1')

######################### case 2 rrtl 0.01
# Obtain x axis
sim_data = np.genfromtxt('errors_2,3,2_shuffle_3e_rrtl0.01.csv', delimiter=',', dtype=None)
fids = sim_data[:,0][:-1]
errs = sim_data[:,1][:-1]

# plt.errorbar(gate_infidelity[:-8], 1-fids[:-8], yerr=errs[:-8], marker='.', label='[2,3,2] Perfect Loss Case', linestyle='', markersize=8, capsize=2, elinewidth=0.5)
plt.plot(gate_infidelity20, 1-fids, 'x', label='rrtl0.01')

######################### case 2 rrtl 0.01
# Obtain x axis
sim_data = np.genfromtxt('errors_2,3,2_rrtl0.001.csv', delimiter=',', dtype=None)
fids = sim_data[:,0][:-1]
errs = sim_data[:,1][:-1]

# plt.errorbar(gate_infidelity[:-8], 1-fids[:-8], yerr=errs[:-8], marker='.', label='[2,3,2] Perfect Loss Case', linestyle='', markersize=8, capsize=2, elinewidth=0.5)
plt.plot(gate_infidelity20, 1-fids, 'x', label='rrtl0.001')
#

#



plt.yscale('log')
plt.xscale('log')

plt.xlabel('Gate Infidelity (Individual)')
plt.ylabel('Tree Infidelity')
plt.grid()
# plt.legend()
plt.show()





# ##################################################################################
# ################################################################################## [4,14,4] plot
# ##################################################################################
plt.figure(figsize=(10, 6))

######################### No loss case
# Obtain x axis
sim_data = np.genfromtxt('errors_results_4,14,4_shuffle_3e_noloss.csv', delimiter=',', dtype=None)
fids = sim_data[:,0][:-1]
errs = sim_data[:,1][:-1]

# plt.errorbar(gate_infidelity[:-8], 1-fids[:-8], yerr=errs[:-8], marker='.', label='[2,3,2] Perfect Loss Case', linestyle='', markersize=8, capsize=2, elinewidth=0.5)
plt.plot(gate_infidelity20, 1-fids, '.', label='No Loss Case')


# ######################### Depol error case
# # Obtain x axis
# sim_data = np.genfromtxt('errors_results_2,3,2_shuffle_depol.csv', delimiter=',', dtype=None)
# fids = sim_data[:,0][:-1]
# errs = sim_data[:,1][:-1]
#
# # plt.errorbar(gate_infidelity[:-8], 1-fids[:-8], yerr=errs[:-8], marker='.', label='[2,3,2] Perfect Loss Case', linestyle='', markersize=8, capsize=2, elinewidth=0.5)
# plt.plot(gate_infidelity20d, 1-fids, '.', label='No Loss Case')


######################### rrtl 0.01
# Obtain x axis
sim_data = np.genfromtxt('errors_4,14,4_shuffle_3e_rrtl0.01.csv', delimiter=',', dtype=None)
fids = sim_data[:,0][:-1]
errs = sim_data[:,1][:-1]

# plt.errorbar(gate_infidelity[:-8], 1-fids[:-8], yerr=errs[:-8], marker='.', label='[2,3,2] Perfect Loss Case', linestyle='', markersize=8, capsize=2, elinewidth=0.5)
plt.plot(gate_infidelity20, 1-fids, 'x', label='rrtl 0.01')

######################### rrtl 0.1
# Obtain x axis
sim_data = np.genfromtxt('errors_4,14,4_shuffle_3e_rrtl0.1.csv', delimiter=',', dtype=None)
fids = sim_data[:,0][:-1]
errs = sim_data[:,1][:-1]

# plt.errorbar(gate_infidelity[:-8], 1-fids[:-8], yerr=errs[:-8], marker='.', label='[2,3,2] Perfect Loss Case', linestyle='', markersize=8, capsize=2, elinewidth=0.5)
plt.plot(gate_infidelity20, 1-fids, 'x', label='rrtl 0.1')
#
######################### rrtl 0.2
# Obtain x axis
sim_data = np.genfromtxt('errors_4,14,4_shuffle_3e_rrtl0.2.csv', delimiter=',', dtype=None)
fids = sim_data[:,0][:-1]
errs = sim_data[:,1][:-1]

# plt.errorbar(gate_infidelity[:-8], 1-fids[:-8], yerr=errs[:-8], marker='.', label='[2,3,2] Perfect Loss Case', linestyle='', markersize=8, capsize=2, elinewidth=0.5)
plt.plot(gate_infidelity20, 1-fids, 'x', label='rrtl 0.2')

plt.yscale('log')
plt.xscale('log')

plt.xlabel('Gate Infidelity (Individual)')
plt.ylabel('Tree Infidelity')
plt.grid()
# plt.legend()
plt.show()





# ##################################################################################
# ################################################################################## [4,14,4] plot
# ##################################################################################
plt.figure(figsize=(10, 6))

######################### No loss case
# Obtain x axis
sim_data = np.genfromtxt('errors_results_4,14,4_shuffle_3e_noloss.csv', delimiter=',', dtype=None)
fids = sim_data[:,0][:-1]
errs = sim_data[:,1][:-1]

# plt.errorbar(gate_infidelity[:-8], 1-fids[:-8], yerr=errs[:-8], marker='.', label='[2,3,2] Perfect Loss Case', linestyle='', markersize=8, capsize=2, elinewidth=0.5)
plt.plot(gate_infidelity20, 1-fids, '.', label='No Loss Case')

######################### kappa0.95rrtl0.01
# Obtain x axis
sim_data = np.genfromtxt('errors_results_4,14,4_shuffle_3e_kappa0.95_rrtl0.01.csv', delimiter=',', dtype=None)
fids = sim_data[:,0][:-1]
errs = sim_data[:,1][:-1]

# plt.errorbar(gate_infidelity[:-8], 1-fids[:-8], yerr=errs[:-8], marker='.', label='[2,3,2] Perfect Loss Case', linestyle='', markersize=8, capsize=2, elinewidth=0.5)
plt.plot(gate_infidelity20, 1-fids, 'x', label='kappa 0.95 rrtl 0.01')


######################### kappa0.95rrtl0.1
# Obtain x axis
sim_data = np.genfromtxt('errors_results_4,14,4_shuffle_3e_kappa0.95_rrtl0.1.csv', delimiter=',', dtype=None)
fids = sim_data[:,0][:-1]
errs = sim_data[:,1][:-1]

# plt.errorbar(gate_infidelity[:-8], 1-fids[:-8], yerr=errs[:-8], marker='.', label='[2,3,2] Perfect Loss Case', linestyle='', markersize=8, capsize=2, elinewidth=0.5)
plt.plot(gate_infidelity20, 1-fids, 'x', label='kappa 0.95 rrtl 0.1')


######################### rrtl 0.2
# Obtain x axis
sim_data = np.genfromtxt('errors_4,14,4_case_4_rrtl0.01.csv', delimiter=',', dtype=None)
fids = sim_data[:,0][:-1]
errs = sim_data[:,1][:-1]

# plt.errorbar(gate_infidelity[:-8], 1-fids[:-8], yerr=errs[:-8], marker='.', label='[2,3,2] Perfect Loss Case', linestyle='', markersize=8, capsize=2, elinewidth=0.5)
plt.plot(gate_infidelity20, 1-fids, 'x', label='rrtl 0.01 with better gate ')
#
######################### rrtl 0.2
# Obtain x axis
sim_data = np.genfromtxt('errors_4,14,4_case5_rrtl0.01.csv', delimiter=',', dtype=None)
fids = sim_data[:,0][:-1]
errs = sim_data[:,1][:-1]

# plt.errorbar(gate_infidelity[:-8], 1-fids[:-8], yerr=errs[:-8], marker='.', label='[2,3,2] Perfect Loss Case', linestyle='', markersize=8, capsize=2, elinewidth=0.5)
plt.plot(gate_infidelity20, 1-fids, 'x', label='rrtl 0.01 with EVEN better gate ')
#
######################### rrtl 0.2
# Obtain x axis
sim_data = np.genfromtxt('errors_4,14,4_case_6_rrtl0.01.csv', delimiter=',', dtype=None)
fids = sim_data[:,0][:-1]
errs = sim_data[:,1][:-1]

# plt.errorbar(gate_infidelity[:-8], 1-fids[:-8], yerr=errs[:-8], marker='.', label='[2,3,2] Perfect Loss Case', linestyle='', markersize=8, capsize=2, elinewidth=0.5)
plt.plot(gate_infidelity20, 1-fids, 'x', label='rrtl 0.01 with a ridiculously better gate ')




plt.yscale('log')
plt.xscale('log')

plt.xlabel('Gate Infidelity (Individual)')
plt.ylabel('Tree Infidelity')
plt.grid()
# plt.legend()
plt.show()











##################################################################### Plot 1 for [2,3,2]: The effects of rrtl and egtl

# infid_data17 = np.genfromtxt('infidelities17.csv', delimiter=',', dtype=None)
# gate_infidelity17 = infid_data17[0]
#
# # Losses 0.125
# sim_data = np.genfromtxt('results_2,3,2_5qshuffle_errors_test0.01rrtl.csv', delimiter=',', dtype=None)
# fids = sim_data[:,0][:-1]
# errs = sim_data[:,1][:-1]
#
# plt.plot(gate_infidelity17, 1-fids, 'x', label='eps0 0.010: prrtl = 0.01')
#
#
# # Losses 0.125
# sim_data = np.genfromtxt('results_2,3,2_5qshuffle_errors_test0.1rrtl.csv', delimiter=',', dtype=None)
# fids = sim_data[:,0][:-1]
# errs = sim_data[:,1][:-1]
#
# plt.plot(gate_infidelity17, 1-fids, 'x', label='eps0 0.1: prrtl = 0.1')
#
#
# # Losses 0.125
# sim_data = np.genfromtxt('results_2,3,2_5qshuffle_errors_test0.2rrtl.csv', delimiter=',', dtype=None)
# fids = sim_data[:,0][:-1]
# errs = sim_data[:,1][:-1]
#
# plt.plot(gate_infidelity17, 1-fids, 'x', label='eps0 0.2: prrtl = 0.2')
#
#
#
# sim_data = np.genfromtxt('errors_results_2,3,2_shuffle_noloss.csv', delimiter=',', dtype=None)
# fids = sim_data[:,0][:-1]
# errs = sim_data[:,1][:-1]
#
# # plt.errorbar(gate_infidelity[:-8], 1-fids[:-8], yerr=errs[:-8], marker='.', label='[2,3,2] Perfect Loss Case', linestyle='', markersize=8, capsize=2, elinewidth=0.5)
# plt.plot(gate_infidelity20, 1-fids, '.', label='No Loss Case')
#
#
# sim_data = np.genfromtxt('results_2,3,2_5qshuffle_errors_testperfect.csv', delimiter=',', dtype=None)
# fids = sim_data[:,0][:-1]
# errs = sim_data[:,1][:-1]
#
# # plt.errorbar(gate_infidelity[:-8], 1-fids[:-8], yerr=errs[:-8], marker='.', label='[2,3,2] Perfect Loss Case', linestyle='', markersize=8, capsize=2, elinewidth=0.5)
# plt.plot(gate_infidelity17, 1-fids, '.', label='No Loss Case before phase fix')
#
#
#
# plt.yscale('log')
# plt.xscale('log')
#
# plt.xlabel('Gate Infidelity (Individual)')
# plt.ylabel('Tree Infidelity')
# plt.grid()
# plt.legend()
# plt.show()



# plt.figure(figsize=(10, 6))
#
# infid_data = np.genfromtxt('infidelities.csv', delimiter=',', dtype=None)
# gate_infidelity = infid_data[0]
# sim_data = np.genfromtxt('errors_results_2,3,2_5qshuffle_perfectloss.csv', delimiter=',', dtype=None)
# fids = sim_data[:,0][:-1]
# errs = sim_data[:,1][:-1]
#
# # plt.errorbar(gate_infidelity[:-8], 1-fids[:-8], yerr=errs[:-8], marker='.', label='[2,3,2] Perfect Loss Case', linestyle='', markersize=8, capsize=2, elinewidth=0.5)
# plt.plot(gate_infidelity, 1-fids, '.', label='No Loss Case')
#
# # Losses 0.125
# sim_data = np.genfromtxt('results_2,3,2_shuffle_errors_egtl0.01.csv', delimiter=',', dtype=None)
# fids = sim_data[:,0][:-1]
# errs = sim_data[:,1][:-1]
#
# plt.plot(gate_infidelity17, 1-fids, 'x', label='eps0 0.010: egtl = 0.01')
#
# # Losses 0.125
# sim_data = np.genfromtxt('results_2,3,2_shuffle_errors_egtl0.1.csv', delimiter=',', dtype=None)
# fids = sim_data[:,0][:-1]
# errs = sim_data[:,1][:-1]
#
# plt.plot(gate_infidelity17, 1-fids, 'x', label='eps0 0.1: egtl = 0.1')
#
# # Losses 0.125
# sim_data = np.genfromtxt('results_2,3,2_shuffle_errors_egtl0.2.csv', delimiter=',', dtype=None)
# fids = sim_data[:,0][:-1]
# errs = sim_data[:,1][:-1]
#
# plt.plot(gate_infidelity17, 1-fids, 'x', label='eps0 0.2: egtl = 0.2')
#
#
# # Losses 0.125
# sim_data = np.genfromtxt('results_2,3,2_shuffle_errors_rrtlegtl0.02.csv', delimiter=',', dtype=None)
# fids = sim_data[:,0][:-1]
# errs = sim_data[:,1][:-1]
#
# plt.plot(gate_infidelity17, 1-fids, '+', label='both 0.02')
#
# plt.yscale('log')
# plt.xscale('log')
#
# plt.xlabel('Gate Infidelity (Individual)')
# plt.ylabel('Tree Infidelity')
# plt.grid()
# plt.legend()
# plt.show()
#
#
#
# ##################################################################### Plot 2 for [2,3,2]: 3 More realistic cases
#
# plt.figure(figsize=(10, 6))
#
# # Obtain x axis
# infid_data = np.genfromtxt('infidelities.csv', delimiter=',', dtype=None)
# gate_infidelity = infid_data[0]
# sim_data = np.genfromtxt('errors_results_2,3,2_5qshuffle_perfectloss.csv', delimiter=',', dtype=None)
# fids = sim_data[:,0][:-1]
# errs = sim_data[:,1][:-1]
#
# # plt.errorbar(gate_infidelity[:-8], 1-fids[:-8], yerr=errs[:-8], marker='.', label='[2,3,2] Perfect Loss Case', linestyle='', markersize=8, capsize=2, elinewidth=0.5)
# plt.plot(gate_infidelity, 1-fids, '.', label='No Loss Case')
#
# infid_data17 = np.genfromtxt('infidelities17.csv', delimiter=',', dtype=None)
# gate_infidelity17 = infid_data17[0]
#
# # Losses 0.125
# sim_data = np.genfromtxt('results_2,3,2_shuffle_errors_mu0.028.csv', delimiter=',', dtype=None)
# fids = sim_data[:,0][:-1]
# errs = sim_data[:,1][:-1]
#
# plt.plot(gate_infidelity17, 1-fids, 'x', label='mu = 0.028 (kappas 0.99, egtl 0, rrtl 0)')
#
# # Losses 0.125
# sim_data = np.genfromtxt('errors_results_2,3,2_shuffle_eps0_0.216.csv', delimiter=',', dtype=None)
# fids = sim_data[:,0][:-1]
# errs = sim_data[:,1][:-1]
#
# plt.plot(gate_infidelity17, 1-fids, 'x', label='mu = 0.2 (kappas 0.96, egtl 0.1, rrtl 0.1)')
#
#
# # # Losses 0.125
# # sim_data = np.genfromtxt('results_2,3,2_shuffle_errors_mu0.4.csv', delimiter=',', dtype=None)
# # fids = sim_data[:,0][:-1]
# # errs = sim_data[:,1][:-1]
# #
# # plt.plot(gate_infidelity17, 1-fids, '.', label='mu = 0.4 (kappas 0.96, egtl 0.22, rrtl 0.22)')
#
#
# # # Losses 0.125
# # sim_data = np.genfromtxt('results_2,3,2_shuffle_errors_mu_high.csv', delimiter=',', dtype=None)
# # fids = sim_data[:,0][:-1]
# # errs = sim_data[:,1][:-1]
# #
# # plt.plot(gate_infidelity17, 1-fids, 'x', label='mu = high')
# #
# #
# # # Losses 0.125
# # sim_data = np.genfromtxt('results_2,3,2_shuffle_errors_mu_high1.csv', delimiter=',', dtype=None)
# # fids = sim_data[:,0][:-1]
# # errs = sim_data[:,1][:-1]
# #
# # plt.plot(gate_infidelity17, 1-fids, 'x', label='mu = high')
#
#
# # Losses 0.125
# sim_data = np.genfromtxt('results_2,3,2_shuffle_errors_mu_0.412.csv', delimiter=',', dtype=None)
# fids = sim_data[:,0][:-1]
# errs = sim_data[:,1][:-1]
#
# plt.plot(gate_infidelity17, 1-fids, 'x', label='mu = 0.412 (kappas 0.95, egtl 0.22, rrtl 0.22)')
#
# # Losses 0.125
# sim_data = np.genfromtxt('results_2,3,2_shuffle_errors_mu_0.6.csv', delimiter=',', dtype=None)
# fids = sim_data[:,0][:-1]
# errs = sim_data[:,1][:-1]
#
# plt.plot(gate_infidelity17, 1-fids, 'x', label='mu = 0.6 (kappas 0.93, egtl 0.36, rrtl 0.36)')
#
# # Losses 0.125
# sim_data = np.genfromtxt('results_2,3,2_shuffle_errors_mu_0.75.csv', delimiter=',', dtype=None)
# fids = sim_data[:,0][:-1]
# errs = sim_data[:,1][:-1]
#
# plt.plot(gate_infidelity17, 1-fids, 'x', label='mu = 0.75 (kappas 0.70, egtl 0.5, rrtl 0.5)')
#
#
#
# plt.yscale('log')
# plt.xscale('log')
#
# plt.xlabel('Gate Infidelity (Individual)')
# plt.ylabel('Tree Infidelity')
# plt.grid()
# plt.legend()
# plt.show()
#
#
#
#
# ##################################################################### Plot 2 for [4,14,4]: The effects of rrtl and egtl
#
# plt.figure(figsize=(10, 6))
#
# # Obtain x axis
# infid_data20 = np.genfromtxt('infidelities20.csv', delimiter=',', dtype=None)
# gate_infidelity20 = infid_data20[0]
# sim_data = np.genfromtxt('errors_results_4,14,4_shuffle_noloss.csv', delimiter=',', dtype=None)
# fids = sim_data[:,0][:-1]
# errs = sim_data[:,1][:-1]
#
# # plt.errorbar(gate_infidelity[:-8], 1-fids[:-8], yerr=errs[:-8], marker='.', label='[2,3,2] Perfect Loss Case', linestyle='', markersize=8, capsize=2, elinewidth=0.5)
# plt.plot(gate_infidelity20, 1-fids, '.', label='No Loss Case')
#
# # Losses 0.125
# sim_data = np.genfromtxt('errors_results_4,14,4_shuffle_rrtl0.1.csv', delimiter=',', dtype=None)
# fids = sim_data[:,0][:-1]
# errs = sim_data[:,1][:-1]
#
# plt.plot(gate_infidelity17, 1-fids, 'x', label='rrtl 0.1')
#
# # Losses 0.125
# sim_data = np.genfromtxt('errors_results_4,14,4_shuffle_rrtl0.2.csv', delimiter=',', dtype=None)
# fids = sim_data[:,0][:-1]
# errs = sim_data[:,1][:-1]
#
# plt.plot(gate_infidelity17, 1-fids, 'x', label='rrtl 0.2')
#
#
#
# #
# # # Losses 0.125
# # sim_data = np.genfromtxt('results_2,3,2_shuffle_errors_mu0.2.csv', delimiter=',', dtype=None)
# # fids = sim_data[:,0][:-1]
# # errs = sim_data[:,1][:-1]
# #
# # plt.plot(gate_infidelity17, 1-fids, '.', label='mu = 0.2 (kappas 0.96, egtl 0.1, rrtl 0.1)')
# #
# #
# # # # Losses 0.125
# # # sim_data = np.genfromtxt('results_2,3,2_shuffle_errors_mu0.4.csv', delimiter=',', dtype=None)
# # # fids = sim_data[:,0][:-1]
# # # errs = sim_data[:,1][:-1]
# # #
# # # plt.plot(gate_infidelity17, 1-fids, '.', label='mu = 0.4 (kappas 0.96, egtl 0.22, rrtl 0.22)')
# #
# #
# # # # Losses 0.125
# # # sim_data = np.genfromtxt('results_2,3,2_shuffle_errors_mu_high.csv', delimiter=',', dtype=None)
# # # fids = sim_data[:,0][:-1]
# # # errs = sim_data[:,1][:-1]
# # #
# # # plt.plot(gate_infidelity17, 1-fids, 'x', label='mu = high')
# # #
# # #
# # # # Losses 0.125
# # # sim_data = np.genfromtxt('results_2,3,2_shuffle_errors_mu_high1.csv', delimiter=',', dtype=None)
# # # fids = sim_data[:,0][:-1]
# # # errs = sim_data[:,1][:-1]
# # #
# # # plt.plot(gate_infidelity17, 1-fids, 'x', label='mu = high')
# #
# #
# # # Losses 0.125
# # sim_data = np.genfromtxt('results_2,3,2_shuffle_errors_mu_0.412.csv', delimiter=',', dtype=None)
# # fids = sim_data[:,0][:-1]
# # errs = sim_data[:,1][:-1]
# #
# # plt.plot(gate_infidelity17, 1-fids, '.', label='mu = 0.412 (kappas 0.95, egtl 0.22, rrtl 0.22)')
# #
# # # Losses 0.125
# # sim_data = np.genfromtxt('results_2,3,2_shuffle_errors_mu_0.6.csv', delimiter=',', dtype=None)
# # fids = sim_data[:,0][:-1]
# # errs = sim_data[:,1][:-1]
# #
# # plt.plot(gate_infidelity17, 1-fids, '.', label='mu = 0.6 (kappas 0.93, egtl 0.36, rrtl 0.36)')
# #
# # # Losses 0.125
# # sim_data = np.genfromtxt('results_2,3,2_shuffle_errors_mu_0.75.csv', delimiter=',', dtype=None)
# # fids = sim_data[:,0][:-1]
# # errs = sim_data[:,1][:-1]
# #
# # plt.plot(gate_infidelity17, 1-fids, '.', label='mu = 0.75 (kappas 0.70, egtl 0.5, rrtl 0.5)')
#
#
#
# plt.yscale('log')
# plt.xscale('log')
#
# plt.xlabel('Gate Infidelity (Individual)')
# plt.ylabel('Tree Infidelity')
# plt.grid()
# plt.legend()
# plt.show()
#
#








# Losses 0.125
# sim_data = np.genfromtxt('results_2,3,2_5qshuffle_errors_eps0.125.csv', delimiter=',', dtype=None)
# fids = sim_data[:,0][:-1]
# errs = sim_data[:,1][:-1]
#
# # plt.errorbar(gate_infidelity[:-8], 1-fids[:-8], yerr=errs[:-8], marker='.', label='[2,3,2] Perfect Loss Case', linestyle='', markersize=8, capsize=2, elinewidth=0.5)
# plt.plot(gate_infidelity, 1-fids, '.', label='eps0 0.125')
#
# # Losses 0.125
# sim_data = np.genfromtxt('results_2,3,2_5qshuffle_errors_eps0.3.csv', delimiter=',', dtype=None)
# fids = sim_data[:,0][:-1]
# errs = sim_data[:,1][:-1]
#
# # plt.errorbar(gate_infidelity[:-8], 1-fids[:-8], yerr=errs[:-8], marker='.', label='[2,3,2] Perfect Loss Case', linestyle='', markersize=8, capsize=2, elinewidth=0.5)
# plt.plot(gate_infidelity, 1-fids, '.', label='eps0 0.3')



#
# ##### Plot direct transmission
# direct_decode_probs, eta = decode_probability([1,1,1], etad, l_list, direct=True)
# plt.plot(eta, direct_decode_probs, label='Direct transmission')
#
# ###### Plot analytical [2,3,2]
# decode_prob, eta = decode_probability([2,3,2], etad, l_list)
# plt.plot(eta, decode_prob, label=str([2,3,2]))
#
# ##### Plot varying of rrtl loss rate
# rrtl_vary = np.genfromtxt('results_2,3,2_5qshuffle_perfect.csv', delimiter=',', dtype=None)
# # test = np.loadtxt("results7.csv", dtype=np.complex_)
# fids = rrtl_vary[:,0]
# mus = rrtl_vary[:,3][:-1]
# decp = rrtl_vary[:,2][:-1]
# errs = rrtl_vary[:,1][:-1]
#
# plt.plot(mus,decp, '.',label='[2, 3, 2] 5q shuffle ideal reflection')