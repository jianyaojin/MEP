import numpy as np
from matplotlib import pyplot as plt

def decode_probability(branching_vector, eta_d, L, L_att=20, direct=False, n=1):

    if not len(branching_vector) == 3:
        raise ValueError("We are only looking at trees with 3 levels.")


    if not direct:
        b0, b1, b2 = branching_vector

        eta = np.exp(-L/L_att)
        print(f'DEBUG: eta {eta} and  etd {eta_d}')
        mu = 1 - eta*eta_d
        R1 = 1-(1-(1-mu)*(1-mu)**b2)**b1
        R2 = 1-(1-(1-mu))**b2
        eta_e = ((1-mu+(mu*R1))**b0 - (mu*R1)**b0)*(1-mu + (mu*R2))**b1
        print(f'Debugging: mu is {mu}, R1 is {R1}, R2 is {R2}')
        print(f'Left bracket in the eta e expression is {((1-mu+mu*R1)**b0 - (mu*R1)**b0)}, right bracket {(1-mu + mu*R2)}')

        return eta_e, eta

    else:
        eta = np.exp(-L / L_att)
        eta_dir = 1-(1-eta_d*eta)**n
        return eta_dir, eta


l_list = np.arange(0,11,0.5)
etad = 1

# def strip_datastream(data):
#     """
#     Strips for instance a 35 datapoint datastream down to a 15 datapoint one
#     """
#     for i in range(len(data))

# #####################################################################   [2,3,2] Proof of variation of egtl / error does not matter
# plt.figure(figsize=(10, 6))
# # plt.title(f'[2,3,2] Decoding probability and variation of parameters')
#
# ##### Plot direct transmission
# direct_decode_probs, eta = decode_probability([1,1,1], etad, l_list, direct=True)
# plt.plot(eta, direct_decode_probs, label='Direct transmission')
#
# ###### Plot analytical [2,3,2]
# decode_prob, eta = decode_probability([2,3,2], etad, l_list)
# plt.plot(eta, decode_prob, label=str([2,3,2]))
#
# ###### Plot varying of rrtl loss rate
# rrtl_vary = np.genfromtxt('results_2,3,2_5qdumb_perfect_egtl.csv', delimiter=',', dtype=None)
# fids = rrtl_vary[:,0]
# mus = rrtl_vary[:,3][:-1]
# decp = rrtl_vary[:,2][:-1]
# errs = rrtl_vary[:,1][:-1]
#
# plt.plot(mus,decp, '.',label='[2, 3, 2] first pick, ideal reflection, vary p_egtl')
#
# ###### Plot varying of rrtl loss rate
# rrtl_vary = np.genfromtxt('results_2,3,2_5qdumb_perfect_higherror.csv', delimiter=',', dtype=None)
# fids = rrtl_vary[:,0]
# mus = rrtl_vary[:,3][:-1]
# decp = rrtl_vary[:,2][:-1]
# errs = rrtl_vary[:,1][:-1]
#
# plt.plot(mus,decp, '.',label='[2, 3, 2] first pick, ideal reflection, high error')
#
# ###### Plot varying of rrtl loss rate
# rrtl_vary = np.genfromtxt('results_2,3,2_5qdumb_perfect.csv', delimiter=',', dtype=None)
# # test = np.loadtxt("results7.csv", dtype=np.complex_)
# fids = rrtl_vary[:,0]
# mus = rrtl_vary[:,3][:-1]
# decp = rrtl_vary[:,2][:-1]
# errs = rrtl_vary[:,1][:-1]
#
# plt.plot(mus,decp, '.',label='[2, 3, 2] first pick, ideal reflection, vary p_rrtl')
#
# ###### Show graph
# plt.xlabel("Transmission probability")
# plt.ylabel("Decoding Probability")
# plt.grid()
# # plt.legend()
# plt.show()


#####################################################################   [2,3,2] Variation of r0 and r1
plt.figure(figsize=(10, 6))

##### Plot direct transmission
direct_decode_probs, eta = decode_probability([1,1,1], etad, l_list, direct=True)
plt.plot(eta, direct_decode_probs, label='Direct transmission')

###### Plot analytical [2,3,2]
decode_prob, eta = decode_probability([2,3,2], etad, l_list)
plt.plot(eta, decode_prob, label=str([2,3,2]))
#
###### Plot varying of rrtl loss rate
rrtl_vary = np.genfromtxt('results_2,3,2_5qdumb_perfect.csv', delimiter=',', dtype=None)
# test = np.loadtxt("results7.csv", dtype=np.complex_)
fids = rrtl_vary[:,0]
mus = rrtl_vary[:,3][:-1]
decp = rrtl_vary[:,2][:-1]
errs = rrtl_vary[:,1][:-1]

plt.plot(mus,decp, '.',label='[2, 3, 2] 5q first pick method ideal reflection')

# ###### Plot varying of rrtl loss rate
# rrtl_vary = np.genfromtxt('results_2,3,2_5qdumb_0.95loss.csv', delimiter=',', dtype=None)
# # test = np.loadtxt("results7.csv", dtype=np.complex_)
# fids = rrtl_vary[:,0]
# mus = rrtl_vary[:,3][:-1]
# decp = rrtl_vary[:,2][:-1]
# errs = rrtl_vary[:,1][:-1]
#
# plt.plot(mus,decp, '.',label='[2, 3, 2] 5q first pick method 0.95 loss')
#
# ###### Plot varying of rrtl loss rate
# rrtl_vary = np.genfromtxt('results_2,3,2_5qdumb_0.96loss.csv', delimiter=',', dtype=None)
# # test = np.loadtxt("results7.csv", dtype=np.complex_)
# fids = rrtl_vary[:,0]
# mus = rrtl_vary[:,3][:-1]
# decp = rrtl_vary[:,2][:-1]
# errs = rrtl_vary[:,1][:-1]
#
# plt.plot(mus,decp, '.',label='[2, 3, 2] 5q first pick method 0.96 loss')
#
# ###### Plot varying of rrtl loss rate
# rrtl_vary = np.genfromtxt('results_2,3,2_5qdumb_0.98loss.csv', delimiter=',', dtype=None)
# # test = np.loadtxt("results7.csv", dtype=np.complex_)
# fids = rrtl_vary[:,0]
# mus = rrtl_vary[:,3][:-1]
# decp = rrtl_vary[:,2][:-1]
# errs = rrtl_vary[:,1][:-1]
#
# plt.plot(mus,decp, '.',label='[2, 3, 2] 5q first pick method 0.98 loss')
#
# ###### Plot varying of rrtl loss rate
# rrtl_vary = np.genfromtxt('results_2,3,2_5qdumb_0.999loss.csv', delimiter=',', dtype=None)
# # test = np.loadtxt("results7.csv", dtype=np.complex_)
# fids = rrtl_vary[:,0]
# mus = rrtl_vary[:,3][:-1]
# decp = rrtl_vary[:,2][:-1]
# errs = rrtl_vary[:,1][:-1]
#
# plt.plot(mus,decp, '.',label='[2, 3, 2] 5q first pick method 0.999 loss')
#
# ##### Plot varying of rrtl loss rate
# rrtl_vary = np.genfromtxt('results_2,3,2_5qshuffle_perfect.csv', delimiter=',', dtype=None)
# # test = np.loadtxt("results7.csv", dtype=np.complex_)
# fids = rrtl_vary[:,0]
# mus = rrtl_vary[:,3][:-1]
# decp = rrtl_vary[:,2][:-1]
# errs = rrtl_vary[:,1][:-1]
#
# plt.plot(mus,decp, 'x',label='[2, 3, 2] 5q shuffle ideal reflection')
#
# ###### Plot varying of rrtl loss rate
# rrtl_vary = np.genfromtxt('results_2,3,2_5qshuffle_0.98loss.csv', delimiter=',', dtype=None)
# # test = np.loadtxt("results7.csv", dtype=np.complex_)
# fids = rrtl_vary[:,0]
# mus = rrtl_vary[:,3][:-1]
# decp = rrtl_vary[:,2][:-1]
# errs = rrtl_vary[:,1][:-1]
#
# plt.plot(mus,decp, 'x',label='[2, 3, 2] 5q shuffle method 0.98 loss')



###### Show graph
plt.xlabel("Transmission probability")
plt.ylabel("Decoding Probability")
plt.grid()
# plt.legend()
plt.show()




#####################################################################   [4,14,4] Variation of r0 and r1
plt.figure(figsize=(10, 6))

direct_decode_probs, eta = decode_probability([1,1,1], etad, l_list, direct=True)
plt.plot(eta, direct_decode_probs, label='Direct transmission')

###### Plot analytical [4,14,4]
decode_prob, eta = decode_probability([4,14,4], etad, l_list)
plt.plot(eta, decode_prob, label=str([4,14,4]))

##### Plot varying of rrtl loss rate
rrtl_vary = np.genfromtxt('results_4,14,4_5qdumb_perfect.csv', delimiter=',', dtype=None)
fids = rrtl_vary[:,0]
mus = etad * rrtl_vary[:,3][:-1]
decp = rrtl_vary[:,2][:-1]
errs = rrtl_vary[:,1][:-1]
plt.plot(mus,decp, '.',label='[4,14,4] 5q first pick perfect')
#
#
# ##### Plot varying of rrtl loss rate
# rrtl_vary = np.genfromtxt('results_4,14,4_5qdumb_0.999loss.csv', delimiter=',', dtype=None)
# fids = rrtl_vary[:,0]
# mus = etad * rrtl_vary[:,3][:-1]
# decp = rrtl_vary[:,2][:-1]
# errs = rrtl_vary[:,1][:-1]
# plt.plot(mus,decp, '.',label='[4,14,4] 5q dumb 0.999loss')
#
# ##### Plot varying of rrtl loss rate
# rrtl_vary = np.genfromtxt('results_4,14,4_5qdumb_0.98loss.csv', delimiter=',', dtype=None)
# fids = rrtl_vary[:,0]
# mus = etad * rrtl_vary[:,3][:-1]
# decp = rrtl_vary[:,2][:-1]
# errs = rrtl_vary[:,1][:-1]
# plt.plot(mus,decp, '.',label='[4,14,4] 5q dumb 0.98loss')
#
# ##### Plot varying of rrtl loss rate
# rrtl_vary = np.genfromtxt('results_4,14,4_5qdumb_0.95loss.csv', delimiter=',', dtype=None)
# fids = rrtl_vary[:,0]
# mus = etad * rrtl_vary[:,3][:-1]
# decp = rrtl_vary[:,2][:-1]
# errs = rrtl_vary[:,1][:-1]
# plt.plot(mus,decp, '.',label='[4,14,4] 5q dumb 0.95loss')
#
# ##### Plot varying of rrtl loss rate
# rrtl_vary = np.genfromtxt('results_4,14,4_5qdumb_0.92loss.csv', delimiter=',', dtype=None)
# fids = rrtl_vary[:,0]
# mus = etad * rrtl_vary[:,3][:-1]
# decp = rrtl_vary[:,2][:-1]
# errs = rrtl_vary[:,1][:-1]
# plt.plot(mus,decp, '.',label='[4,14,4] 5q dumb 0.92loss')
#
# ##### Plot varying of rrtl loss rate
# rrtl_vary = np.genfromtxt('results_4,14,4_5qshuffle_perfect.csv', delimiter=',', dtype=None)
# fids = rrtl_vary[:,0]
# mus = etad * rrtl_vary[:,3][:-1]
# decp = rrtl_vary[:,2][:-1]
# errs = rrtl_vary[:,1][:-1]
# plt.plot(mus,decp, 'x',label='[4,14,4] 5q shuffle perfect')
#
# ##### Plot varying of rrtl loss rate
# rrtl_vary = np.genfromtxt('results_4,14,4_5qshuffle_0.95loss.csv', delimiter=',', dtype=None)
# fids = rrtl_vary[:,0]
# mus = etad * rrtl_vary[:,3][:-1]
# decp = rrtl_vary[:,2][:-1]
# errs = rrtl_vary[:,1][:-1]
# plt.plot(mus,decp, 'x',label='[4,14,4] 5q shuffle 0.95loss')


##### Show graph
plt.xlabel("Transmission probability mu")
plt.ylabel("Decoding Probability")
plt.grid()
# plt.legend()
plt.show()



###################### LEGACY

# ###### Plot varying of rrtl loss rate 20000 runs
# rrtl_vary = np.genfromtxt('results2extra.csv', delimiter=',')
# fids = rrtl_vary[:,0]
# mus = rrtl_vary[:,3][:-1]
# decp = rrtl_vary[:,2][:-1]
# errs = rrtl_vary[:,1][:-1]
#
# # plt.plot(mus,decp, '.',label='[2, 2, 2] vary rrtl')
# plt.errorbar(mus, decp, yerr=errs, marker='.', label='[2, 2, 2] vary rrtl 20000 runs', linestyle='', markersize=8, capsize=2, elinewidth=0.5)
#
# ##### Plot varying of rrtl loss rate no error
# rrtl_vary = np.genfromtxt('results2noerror.csv', delimiter=',')
# fids = rrtl_vary[:,0]
# mus = rrtl_vary[:,3][:-1]
# decp = rrtl_vary[:,2][:-1]
# errs = rrtl_vary[:,1][:-1]
#
# # plt.plot(mus,decp, '.',label='[2, 2, 2] vary rrtl')
# plt.errorbar(mus, decp, yerr=errs, marker='.', label='[2, 2, 2] vary rrtl with no error', linestyle='', markersize=8, capsize=2, elinewidth=0.5)


# ##### Show graph
# plt.xlabel("Transmission probability mu")
# plt.ylabel("Decoding Probability")
# plt.grid()
# plt.legend()
# plt.show()

