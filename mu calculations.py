import numpy as np
from matplotlib import pyplot as plt

def gate_coefficients(system_constants, noloss=False):
	"""
	Returns values for the reflection coefficients based on the formula to work them out.

	Parameters:
	-----------
	system_constants: list
		list of coefficients in the following order:
		omega_d / gamma :
		delta1 / gamma :
		cooperativity :
		kappa / gamma :
		kappa alpha / kappa : the ratio of kappa alpha w.r.t kappa
		Delta / gamma :

	Returns:
	--------
	r0: complex float
	r1: complex float
	l0: complex float
	l1: complex float
	"""

	if noloss:
		return -1,1,0,0

	a,b,c,d,e,bigd = system_constants

	d0 = b - bigd

	print(f'Delta 0 is {d0} and Delta 1 is {b}')

	c1 = c
	c0 = c

	r_zero = 1-(e/(-1j*a/d + 1/2 + c0/(-1j*(a+d0) + 1/2)))
	r_one = 1-(e/(-1j*a/d + 1/2 + c1/(-1j*(a+b) + 1/2)))

	l_zero = (np.sqrt(1-e)*np.sqrt(e))/(-1j*a/d + 1/2 + c0/(-1j*(a+d0) + 1/2))
	l_one = (np.sqrt(1-e)*np.sqrt(e))/(-1j*a/d + 1/2 + c1/(-1j*(a+b) + 1/2))

	return r_zero, r_one, l_zero, l_one

# If running multiprocessing, this has to match the multiprocessing thread.
dont_include_error = False
dont_include_loss = False


# Error categories split into three: Emitter error, Single Qubit Gate Errors, Multi Qubit Gate Errors, Nuclear dephasing error
# error_emitter = 0.15
# error_single = 0.15
# error_multi = 0.15
# n_dephasing_sd = 0.1

# Given in order of: omega_d/gamma, delta1/gamma, cooperativity, kappa/gamma, kappa alpha / kappa, Delta / gamma
# sysconsts = [119.0, -74.0, 100, 50, 0.95, 20]


# Additional gate parameters

# r_0,r_1,l_0,l_1 = -1, 1, 0, 0

# Scattering parameters
prob_egtl = 0.22
prob_rrtl = 0.22
cross_loss_rate = 0.1

# sysconsts = [119, -74, 100, 50, 0.95, 20]
# r_0, r_1, l_0, l_1 = gate_coefficients(sysconsts, dont_include_loss)
# print(f'The actual reflection coefficients are {r_0} and {r_1}')
# print(f'refl coeffs {np.abs(r_0)} and {np.abs(r_1)}, loss coeffs {np.abs(l_0)} and {np.abs(l_1)}')


# sysconsts = [96, -75, 20, 100, 0.95, 20]
# r_0, r_1, l_0, l_1 = gate_coefficients(sysconsts, dont_include_loss)
# print(f'The actual reflection coefficients are {r_0} and {r_1}')
# print(f'The actual loss coefficients are {l_0} and {l_1}')
# print(f'refl coeffs {np.abs(r_0)} and {np.abs(r_1)}, loss coeffs {np.abs(l_0)} and {np.abs(l_1)}')
#
# sysconsts = [96, -75, 20, 100, 0.99, 20]
# r_0, r_1, l_0, l_1 = gate_coefficients(sysconsts, dont_include_loss)
# print(f'The actual reflection coefficients are {r_0} and {r_1}')
# print(f'The actual loss coefficients are {l_0} and {l_1}')
# print(f'refl coeffs {np.abs(r_0)} and {np.abs(r_1)}, loss coeffs {np.abs(l_0)} and {np.abs(l_1)}')
#
# sysconsts = [0, 0, 50, 100, 0.99, 1000]
# r_0, r_1, l_0, l_1 = gate_coefficients(sysconsts, dont_include_loss)
# print(f'The actual reflection coefficients are {r_0} and {r_1}')
# print(f'The actual loss coefficients are {l_0} and {l_1}')
# print(f'refl coeffs {np.abs(r_0)} and {np.abs(r_1)}, loss coeffs {np.abs(l_0)} and {np.abs(l_1)}')
#
# sysconsts = [0, 0, 500, 100, 0.9999, 10000]
# r_0, r_1, l_0, l_1 = gate_coefficients(sysconsts, dont_include_loss)
# print(f'The actual reflection coefficients are {r_0} and {r_1}')
# print(f'The actual loss coefficients are {l_0} and {l_1}')
# print(f'refl coeffs {np.abs(r_0)} and {np.abs(r_1)}, loss coeffs {np.abs(l_0)} and {np.abs(l_1)}')
#
# sysconsts = [0, 0, 2000, 100, 0.9999999, 1000000]
# r_0, r_1, l_0, l_1 = gate_coefficients(sysconsts, dont_include_loss)
# print(f'The actual reflection coefficients are {r_0} and {r_1}')
# print(f'The actual loss coefficients are {l_0} and {l_1}')
# print(f'refl coeffs {np.abs(r_0)} and {np.abs(r_1)}, loss coeffs {np.abs(l_0)} and {np.abs(l_1)}')

sysconsts = [96, -75, 20, 100, 0.999, 20]
r_0, r_1, l_0, l_1 = gate_coefficients(sysconsts, dont_include_loss)
print(f'The actual reflection coefficients are {r_0} and {r_1}')
print(f'The actual loss coefficients are {l_0} and {l_1}')
print(f'refl coeffs {np.abs(r_0)} and {np.abs(r_1)}, loss coeffs {np.abs(l_0)} and {np.abs(l_1)}')

sysconsts = [96, -75, 20, 100, 0.98, 20]
r_0, r_1, l_0, l_1 = gate_coefficients(sysconsts, dont_include_loss)
print(f'The actual reflection coefficients are {r_0} and {r_1}')
print(f'The actual loss coefficients are {l_0} and {l_1}')
print(f'refl coeffs {np.abs(r_0)} and {np.abs(r_1)}, loss coeffs {np.abs(l_0)} and {np.abs(l_1)}')

sysconsts = [96, -75, 20, 100, 0.95, 20]
r_0, r_1, l_0, l_1 = gate_coefficients(sysconsts, dont_include_loss)
print(f'The actual reflection coefficients are {r_0} and {r_1}')
print(f'The actual loss coefficients are {l_0} and {l_1}')
print(f'refl coeffs {np.abs(r_0)} and {np.abs(r_1)}, loss coeffs {np.abs(l_0)} and {np.abs(l_1)}')

sysconsts = [96, -75, 20, 100, 0.92, 20]
r_0, r_1, l_0, l_1 = gate_coefficients(sysconsts, dont_include_loss)
print(f'The actual reflection coefficients are {r_0} and {r_1}')
print(f'The actual loss coefficients are {l_0} and {l_1}')
print(f'refl coeffs {np.abs(r_0)} and {np.abs(r_1)}, loss coeffs {np.abs(l_0)} and {np.abs(l_1)}')