from qutip import *
import numpy as np
import matplotlib.pyplot as plt
from qutip.measurement import measure, measurement_statistics

### Trying to get the X measurement working:

def test(teststate, testnumber):
    m1counter1 = 0
    m1counter2 = 0
    m2counter1 = 0
    m2counter2 = 0

    for i in range(testnumber):
        m1result = measure(teststate, spin_x)[0]
        m2result = measure(teststate, [X0, X1])[0]
        if m1result == 1:
            m1counter1 += 1
        else:
            m1counter2 += 1

        if m2result == 0:
            m2counter1 += 1
        else:
            m2counter2 += 1

    print(f'spinx method returned the result {[m1counter1, m1counter2]}')
    print(f'projector density matrix method returned the result {[m2counter1, m2counter2]}')
    return None

zero = basis(2,0)
one = basis(2,1)
plus = (zero + one).unit()
minus = (zero - one).unit()
spin_x, spin_z = sigmax(), sigmaz()
H = 1/np.sqrt(2)*Qobj(np.array([[1,1],[1,-1]]))
Z0, Z1 = ket2dm(zero), ket2dm(one)
X0, X1 = ket2dm(plus), ket2dm(minus)
#
# state1 = tensor(plus, plus)
# state2 = tensor(zero, plus)
# problematic_state = 1/4*Qobj(np.array([-1, -1, -1, 1, -1, -1, -1, 1, -1, -1, -1, 1, -1, -1, -1, 1,]), dims=[[2,2,2,2],[1,1,1,1]])
#
# teststate = problematic_state
# testnumber = 100
#
# """
# Consider rewriting the whole state in terms of another basis
# """
# # test(teststate, testnumber)
# print(problematic_state)
# print(measure(teststate, [plus, minus], targets=[0]))



### Working things out for the Bell measurement

sigma_plus = bell_state(state='00')
sigma_minus = bell_state(state='01')
psi_plus = bell_state(state='10')
psi_minus = bell_state(state='11')

B00 = ket2dm(sigma_plus)
B01 = ket2dm(sigma_minus)
B10 = ket2dm(psi_plus)
B11 = ket2dm(psi_minus)

state = 1/2*(tensor(zero, zero, zero) + tensor(zero, zero, one) + tensor(one, one, zero) + tensor(one, one, one))
teststate = state

result = measure(teststate, [B00, B01, B10, B11], targets=[1, 2])
print(result)















