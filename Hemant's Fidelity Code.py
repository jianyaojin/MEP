# -*- coding: utf-8 -*-

import numpy as np
import math as math
import matplotlib.pyplot as plt
from qutip import *
from qutip.measurement import measure, measurement_statistics, measure_observable

X = sigmax()
Z = sigmaz()
I = qeye(2)
H = 1/math.sqrt(2)*(X+Z)

#|0><0| and |1><1| operators
kb0 = ket2dm(basis(2, 0))
kb1 = ket2dm(basis(2, 1))

"""entangling functions"""

"""
The state is of the form |psi> =  |photon>|e spin>|nuclear spin>|nuclear spin>

These are functions to entangle different qubits with each other
"""

"""
function to entangle n1 with n0 spin tensor |p>|s>|n1>|n0> to |p>|s>|nn>
"""
def cz_nu1(nspin):   
  """
  These operators apply CZ-gate to second nuclear spin, 
  similar operators are used for other entanglements
  """
  nu_0 = tensor(I, I, kb0, I)
  nu_1 = tensor(I, I, kb1, Z) 

  #entangle spin and nuclear spin n1
  sn = nu_0*nspin + nu_1*nspin
  
  return sn

"""
function to entangle spin with nuclear spin n0 |p>|s>|n> to |p>|sn>
takes in |p>|+>|n1>|n0> and entangles spin and n0 
"""
def cz_sn0(nspin):   
  sn_0 = tensor(I, kb0, I, I)
  sn_1 = tensor(I, kb1, I, Z) 

  #entangle spin and nuclear spin n1
  sn = sn_0*nspin + sn_1*nspin

  return sn

"""
function to entangle spin with nuclear spin n1 |p>|s>|n> to |p>|sn>
takes in |p>|+>|n>
"""
def cz_sn1(nspin):   
  sn_0 = tensor(I, kb0, I, I)
  sn_1 = tensor(I, kb1, Z, I) 

  #entangle spin and nuclear spin n1
  sn = sn_0*nspin + sn_1*nspin

  return sn

"""
function for spin-photon entanglement based on the entanglement protocol
takes in |+>|sn> state, the values of reflection coefficients, and returns
 CZ between |+> and |sn> = |sp>
"""
def entangle_sp(sp, r0, r1):
  z_rot = (I + 1j*Z)/(1+1j)
  sp = tensor(z_rot, I, I, I)*sp

  op1 = (0.5*(r0-r1)*basis(2,0) + 0.5*1j*(r0+r1)*basis(2,1))*basis(2,0).dag()
  op2 = (0.5*1j*(r0+r1)*basis(2,0) - 0.5*(r0-r1)*basis(2,1))*basis(2,1).dag()
  op3 = (0.5*(r0-r1)*basis(2,0) + 0.5*(r0+r1)*basis(2,1))*basis(2,0).dag()
  op4 = (0.5*(r0+r1)*basis(2,0) + 0.5*(r0-r1)*basis(2,1))*basis(2,1).dag()

  sp1 = tensor(kb0, tensor(op1, I, I) + tensor(op2, I, I))
  sp2 = tensor(1j*kb1, tensor(op3, I, I) + tensor(op4, I, I))

  sp = (sp1+sp2)*sp
  
  #single qubit rotation for perfect cz gate
  sp = (tensor(I, Z, I, I)*sp).unit()
  return sp

"""functions for measurement"""

"""
The measure function collapses the input state, 
so if we measure |+> state in z basis, the ouput state is 
either |1> or |0> with some probability. Therefore i apply 
gates to the output state so that the output state can be 
used to entangle again
"""

"""
measures photon in z-basis and returns |+>|sn> state
"""
def measure_z_ph(sp):
  #|0><0| and |1><1| operators
  pz0 = tensor(kb0, I, I, I)
  pz1 = tensor(kb1, I, I, I)

  #measurement in z-basis using pz0 and  pz1
  (mea_output, sn1) = measure(sp, [pz0, pz1])
  
  #apply gates to return |+>|sn> given the measurement output
  if mea_output == 0:
    sn1 = tensor(H, I, I, I)*sn1
    mea_output = 1
  else:
    sn1 = tensor(H*X, I, I, I)*sn1  
    mea_output = -1
    
  return mea_output, sn1

"""
measures photon in x-basis and returns value in |+>|sn> state
"""
def measure_x_ph(sp):
  #|+><+| and |-><-| operators
  plus = (basis(2,0)+basis(2,1)).unit()
  minus = (basis(2,0)-basis(2,1)).unit()
  px0 = tensor(ket2dm(plus), I, I, I)
  px1 = tensor(ket2dm(minus), I, I, I)

  #measurement in x-basis parameterized by px0 and px1
  (mea_output, sn1) = measure(sp, [px0, px1])
  
  #apply gates to return to a specific state
  if mea_output == 1:
    sn1 = tensor(Z, I, I, I)*sn1
    mea_output = -1
  else:
    mea_output = 1  
  
  return mea_output, sn1

"""
measures spin in x-basis and returns value in |p>|+>|n> state
"""
def measure_x_spin(sp):
  #|+><+| and |-><-| operators
  plus = (basis(2,0)+basis(2,1)).unit()
  minus = (basis(2,0)-basis(2,1)).unit()
  #measuring spin in x-basis
  px0 = tensor(I, ket2dm(plus), I, I)
  px1 = tensor(I, ket2dm(minus), I, I)

  #measurement in x-basis
  (mea_output, sn1) = measure(sp, [px0, px1])

  #apply gates to return to specific state
  if mea_output == 1:
    mea_output = -1
    sn1 = tensor(I, Z, I, I)*sn1
  else:
    mea_output = 1

  return mea_output, sn1

"""
measures n1 in x-basis and returns value in |sp>|+>|n0> state
"""
def measure_x_n1(sp):
  #|+><+| and |-><-| operators
  plus = (basis(2,0)+basis(2,1)).unit()
  minus = (basis(2,0)-basis(2,1)).unit()
  #operator for measuring n1
  px0 = tensor(I, I, ket2dm(plus), I)
  px1 = tensor(I, I, ket2dm(minus), I)

  #measurement in x-basis
  (mea_output, sn1) = measure(sp, [px0, px1])

  #apply gates to return to specific state
  if mea_output == 1:
    mea_output = -1
    sn1 = tensor(I, I, Z, I)*sn1
  else:
    mea_output = 1

  return mea_output, sn1
  
"""
measures n0 in x-basis and returns value in |sp>|n1>|+> state
"""
def measure_x_n0(sp):
  #|+><+| and |-><-| operators
  plus = (basis(2,0)+basis(2,1)).unit()
  minus = (basis(2,0)-basis(2,1)).unit()
  #operator for measuring n1
  px0 = tensor(I, I, I, ket2dm(plus))
  px1 = tensor(I, I, I, ket2dm(minus))

  #measurement in x-basis
  (mea_output, sn1) = measure(sp, [px0, px1])

  #apply gates to return to specific state
  if mea_output == 1:
    mea_output = -1
    sn1 = tensor(I, I, I, Z)*sn1
  else:
    mea_output = 1

  return mea_output, sn1


"""functions for SWAP operations """

"""
swaping state of photon with spin
input state |photon>|spin> = |+>(a|0>+b|1>) and gives out (a|0>+b|1>)|+>
iirc it is based on teleportation circuit

a and b could carry information about nuclear spins
"""

"""
Because we are swapping spin photon state,
the input to this function are the two reflection
coefficients
"""
def swap_sp(sp, r0, r1):
  #sp = tensor(H, I, I, I)*sp
  sp = entangle_sp(sp, r0, r1)
  sp = tensor(H, I, I, I)*sp
  mea, sp = measure_x_spin(sp)
  
  if mea == -1:
    sp = tensor(Z, I, I, I)*sp

  return sp  

"""swap state of spin with n1 nuclear spin
input state is |p>|+>|n> and output is swap between s and n1
output state of n1 is |+>
"""
def swap_sn1(sp):
  sp = cz_sn1(sp)    #spin is already entangled in the for loop
  sp = tensor(I, H, I, I)*sp
  mea, sp = measure_x_n1(sp)

  if mea == -1:
    sp = tensor(I, Z, I, I)*sp

  return sp  

"""swap state of spin with n0 nuclear spin
input state is |p>|+>|n> and output is swap between s and n0
output state of n0 is |+>
"""
def swap_sn0(sp):
  sp = cz_sn0(sp)
  sp = tensor(I, H, I, I)*sp
  mea, sp = measure_x_n0(sp)
  
  if mea == -1:
    sp = tensor(I, Z, I, I)*sp

  return sp


"""functions for generating and measuring photons in a tree"""

"""
This part creates the tree cluster state acc. to 
"tree generation protocol". We measure the photons 
as they are entangled/scattered off the cavity and we save the result.
The measurement results are saved in an array depending
on the branching parameter.
"""

"""
takes in |+>|+>|+>|+> and creates tree, measures stabilizer0 
returns array of outputs
"""
def tree_measure0(sp, r0, r1, b, m_z):
  #a=0
  for l in range(b[0]):
    sp = cz_nu1(sp)

    for i in range(b[1]):
      sp = cz_sn1(sp)

      for j in range(b[2]):
        sp1 = entangle_sp(sp, r0, r1)
        #a=a+1
        mea_output, sp = measure_x_ph(sp1)  #returns ph in |+>
        m_z[3*l+2, i*b[2]+j] = mea_output
      
      #spin in |+> state and others entangled after this operation
      sp1 = swap_sp(sp, r0, r1)   
      #a=a+1  
      mea_output, sp = measure_z_ph(sp1)
      m_z[3*l+1, i*b[2]] = mea_output

    #n1 is in |+> after swap
    sp = swap_sn1(sp)

    #spin is in |+> after swap
    sp = swap_sp(sp, r0, r1)
    #a=a+1
    #after this step the state should be, |+>|+>|+>|0 or 1>
    mea_output, sp = measure_x_ph(sp)
    m_z[3*l,0] = mea_output

  #state is in |+>|s>|+>|+> after this operation
  sp = swap_sn0(sp)

  #state is |p>|+>|+>|+> after this operation
  sp = swap_sp(sp, r0, r1)
  #a=a+1
  #measure the state of photon in z
  mea_output, sp = measure_z_ph(sp)
  m_z[0,b[1]*b[2]-1] = mea_output
  #print(a)
  return m_z


"""
takes in |+>|+>|+>|+> and creates tree, measures stabilizer1
returns array of outputs
"""
def tree_measure1(sp, r0, r1, b, m_z):
  #a=0
  for l in range(b[0]):
    sp = cz_nu1(sp)

    for i in range(b[1]):
      sp = cz_sn1(sp)

      for j in range(b[2]):
        sp1 = entangle_sp(sp, r0, r1)
        mea_output, sp = measure_z_ph(sp1)  #returns ph in |+>
        #a = a+1
        m_z[3*l+2, i*b[2]+j] = mea_output
      
      #spin in |+> state and others entangled after this operation
      sp1 = swap_sp(sp, r0, r1)   
      #a=a+1  
      mea_output, sp = measure_x_ph(sp1)
      m_z[3*l+1, i*b[2]] = mea_output

    #n1 is in |+> after swap
    sp = swap_sn1(sp)

    #spin is in |+> after swap
    sp = swap_sp(sp, r0, r1)
    #a=a+1
    #after this step the state should be, |+>|+>|+>|0 or 1>
    mea_output, sp = measure_z_ph(sp)
    m_z[3*l,0] = mea_output

  #state is in |+>|s>|+>|+> after this operation
  sp = swap_sn0(sp)

  #state is |p>|+>|+>|+> after this operation
  sp = swap_sp(sp, r0, r1)
  #a=a+1
  #measure the state of photon in z
  mea_output, sp = measure_x_ph(sp)
  m_z[0,b[1]*b[2]-1] = mea_output
  #print(a)
  return m_z


"""functions for stabilizers measurement results"""

"""
The measurement outputs from the previous two functions
are used to calculate the result of stabilizer measurement
"""

#measuring stabilizers zxzx
def stabilizer0(b, m_z):
  s = []
  r = []
  sm = 1
  for j in range(b[0]):
    #print(j)
    for i in range(b[1]*b[2]):
      a = m_z[3*j+2, i]*m_z[3*j+1, b[2]*math.floor(i/b[2])]
      s.append(a) 

  for j in range(b[0]):
    #print(j)
    a = 1*m_z[3*j, 0]
    for i in range(b[1]):
      a = a*m_z[3*j+1, b[2]*i]
    a = a*m_z[0, b[1]*b[2]-1]
    r.append(a)

  for i in range(len(s)):
    s[i] = (s[i]+1)/2
    sm = sm*s[i]
  
  for i in range(len(r)):
    r[i] = (r[i]+1)/2
    sm = sm*r[i]
  
  return sm

#measuring stabilizers xzxz
def stabilizer1(b, m_z):
  s = []
  r = []
  sm = 1
  for j in range(b[0]):
    for i in range(b[1]):
      a = m_z[3*j, 0]*m_z[3*j+1, i*b[2]]
      for k in range(b[2]):
        a = a*m_z[3*j+2, i*b[2]+k]
      s.append(a)

  a = m_z[0, b[1]*b[2]-1]
  for i in range(b[0]):
    a = a*m_z[3*i, 0]
  r.append(a)
  
  for i in range(len(s)):
    s[i] = (s[i]+1)/2
    sm = sm*s[i]
  
  for i in range(len(r)):
    r[i] = (r[i]+1)/2
    sm = sm*r[i]

  return sm



"""
The 'main' part of the code
"""

#branching parameter
b = [2, 3, 2]
#array for measurement outputs
m_z = np.zeros((b[0]*(len(b)), b[2]*b[1]))

#initialize 
plus = (basis(2, 0) + basis(2, 1)).unit()
zero = basis(2, 0)

l = 1

x = np.zeros(l)
fid = np.zeros(l)
efid = np.zeros(l)

#initialize spin-photon state
for j in range(l):
  
  ef = 0.9958107306432444
  r0 = -0.9975801700615283-0.06663961543507524j
  r1 = 0.9915262604647272-0.08250223808938412j
  
  #number of tries of monte carlo simulation
  n = 750   
  m = 0
  x[j] = n
  
  for i in range(n):
    sp = tensor(plus, plus, plus, plus)

    #create tree and get array of measurements
    m_z = tree_measure0(sp, r0, r1, b, m_z)
    
    #stabilizer output
    s = stabilizer0(b, m_z)
    m = m+s
  
  for i in range(n):
    sp = tensor(plus, plus, plus, plus)

    #create tree and get array of measurements
    m_z = tree_measure1(sp, r0, r1, b, m_z)
    
    #stabilizer output
    s = stabilizer1(b, m_z)
    m = m+s
  
  m = m/n  
  print(m-1, n, ef)
  fid[j] = m-1
  efid[j] = ef

print(fid)
print(efid)
