import numpy as np
from matplotlib import pyplot as plt
from qutip import *
import random
from qutip.measurement import measure, measurement_statistics
from collections import Counter as Majoritycount
import time
import cProfile

"""
This file is split into two major sections: The functions required to run the scheme and the running section itself.
The functions section is split into two more sections, one containing mostly basic gate functions (such as hadamard,
cnot, etc.) with some more complex operations mixed in, and another one with process functions: functions that run for
example the generation of one entire branch of the tree, or initialize the tree etc. 

Eventually, this file should be split into several smaller files for ease of reading.

Notation note: The expression |qubit 1> |qubit 2> |qubit 3> is shorthand for |qubit 1> tensor |qubit 2> tensor |qubit 3>

For those unfamiliar with qutip, some pointers to improve readability:
- Qutip works with the Qobj() class. All references to Qobj are linked to Qutip. This object contains the information 
for a specific state or operator, including the vector / matrix that describes it, its shape and the number of qubits 
involved.
- To check the basis size that a certain Qobj works in, refer to the "dims" parameter (if dims is not a parameter in a 
Qobj call, you can usually tell the basis size from the matrix shape, or by the context by looking for the tensor 
product tensor() function). Some examples are: dims = [[2],[2]] means 1 qubit basis operator,[[2,2],[2,2]] means 2 
qubit basis operator, [[2,2,2],[2,2,2]] means 3 qubit basis operator etc. [[2],[1]] means 1 qubit state vector, 
[[2,2],[1,1]] means 2 qubit state vector, [[2,2,2],[1,1,1]] means 3 qubit state vector.

This program works with the last decoding principle. That is: it is assumed that the decoding branch is always the last
branch that is visited.
"""

def isnorm(qobject):
	"""
	Debugging function. Checks if given quantum object is normalized
	"""
	totalprob = np.sum(np.abs(np.array(qobject)) ** 2)
	return totalprob==1.0, totalprob

def en_detached_checker(state):
	"""
	Function runs similarly to discard_electron_nuclear_spins(). Checks if the electron and nuclear qubits are indeed
	detached from the rest of the system.
	"""
	print(f'/\/\/\/\/\/\/\/\/\/\/\/\/\/\/')
	arr = np.array(state).T[0]
	nflag = True
	eflag = True
	if len(arr) == 8:
		print(f'EN_Detached_Checker: 3 qubit')
		e_zero = arr[:4]
		e_one = arr[4:]
		if not (abs(e_zero) == abs(e_one)).all():
			print(f'>>>>>>>>>>>>>>>>>>>>>>>>>>>Electron Is not detached')
			eflag = False

		n_zero = np.concatenate((arr[:2],arr[4:6]))
		n_one = np.concatenate((arr[2:4], arr[6:]))

		if not (abs(n_zero) == abs(n_one)).all():
			print(f'>>>>>>>>>>>>>>>>>>>>>>>>>>>Nuclear Spin Is not detached')
			nflag = False

	elif len(arr) == 16:
		print(f'EN_Detached_Checker: 4 qubit')
		e_zero = np.concatenate((arr[:4], arr[8:12]))
		e_one = np.concatenate((arr[4:8], arr[12:]))

		if not (abs(e_zero) == abs(e_one)).all():
			print(f'>>>>>>>>>>>>>>>>>>>>>>>>>>>Electron Is not detached')
			eflag = False

		n_zero = np.concatenate((arr[:2],arr[4:6],arr[8:10],arr[12:14]))
		n_one = np.concatenate((arr[2:4],arr[6:8],arr[10:12],arr[14:]))

		if not (abs(n_zero) == abs(n_one)).all():
			print(f'>>>>>>>>>>>>>>>>>>>>>>>>>>>Nuclear Spin Is not detached')
			nflag = False

	else:
		raise AssertionError("Input array is neither 3 qubit nor 4 qubit basis.")

	if eflag and nflag:
		print(f'Both your nuclear and electron spins are detached from the system.')
	print(f'/\/\/\/\/\/\/\/\/\/\/\/\/\/\/')
	return eflag, nflag

# Basic gate functions
def draw_errvals(theta_sd = 0.5, zero_error=False):
	"""
	Function draws values for theta and nx, ny, nz. Theta is drawn from a normal distribution with standard deviation
	given by theta_sd (By default it is 0.1), and nx, ny, nz are drawn to produce a random axis/vector on the Bloch
	sphere

	Parameters:
	-------
	theta_sd: float
		optional input setting the standard deviation of the normal distribution to draw theta from

	zero_error: True or False
		optionally, True to return [0, 0, 0, 0] (no error at all), False to model error.

	Returns:
	--------
	list of errors: list
		a list of 4 values ordered: [theta, nx, ny, nz]

	"""
	if zero_error:
		return [0, 0, 0, 0]
	theta = np.random.normal(0, theta_sd)
	n1 = np.random.uniform(-1, 1)
	n2 = np.random.uniform(-1, 1)
	n3 = np.random.uniform(-1, 1)
	normalization = np.sqrt(n1 ** 2 + n2 ** 2 + n3 ** 2)
	nx = n1 / normalization
	ny = n2 / normalization
	nz = n3 / normalization
	return [theta, nx, ny, nz]

def errored_H(errvals):
	"""
	Function generates an errored Hadamard operation matrix given a set of error values [theta, nx, ny, nz]. The error
	is modelled as RH, where R is the bloch sphere rotation matrix.	The Hadamard expression is a 2x2,
	1 qubit basis matrix.

	Parameters:
	-----------
	errvals: list of errors
		direct output from draw_errvals() in the format [theta, nx, ny, nz]
		
	Returns:
	--------
	Quantum Operator Object: 1 qubit Qobj
		Qutip quantum object (operator) with size 2x2
	"""
	theta, nx, ny, nz = errvals
	H1 = np.cos(theta/2) - 1j*np.sin(theta/2)*(nz + nx - 1j*ny)
	H2 = np.cos(theta/2) + 1j*np.sin(theta/2)*(-nz + nx - 1j*ny)
	H3 = np.cos(theta/2) + 1j*np.sin(theta/2)*(nz - nx - 1j*ny)
	H4 = -np.cos(theta/2) - 1j*np.sin(theta/2)*(nz + nx + 1j*ny)
	return Qobj(1/np.sqrt(2)*np.array([[H1,H2],[H3,H4]]))

def errored_Z(errvals):
	"""
	Function generates an errored Z gate operation matrix given a set of error values [theta, nx, ny, nz]. The error
	is modelled as RZ, where R is the bloch sphere rotation matrix.	The Z gate expression is a 2x2,
	1 qubit basis matrix.

	Parameters:
	-----------
	errvals: list of errors
		direct output from draw_errvals() in the format [theta, nx, ny, nz]
		
	Returns:
	--------
	Quantum Operator Object: 1 qubit Qobj
		Qutip quantum object (operator) with size 2x2
	"""
	theta, nx, ny, nz = errvals
	Z1 = np.cos(theta / 2) - 1j * np.sin(theta / 2) * (nz)
	Z2 = 1j * np.sin(theta / 2) * (nx - 1j * ny)
	Z3 = -1j * np.sin(theta / 2) * (nx + 1j * ny)
	Z4 = -np.cos(theta / 2) - 1j * np.sin(theta / 2) * (nz)
	return Qobj(np.array([[Z1, Z2], [Z3, Z4]]))

def errored_xrotate(errvals):
	"""
	Function generates an errored rotation gate operation matrix given a set of error values [theta, nx, ny, nz]. The
	rotation gate is that of a rotation over the negative x-axis about an angle of pi/2. The error is modelled as RM, 
	where R is the bloch sphere rotation matrix and M is our desired rotation. The gate expression is a 2x2,
	1 qubit basis matrix.

	Parameters:
	-----------
	errvals: list of errors
		direct output from draw_errvals() in the format [theta, nx, ny, nz]
		
	Returns:
	--------
	Quantum Operator Object: 1 qubit Qobj
		Qutip quantum object (operator) with size 2x2
	
	"""
	theta, nx, ny, nz = errvals
	R1 = np.cos(theta/2) - 1j*np.sin(theta/2)*(nz + ny + 1j*nx)
	R2 = 1j*np.cos(theta/2) + np.sin(theta/2)*(nz - ny - 1j*nx)
	R3 = 1j*np.cos(theta/2) + np.sin(theta/2)*(-nz + ny - 1j*nx)
	R4 = np.cos(theta/2) + 1j*np.sin(theta/2)*(nz + ny - 1j*nx)
	return Qobj(1/np.sqrt(2) * np.array([[R1, R2], [R3, R4]]))

def errored_rotate(errvals):
	"""
	Function generates the expression for the generic bloch sphere rotation matrix R_theta, given a set of input
	parameters [theta, nx, ny, nz]
	
	Parameters:
	-----------
	errvals: list of errors
		direct output from draw_errvals() in the format [theta, nx, ny, nz]
		
	Returns:
	--------
	Quantum Operator Object: 1 qubit Qobj
		Qutip quantum object (operator) with size 2x2
	"""
	theta, nx, ny, nz = errvals
	return np.cos(theta/2)*qeye(2) - 1j*np.sin(theta/2)*Qobj(np.array([[nz, nx-1j*ny],[nx + 1j*ny, -nz]]), dims=[[2], [2]])

def carbon_electron_cphase(errored_hadamard, errvals):
	"""
	Function produces matrix that performs a CPhase operation between a carbon and an electron spin. The output matrix is
	written in the basis |electron> |nuclear> |carbon>. The CPhase operation is performed by the steps: 1) Hadamard
	(errored) on the electron, 2) CcNOTe, 3) Hadamard (errored) on the electron.
	
	Note the inclusion of the nuclear spin in the basis. The operator only acts on the electron and carbon basis, but
	the nuclear is included to keep the basis in line with the rest of the code, i.e. we want the output to be directly
	usable by the other functions. This is the only function that "skips over" a qubit when it operates, so we peer
	pressure it into conforming into the same format as all other operations.
	
	Parameters:
	-----------
	errored_hadamard: Qobj
		direct output of the function errored_H(), or alternatively a qutip quantum object of a Hadamard gate
		
	errvals: list of errors
		direct output from draw_errvals() in the format [theta, nx, ny, nz]
		
	Returns:
	--------
	Quantum operator object: Qobj
		qutip quantum object operator matrix (8x8) that acts on the 3 qubit basis |electron> |photon> |carbon>
	"""
	H_basis = tensor(errored_hadamard, qeye(2), qeye(2))
	theta, nx, ny, nz = errvals

	CcNOTeErrored = np.array([[0,0,0,0,1,0,0,0],[0,1,0,0,0,0,0,0],[0,0,0,0,0,0,1,0],[0,0,0,1,0,0,0,0],[1,0,0,0,0,0,0,0],[0,0,0,0,0,1,0,0],[0,0,1,0,0,0,0,0],[0,0,0,0,0,0,0,1]])
	errorterm = np.cos(theta/2)*np.identity(8) - 1j*np.sin(theta/2)*CcNOTeErrored
	CcNOTe = np.array([[1,0,0,0,0,0,0,0],[0,0,0,0,0,1,0,0],[0,0,1,0,0,0,0,0],[0,0,0,0,0,0,0,1],[0,0,0,0,1,0,0,0,],[0,1,0,0,0,0,0,0],[0,0,0,0,0,0,1,0],[0,0,0,1,0,0,0,0]])
	errored_expr = Qobj(errorterm @ CcNOTe, dims=[[2,2,2],[2,2,2]])
	return H_basis*errored_expr*H_basis

def electron_nuclear_cphase(errored_hadamard, errvals):
	"""
	Function produces the matrix that performs a CPhase operation between electron and nuclear spins. The matrix acts
	in the basis given by |electron> |nuclear>

	Parameters:
	-----------
	errored_hadamard: Qobj
		direct output of the function errored_H(), or alternatively a qutip quantum object of a Hadamard gate

	errvals: list of errors
		direct output from draw_errvals() in the format [theta, nx, ny, nz]

	Returns:
	--------
	Quantum Operator Object: 2 qubit Qobj
		Qutip quantum object (operator) with size 4x4
	"""
	Hprime = tensor(errored_hadamard, qeye(2))
	CnNOTe = Qobj(np.array([[1,0,0,0],[0,0,0,1],[0,0,1,0],[0,1,0,0]]), dims=[[2,2],[2,2]])
	return Hprime * CnNOTe * Hprime


def electron_nuclear_swap(errvals):
	"""
	Function generates the gate operator matrix that swaps the states of the electron and nuclear spins. Basis is given
	in: |electron> |nuclear>.

	The operation consists of three alternating CNOT gates. To minimize error, the orientation of CNOT gates are as
	follows: CnNOTe, CeNOTn, CnNOTe.

	Parameters:
	-----------
	errvals: list of errors
		direct output from draw_errvals() in the format [theta, nx, ny, nz]

	Returns:
	--------
	Quantum Operator Object: 2 qubit Qobj
		Qutip quantum object (operator) with size 4x4
	"""
	theta, nx, ny, nz = errvals
	mat = np.array([[np.cos(theta/2),0,0,-1j*np.sin(theta/2)],[0,0,-1j*np.sin(theta/2) + np.cos(theta/2),0],[0, -1j*np.sin(theta/2) + np.cos(theta/2), 0, 0],[-1j*np.sin(theta/2),0,0,np.cos(theta/2)]])
	return Qobj(mat, dims=[[2, 2], [2, 2]])

def electron_photon_entangle(H, R, Z, refl_coeffs, nuclear_dephasing_thetas=None, op_basis_size=4):
	"""
	Function produces a matrix that performs the entangling operation between an electron spin and a photon. The matrix
	operation in full is exactly that of described by the process of scattering a photon off of the gate (spin-cavity
	system), assuming the photon is not lost somewhere during the process.

	Since at this point in the algorithm we will be working with our maximum basis size of 4 qubits, we note that the
	matrix will work in the basis |photon> |electron> |nuclear> |carbon>.

	The early and late scattering matrices are only unitary in the case r_0 = -1 and r_1 = 1.

	Parameters:
	-----------
	H: Qobj
		direct output of errored_H()

	R: Qobj
		direct output of errored_R()

	Z: Qobj
		direct output of errored_Z()

	refl_coeffs: list of 2 values
		list of 2 values given [r_0, r_1]

	Returns:
	--------
	Quantum operator object: Qobj (4 qubit)
		matrix operator Qobj that entangles a photon and electron spin together, including errors in single qubit gate
		operations, assuming the photon is not lost	somewhere

	Function takes in the errored matrices for Hadamard, rotation of pi/2 over -x axis and Z, and returns the
	full photon electron entangling operation provided that photon loss does not occur. refl_coeffs is [r_0,r_1]
	Important to note is that this operation will always be used on a 4 qubit basis. We decide that this 4 qubit basis
	will be arranged as:
	"""

	if op_basis_size == 4:
		if nuclear_dephasing_thetas is not None:
			if isinstance(nuclear_dephasing_thetas, np.ndarray):
				th00, th01, th10, th11 = nuclear_dephasing_thetas
				d_basis_diagonal_early = np.array([np.exp(1j*th00), np.exp(1j*th01), np.exp(1j*th10), np.exp(1j*th11), np.exp(1j*th00), np.exp(1j*th01), np.exp(1j*th10), np.exp(1j*th11), 1, 1, 1, 1, 1, 1, 1, 1])
				D_basis_early = Qobj(np.eye(16) * d_basis_diagonal_early, dims=[[2, 2, 2, 2], [2, 2, 2, 2]])
				d_basis_diagonal_late = np.array([1, 1, 1, 1, 1, 1, 1, 1, np.exp(1j * th00), np.exp(1j * th01), np.exp(1j * th10), np.exp(1j * th11), np.exp(1j * th00),np.exp(1j * th01), np.exp(1j * th10), np.exp(1j * th11)])
				D_basis_late = Qobj(np.eye(16) * d_basis_diagonal_late, dims=[[2, 2, 2, 2], [2, 2, 2, 2]])
			else:
				raise ValueError("The format for the thetas should be a numpy array")
		else:
			# print(f'No nuclear dephasing')
			D_basis_early = tensor(qeye(2),qeye(2),qeye(2),qeye(2))
			D_basis_late = tensor(qeye(2),qeye(2),qeye(2),qeye(2))

		# print(f'Debug desphasing matrix looks like {D_basis}')
		r0, r1 = refl_coeffs
		H_basis = tensor(qeye(2), H, qeye(2), qeye(2))
		Z_basis = tensor(qeye(2), Z, qeye(2), qeye(2))
		R_basis = tensor(qeye(2), R, qeye(2), qeye(2))

		ES = Qobj(np.array([[r0,0,0,0],[0,r1,0,0],[0,0,1,0],[0,0,0,1]]), dims = [[2,2],[2,2]])
		LS = Qobj(np.array([[1,0,0,0],[0,1,0,0],[0,0,r0,0],[0,0,0,r1]]), dims = [[2,2],[2,2]])

		ES_basis = tensor(ES, qeye(2), qeye(2))
		LS_basis = tensor(LS, qeye(2), qeye(2))

	elif op_basis_size == 5:
		if nuclear_dephasing_thetas is not None:
			if isinstance(nuclear_dephasing_thetas, np.ndarray):
				th00, th01, th10, th11 = nuclear_dephasing_thetas
				d_basis_diagonal_early = np.array(
					[np.exp(1j * th00), np.exp(1j * th01), np.exp(1j * th10), np.exp(1j * th11), np.exp(1j * th00),
					 np.exp(1j * th01), np.exp(1j * th10), np.exp(1j * th11), np.exp(1j * th00), np.exp(1j * th01), np.exp(1j * th10), np.exp(1j * th11), np.exp(1j * th00),
					 np.exp(1j * th01), np.exp(1j * th10), np.exp(1j * th11), 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1])
				D_basis_early = Qobj(np.eye(32) * d_basis_diagonal_early, dims=[[2, 2, 2, 2, 2], [2, 2, 2, 2, 2]])
				d_basis_diagonal_late = np.array(
					[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, np.exp(1j * th00), np.exp(1j * th01), np.exp(1j * th10), np.exp(1j * th11),
					 np.exp(1j * th00), np.exp(1j * th01), np.exp(1j * th10), np.exp(1j * th11), np.exp(1j * th00), np.exp(1j * th01), np.exp(1j * th10), np.exp(1j * th11),
					 np.exp(1j * th00), np.exp(1j * th01), np.exp(1j * th10), np.exp(1j * th11)])
				D_basis_late = Qobj(np.eye(32) * d_basis_diagonal_late, dims=[[2, 2, 2, 2, 2], [2, 2, 2, 2, 2]])
			else:
				raise ValueError("The format for the thetas should be a numpy array")
		else:
			# print(f'No nuclear dephasing')
			D_basis_early = tensor(qeye(2), qeye(2), qeye(2), qeye(2), qeye(2))
			D_basis_late = tensor(qeye(2), qeye(2), qeye(2), qeye(2), qeye(2))

		# print(f'Debug desphasing matrix looks like {D_basis}')
		r0, r1 = refl_coeffs
		H_basis = tensor(qeye(2), qeye(2), H, qeye(2), qeye(2))
		Z_basis = tensor(qeye(2), qeye(2), Z, qeye(2), qeye(2))
		R_basis = tensor(qeye(2), qeye(2), R, qeye(2), qeye(2))

		ES = Qobj(np.array([r0,r1,r0,r1,1,1,1,1])*np.eye(8), dims=[[2, 2, 2], [2, 2, 2]])
		LS = Qobj(np.array([1,1,1,1,r0,r1,r0,r1])*np.eye(8), dims=[[2, 2, 2], [2, 2, 2]])

		ES_basis = tensor(ES, qeye(2), qeye(2))
		LS_basis = tensor(LS, qeye(2), qeye(2))

	else:
		raise NotImplementedError("Currently we only support entangling circuits for a basis containing 4 or 5 qubits")

	return Z_basis * R_basis * H_basis * D_basis_late * LS_basis * H_basis * D_basis_early * ES_basis * R_basis



# Scheme progress functions
def init_ptcs(coefficients, custom_state=None):
	"""
	Function initializes the photonic tree cluster state, by setting up three spin qubits and entangling them (with
	error). Electron refers to SiV spin, Nuclear refers to nuclear spin, and Carbon refers to C13 Auxilliary spin

	Entangling happens by means of three steps: first a CPhase between the carbon and electron, then an electron nuclear
	swap, followed by a electron nuclear cphase.

	Parameters:
	-----------
	coefficients: list
		System coefficients fed in from generate_tree()

	custom_state: Qobj state
		optional input: If this is the first time running the function, i.e. we are starting with absolutely nothing,
		leave this input on None. If we have just completed the measurement of a level 1 photon and want to re-entangle
		a given state of 3 separate qubits (the electron, nuclear, and carbon spins), input this quantum object state
		in the basis |electron>|nuclear>|carbon> under custom_state.

	Returns:
	--------
	Quantum object state vector: Qobj state vector (3 qubit)
		Returns a Qobj class object representing the state vector of a state consisting of three entangled qubit spins.

	Examples:
	---------
	Leaving custom_state=None, the function initializes our two memory spins and one electron spin in the following way:
	|+>|+>|+>, entangles them,

	We can for example choose to provide a state our_state to custom_state. We expect our_state then to look similar
	to the None case above, for example like this: -|->|+>|->

	"""

	error_single, error_multi = coefficients[8], coefficients[9]

	# print(f'Input custom state is {custom_state}')
	if custom_state is None:
		# print(f'This is the first time the state is initialized, using |+> states:')
		electron_state = (basis(2, 0) + basis(2, 1)).unit()
		nuclear_state = (basis(2, 0) + basis(2, 1)).unit()
		carbon_state = (basis(2, 0) + basis(2, 1)).unit()

		separate_qubits_state = tensor(electron_state, nuclear_state, carbon_state)
	else:
		# print(f're-entangling state from previous run...')
		separate_qubits_state = custom_state

	qubit_number = int(np.log(separate_qubits_state.shape[0]) / np.log(2))
	if qubit_number != 3 and qubit_number != 4:
		raise ValueError("The state you want to initialize isn't either a 3 qubit state or a 4 qubit state!")

	H = errored_H(draw_errvals(theta_sd=error_single, zero_error=dont_include_error))
	CECPhase = carbon_electron_cphase(H, draw_errvals(theta_sd=error_multi, zero_error=dont_include_error))

	# Apply Carbon Electron CPhase as step 1 of the initialization entangling protocol
	if qubit_number == 3:
		entangled_state1 = CECPhase * separate_qubits_state
	else:
		entangled_state1 = tensor(qeye(2), CECPhase) * separate_qubits_state

	# Apply electron nuclear swap operation as step 2
	if qubit_number == 3:
		CeNOTn = tensor(electron_nuclear_swap(draw_errvals(theta_sd=error_multi, zero_error=dont_include_error)), qeye(2))
		entangled_state2 = CeNOTn * entangled_state1
	else:
		CeNOTn = tensor(qeye(2), electron_nuclear_swap(draw_errvals(theta_sd=error_multi, zero_error=dont_include_error)), qeye(2))
		entangled_state2 = CeNOTn * entangled_state1

	# Apply electron nuclear cphase to finish the initialization (step 3)
	NECPhase = electron_nuclear_cphase(H, draw_errvals(theta_sd=error_multi, zero_error=dont_include_error))

	if qubit_number == 3:
		entangled_state3 = tensor(NECPhase, qeye(2)) * entangled_state2
	else:
		entangled_state3 = tensor(qeye(2), NECPhase, qeye(2)) * entangled_state2

	# print(f'After initializing, our state looks like {current_state}')
	return entangled_state3

def scatter(system_state, coeffs, nuc_dephase_thetas, force_noloss=False, force_loss=False):
	"""
	Function attempts to scatter a photon off the Emitter Gate system, with a probability that the photon is lost
	through spontaneous decay, cavity loss, E-G transmission loss (lost during transmission between emitter and gate),
	and repeater-repeater transmission loss (lost during transmission between repeater stations). Function works in the
	basis |photon> |electron> |nuclear> |carbon>.

	The photon can be lost in 4 ways: Transmission loss between the emitter and gate (p_egtl), spontaneous decay in the
	cavity, cavity loss in the cavity, and transmission loss between repeaters (p_rtl).

	Parameters:
	-----------
	system_state: Qobj
		Quantum object state vector representing the current state of the system. Should be a 4 qubit basis vector.

	coeffs: list of coefficients
		list of coefficients oriented [r_0, r_1, l_0, l_1, p_egtl, p_rtl], where l_0 and l_1 are the cavity loss
		coefficients.

	force_noloss: True or False
		optional parameter. When set to true, all loss cases are abandoned and the photon will be entangled without
		fail. Typically only used for the decoding qubit.

	Returns:
	--------
	[state, outcome, probability]

	state: Qobj state vector
		quantum object state vector describing the system state after the scattering process.
	outcome: "success" or "failure"
		one of two possible strings flagging whether photon loss has occurred during the process.
	probability: float
		only returned in the case of successful scattering, for use in the calculation of conditional probability.

	"""

	# Set up coefficients
	r0, r1, l0, l1, p_egtl, p_rrtl, eta = coeffs[:7]
	error_emitter, error_single = coeffs[7], coeffs[8]
	if dont_include_loss:
		force_noloss = True

	qubit_number = int(np.log(system_state.shape[0]) / np.log(2))
	if qubit_number != 3 and qubit_number != 4:
		raise ValueError("The system state you want to scatter with needs to be either a 3 or 4 qubit basis!")

	eg_transmission_loss = random.choices([True, False], [p_egtl, 1-p_egtl])[0]
	if eg_transmission_loss == 1 and not force_noloss:
		# print(f'BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB transmission loss')
		return system_state
	elif force_loss:
		# print(f'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA FORCED LOSS')
		return system_state

	# Set up error matrices
	err_h = errored_H(draw_errvals(theta_sd=error_single, zero_error=dont_include_error))
	err_xr = errored_xrotate(draw_errvals(theta_sd=error_single, zero_error=dont_include_error))
	err_z = errored_Z(draw_errvals(theta_sd=error_single, zero_error=dont_include_error))

	# Set up emitter output
	emitter_error = errored_rotate(draw_errvals(theta_sd=error_emitter, zero_error=dont_include_error))
	emitter_output = emitter_error * (basis(2, 0) - 1j * basis(2, 1)).unit()

	# First we calculate the no loss case output state, and extract the probability Pnl from that
	if qubit_number == 3:
		mat = electron_photon_entangle(err_h, err_xr, err_z, [r0, r1], nuc_dephase_thetas)
		four_basis_state = tensor(emitter_output, system_state)
		noloss_output = mat * four_basis_state
		# print(f'our no loss output looks like {np.array(noloss_output)}')
		pnl = np.sum(np.abs(np.array(noloss_output)) ** 2)
		# print(f'pnl with error is {pnl}')
	else:
		mat = electron_photon_entangle(err_h, err_xr, err_z, [r0, r1], nuc_dephase_thetas, op_basis_size=5)
		five_basis_state = tensor(emitter_output, system_state)
		noloss_output = mat * five_basis_state
		# print(f'our no loss output looks like {np.array(noloss_output)}')
		pnl = np.sum(np.abs(np.array(noloss_output)) ** 2)
		# print(f'pnl with error is {pnl}')



	# Next we calculate the cavity loss case output state, and extract the probability Pcl from that
	if qubit_number == 3:
		mat1 = electron_photon_entangle(err_h, err_xr, err_z, [l0, l1], nuc_dephase_thetas)
		four_basis_state1 = tensor(emitter_output, system_state)
		cavityloss_output = mat1 * four_basis_state1
		pcl = np.sum(np.abs(np.array(cavityloss_output)) ** 2)
	else:
		mat1 = electron_photon_entangle(err_h, err_xr, err_z, [l0, l1], nuc_dephase_thetas, op_basis_size=5)
		five_basis_state1 = tensor(emitter_output, system_state)
		cavityloss_output = mat1 * five_basis_state1
		pcl = np.sum(np.abs(np.array(cavityloss_output)) ** 2)


	# Calculate Pse using the above results (No need for Pse0 and Pse1):
	pse = 1-pnl-pcl

	# print(f'Errored total probability is {pse + pnl + pcl}, with individual probabilities {pnl, pcl, pse}')
	# print(f'The spontaneous loss probabilities are {pse0, pse1}')

	situation = random.choices([1, 2, 3], [pnl, pcl, pse])[0]
	# print(f'reached this point, random choice is {situation}')
	if force_noloss:
		situation = 1

	if situation == 1:
		# No loss
		# print(f'DEBUG ------------------------------------------------------------------------------NO LOSS')
		return noloss_output * 1/np.sqrt(pnl)

	elif situation == 2:
		# Cavity loss
		# print(f'-[-[-[-[-[-[-[-[-[-[-[-[-[-[-[-[-[-[-[-[-[-[-[-[ Registered as cavity loss')
		cavityloss_state = cavityloss_output/np.sqrt(pcl)
		if qubit_number == 3:
			cutoff = 8
			qobj_dims = [[2,2,2],[1,1,1]]
		else:
			cutoff = 16
			qobj_dims = [[2,2,2,2],[1,1,1,1]]

		early_loss_prob = np.sum(np.abs(np.array(cavityloss_state)[:cutoff]) ** 2)
		late_loss_prob = np.sum(np.abs(np.array(cavityloss_state)[cutoff:]) ** 2)

		cavity_loss_mode = random.choices([1, 2], [early_loss_prob, late_loss_prob])[0]
		if cavity_loss_mode == 1:
			cl_endstate = np.array(cavityloss_state)[:cutoff]*1/np.sqrt(early_loss_prob)
			# print(f'Is this state normalized? {np.sum(np.abs(cl_endstate) ** 2)}')
			# print(f'what does this state look like? {Qobj(cl_endstate)}')
			return Qobj(cl_endstate, dims=qobj_dims)

		elif cavity_loss_mode == 2:
			cl_endstate = np.array(cavityloss_state)[cutoff:] * 1 / np.sqrt(late_loss_prob)
			# print(f'Is this state normalized? {np.sum(np.abs(cl_endstate) ** 2)}')
			# print(f'what does this state look like? {Qobj(cl_endstate)}')
			return Qobj(cl_endstate, dims=qobj_dims)
		else:
			raise ValueError("This should not be possible to reach")

	elif situation == 3:
		# Spontaneous Decay
		# print(f'-[-[-[-[-[-[-[-[-[-[-[-[-[-[-[-[-[-[-[-[-[-[-[-[ Registered as spontaneous emission')
		combined_state = tensor(emitter_output, system_state)
		# print(four_basis_state2,tensor(qeye(2), err_xr, qeye(2), qeye(2)))
		if qubit_number == 3:
			before_ES = tensor(qeye(2), err_xr, qeye(2), qeye(2)) * combined_state
			ES_SE = tensor(Qobj(np.array([r0, r1, 1, 1]) * np.eye(4), dims=[[2, 2], [2, 2]]),qeye(2), qeye(2))
			before_LS = tensor(qeye(2), err_h, qeye(2), qeye(2)) * ES_SE * tensor(qeye(2), err_xr, qeye(2), qeye(2)) * combined_state
		else:
			before_ES = tensor(qeye(2), qeye(2), err_xr, qeye(2), qeye(2)) * combined_state
			ES_SE = tensor(Qobj(np.array([r0,r1,r0,r1,1,1,1,1])*np.eye(8), dims=[[2, 2, 2], [2, 2, 2]]), qeye(2), qeye(2))
			before_LS = tensor(qeye(2), qeye(2), err_h, qeye(2), qeye(2)) * ES_SE * tensor(qeye(2), qeye(2), err_xr, qeye(2), qeye(2)) * combined_state


		# print(f'These are our states, before ES: {before_ES} and before LS: {before_LS}')

		# Determine probabilities for each SE loss case to occur:
		if qubit_number == 3:
			straight_loss_0_early = (1-eta) * np.sum(np.abs(np.array(before_ES)[:4]) ** 2)
			straight_loss_0_late = (1-eta) * np.sum(np.abs(np.array(before_LS)[8:12]) ** 2)
			straight_loss_1_early = (1-eta) * np.sum(np.abs(np.array(before_ES)[4:8]) ** 2)
			straight_loss_1_late = (1-eta) * np.sum(np.abs(np.array(before_LS)[12:]) ** 2)
		else:
			straight_loss_0_early = (1 - eta) * (np.sum(np.abs(np.array(before_ES)[:4]) ** 2) + np.sum(np.abs(np.array(before_ES)[8:12]) ** 2))
			straight_loss_0_late = (1 - eta) * (np.sum(np.abs(np.array(before_LS)[16:20]) ** 2) + np.sum(np.abs(np.array(before_LS)[24:28]) ** 2))
			straight_loss_1_early = (1 - eta) * (np.sum(np.abs(np.array(before_ES)[4:8]) ** 2) + np.sum(np.abs(np.array(before_ES)[12:16]) ** 2))
			straight_loss_1_late = (1 - eta) * (np.sum(np.abs(np.array(before_LS)[20:24]) ** 2) + np.sum(np.abs(np.array(before_LS)[28:]) ** 2))

		cross_loss_10_early = eta / (1 - eta) * straight_loss_0_early
		cross_loss_10_late = eta / (1 - eta) * straight_loss_0_late
		cross_loss_01_early = eta / (1 - eta) * straight_loss_1_early
		cross_loss_01_late = eta / (1 - eta) * straight_loss_1_late

		# print(f'Check for total probability: {straight_loss_0_early + straight_loss_1_early + straight_loss_0_late + straight_loss_1_late + cross_loss_10_early + cross_loss_10_late + cross_loss_01_early + cross_loss_01_late}')
		spontaneous_loss_mode = random.choices([1,2,3,4,5,6,7,8], [straight_loss_0_early, straight_loss_0_late, straight_loss_1_early, straight_loss_1_late, cross_loss_10_early, cross_loss_10_late, cross_loss_01_early, cross_loss_01_late])[0]

		if qubit_number == 3:
			# Straight losses
			if spontaneous_loss_mode == 1:
				se_output = tensor(basis(2, 0), Qobj(np.array(before_ES)[:4], dims=[[2,2],[1,1]])/np.sqrt(straight_loss_0_early/(1-eta)))
			elif spontaneous_loss_mode == 2:
				se_output = tensor(basis(2, 0), Qobj(np.array(before_LS)[8:12], dims=[[2,2],[1,1]])/np.sqrt(straight_loss_0_late/(1-eta)))
			elif spontaneous_loss_mode == 3:
				se_output = tensor(basis(2, 1), Qobj(np.array(before_ES)[4:8], dims=[[2,2],[1,1]])/np.sqrt(straight_loss_1_early/(1-eta)))
			elif spontaneous_loss_mode == 4:
				se_output = tensor(basis(2, 1), Qobj(np.array(before_LS)[12:], dims=[[2,2],[1,1]])/np.sqrt(straight_loss_1_late/(1-eta)))

			# Cross losses
			elif spontaneous_loss_mode == 5:
				se_output = tensor(basis(2, 0), Qobj(np.array(before_ES)[:4], dims=[[2, 2],[1, 1]]) / np.sqrt(cross_loss_10_early / eta))
			elif spontaneous_loss_mode == 6:
				se_output = tensor(basis(2, 0), Qobj(np.array(before_LS)[8:12], dims=[[2, 2],[1, 1]]) / np.sqrt(cross_loss_10_late / eta))
			elif spontaneous_loss_mode == 7:
				se_output = tensor(basis(2, 1), Qobj(np.array(before_ES)[4:8], dims=[[2, 2],[1, 1]]) / np.sqrt(cross_loss_01_early / eta))
			elif spontaneous_loss_mode == 8:
				se_output = tensor(basis(2, 1), Qobj(np.array(before_LS)[12:], dims=[[2, 2],[1, 1]]) / np.sqrt(cross_loss_01_late / eta))
			else:
				raise ValueError("This should not be possible")

		else:				# 5 q basis collapsing into 4
			# Straight losses
			# print(f'Isolated the issue to here')
			if spontaneous_loss_mode == 1:
				se_output = Qobj(np.concatenate((np.array(before_ES)[:4].T[0], np.zeros(4), np.array(before_ES)[8:12].T[0], np.zeros(4))),dims=[[2,2,2,2],[1,1,1,1]]) / np.sqrt(straight_loss_0_early / (1 - eta))
			elif spontaneous_loss_mode == 2:
				se_output = Qobj(np.concatenate((np.array(before_LS)[16:20].T[0], np.zeros(4), np.array(before_LS)[24:28].T[0], np.zeros(4))),dims=[[2,2,2,2],[1,1,1,1]]) / np.sqrt(straight_loss_0_late / (1 - eta))
			elif spontaneous_loss_mode == 3:
				se_output = Qobj(np.concatenate((np.zeros(4), np.array(before_ES)[4:8].T[0], np.zeros(4), np.array(before_ES)[12:16].T[0])),dims=[[2,2,2,2],[1,1,1,1]]) / np.sqrt(straight_loss_1_early / (1 - eta))
			elif spontaneous_loss_mode == 4:
				se_output = Qobj(np.concatenate((np.zeros(4), np.array(before_LS)[20:24].T[0], np.zeros(4),  np.array(before_LS)[28:].T[0])),dims=[[2,2,2,2],[1,1,1,1]]) / np.sqrt(straight_loss_1_late / (1 - eta))

			# Cross losses
			elif spontaneous_loss_mode == 5:
				se_output = Qobj(np.concatenate((np.array(before_ES)[:4].T[0], np.zeros(4), np.array(before_ES)[8:12].T[0], np.zeros(4))),dims=[[2,2,2,2],[1,1,1,1]]) / np.sqrt(cross_loss_10_early / eta)
			elif spontaneous_loss_mode == 6:
				se_output = Qobj(np.concatenate((np.array(before_LS)[16:20].T[0], np.zeros(4), np.array(before_LS)[24:28].T[0], np.zeros(4))),dims=[[2,2,2,2],[1,1,1,1]]) / np.sqrt(cross_loss_10_late / eta)
			elif spontaneous_loss_mode == 7:
				se_output = Qobj(np.concatenate((np.zeros(4), np.array(before_ES)[4:8].T[0], np.zeros(4), np.array(before_ES)[12:16].T[0])),dims=[[2,2,2,2],[1,1,1,1]]) / np.sqrt(cross_loss_01_early / eta)
			elif spontaneous_loss_mode == 8:
				se_output = Qobj(np.concatenate((np.zeros(4), np.array(before_LS)[20:24].T[0], np.zeros(4),  np.array(before_LS)[28:].T[0])),dims=[[2,2,2,2],[1,1,1,1]]) / np.sqrt(cross_loss_01_late / eta)
			else:
				raise ValueError("This should not be possible")

		# print(f'Returning the state {se_output}, is this normalized? {isnorm(se_output)}')
		return se_output

	else:
		raise ValueError("It should be impossible to get here.")

def measure_photon(system_state, measurement_basis):
	"""
	Function measures (assuming of course that the photon is not lost) the photon, using an x or z measurement depending
	on its position in the tree cluster state. The result of this measurement will contribute to a majority vote of 3
	photons.
	Input system state should be written in basis |photon> |electron> |nuclear> |carbon>
	"""
	qubit_number = int(np.log(system_state.shape[0]) / np.log(2))

	if qubit_number != 4 and qubit_number != 5:
		raise ValueError("The state you are trying to measure does not contain 4 or 5 qubits.")

	Z0, Z1 = ket2dm(basis(2, 0)), ket2dm(basis(2, 1))
	X0, X1 = ket2dm((basis(2, 0)+basis(2,1)).unit()), ket2dm((basis(2, 0) - basis(2,1)).unit())

	if measurement_basis == "z":
		measure_result = measure(system_state, [Z0, Z1], targets=[0])
	elif measurement_basis == "x":
		measure_result = measure(system_state, [X0, X1], targets=[0])
	else:
		raise ValueError("Please give me either 'z' or 'x' for the measurement_basis input")

	return measure_result[1], measure_result[0]

def measure_spin(system_state):
	"""
	Function performs an X measurement on the spin qubit as part of the photon spin swapping operation. The input can either
	be in basis |photon>|electron>|nuclear>|carbon> or |electron>|nuclear>|carbon>
	"""
	qubit_number = int(np.log(system_state.shape[0]) / np.log(2))

	if qubit_number != 3 and qubit_number != 4 and qubit_number != 5:
		raise ValueError("The state you want to measure the spin of isnt either a 3,4 or 5 qubit state!")

	X0, X1 = ket2dm((basis(2, 0) + basis(2, 1)).unit()), ket2dm((basis(2, 0) - basis(2, 1)).unit())

	if qubit_number == 5:
		measure_result = measure(system_state, [X0, X1], targets=[2])

	elif qubit_number == 4:
		measure_result = measure(system_state, [X0, X1], targets=[1])

	elif qubit_number == 3:
		measure_result = measure(system_state, [X0, X1], targets=[0])

	else:
		raise ValueError("Your state has the wrong number of qubits.")

	return measure_result[1], measure_result[0]


def generate_tree(branching_vector, coeffs):
	"""
	Function generates photonic tree cluster state characterised by the given branching_vector (must be length 3 for
	our purposes).
	"""

	def majority_vote_auxiliary(l3photon_measurements, l2photon_measurements, l1photon_measurements):
		"""
        Function performs majority voting on l1 photon of each auxiliary branch (head photon of each auxiliary branch).
        Contributing factors include the parity of the l3 photon measurements, and the x measurement outcome of the
        l2 photon measurements.

        l3 photon measurements should be in format 0 or 1. This function handles converting that to the +1, -1 format.

        Function returns a list of majority voted l1 measurements
        """

		def switch_notation(array):
			"""
			Input measurements are written in the 0,1 basis, this function switches it to the 1,-1 basis.
			"""
			return (array - 0.5) * -2

		def give_l3_parity(l1p, l2p, l3p_measurements):
			measurements_arr = l3p_measurements[l1p][l2p]
			if np.all(np.invert(np.isnan(measurements_arr))):
				return np.product(switch_notation(measurements_arr))
			else:
				# print(f'DEBUG: The level 3 photons associated with level 1 photon {l1p} and level 2 photon {l2p} is incomplete. Returning None parity')
				return None

			# if np.all(np.isnan(measurements_arr)):
			# 	return np.product(switch_notation(measurements_arr))
			# else:
			# 	mask = np.invert(np.isnan(measurements_arr))
			# 	non_nan = measurements_arr[mask]
			# 	return np.product(switch_notation(non_nan))


		nl1 = l3photon_measurements.shape[0]					# Number of level 1 photons
		nl2 = l2photon_measurements.shape[1]					# Number of level 2 photons
		l1majority_vote_result = []

		# print(f'Input l1 measurements {l1photon_measurements}')
		# print(f'Inputs are {l3photon_measurements, l2photon_measurements}')
		# print(f'we"re gonna loop through {l1photons_number} level 1 photons and {l2photons_number} level 2 photons')
		for l1ph in range(nl1):
			l1photon_votes = []
			l1photon_votes.append(switch_notation(l1photon_measurements[l1ph]))
			# print(f'DEBUGGING: For clarity, we have added the following vote: {l1photon_votes}')
			for l2ph in range(nl2):
				# l3parity = np.product(switch_notation(l3photon_measurements[l1ph][l2ph]))
				l3parity = give_l3_parity(l1ph,l2ph,l3photon_measurements)
				if l3parity == 1.0:
					if l2photon_measurements[l1ph][l2ph] == 0:
						l1photon_votes.append(1)
					elif l2photon_measurements[l1ph][l2ph] == 1:
						l1photon_votes.append(-1)
					else:
						# In the case where only the l2 photon is lost
						# l1photon_votes.append(1)
						# print(f'DEBUG: We have a l3 parity for level 1 photon {l1ph} level 2 photon {l2ph} but the l2 photon is lost. Returning None parity for this branch')
						l1photon_votes.append(None)
				elif l3parity == -1.0:
					if l2photon_measurements[l1ph][l2ph] == 0:
						l1photon_votes.append(-1)
					elif l2photon_measurements[l1ph][l2ph] == 1:
						l1photon_votes.append(1)
					else:
						# In the case where only the l2 photon is lost
						# l1photon_votes.append(-1)
						# print(f'DEBUG: We have a l3 parity for level 1 photon {l1ph} level 2 photon {l2ph} but the l2 photon is lost. Returning None parity for this branch')
						l1photon_votes.append(None)
				else:
					# Parity is None
					l1photon_votes.append(None)
					# if np.isnan(l2photon_measurements[l1ph][l2ph]):
					# 	l1photon_votes.append(None)
					# else:
					# 	l1photon_votes.append(switch_notation(l2photon_measurements[l1ph][l2ph]))

			# 3 possibilities: All None, mostly None with clear majority vote winner, mostly None with majority vote tie
			nan_array = np.array(l1photon_votes, dtype=float)
			if np.all(np.isnan(nan_array)):
				l1majority_vote_result.append(None)
				continue

			mask = np.invert(np.isnan(nan_array))
			nan_excluded_votes_array = nan_array[mask]

			votes_list = Majoritycount(nan_excluded_votes_array).most_common()

			if len(votes_list) > 1 and np.all(np.array([x[1] for x in votes_list]) == votes_list[0][1]):
				# print(f'Random condition met')
				# print("I'm just going to select a random outcome, since we have a tie in the vote. Should this be possible?")
				l1majority_vote_result.append(random.choices([1, -1], [0.5, 0.5])[0])
				continue

			# Count majority votes
			# print(f'Votes list gives {votes_list}')
			if votes_list[0][0] == 1:
				l1majority_vote_result.append(1)
			elif votes_list[0][0] == -1:
				l1majority_vote_result.append(-1)
			else:
				raise ValueError(f'This shouldnt be possible.')

		# print(f'The final result of all that voting is: {l1majority_vote_result}')
		return l1majority_vote_result

	def majority_vote_decode(depth3_decode_measurements, depth2_decode_measurements):
		"""
		Function performs majority voting on decoding branch.
		"""
		def switch_notation(array):
			"""
			Input measurements are written in the 0,1 basis, this function switches it to the 1,-1 basis.
			"""
			return (array - 0.5) * -2

		l2photons_number = depth3_decode_measurements.shape[0]
		l2majority_vote_result = []
		# print(f'we are working with level 2 and level 3 decode measurement arrays d3 {depth3_decode_measurements}, d2 {depth2_decode_measurements}')
		for l2i in range(l2photons_number):
			# print(f'Checking condition for the {l2i}th level 2 photon: {np.isnan(depth2_decode_measurements[l2i].astype(float))}')
			if np.all(np.isnan(depth3_decode_measurements[l2i])) and np.isnan(depth2_decode_measurements[l2i].astype(float)):
			# if np.all(np.isnan(depth3_decode_measurements[l2i])):
			# 	print(f'No clue what the level 2 information is')
				l2majority_vote_result.append(None)
				continue

			# print(f'Are we taking the correct arrays? We look at decoding photon {l2i}, the level 3 measurements for this photon are {depth3_decode_measurements[l2i]} and the measurement itself is {[depth2_decode_measurements[l2i]]}')
			combinedlist = np.concatenate((depth3_decode_measurements[l2i], [depth2_decode_measurements[l2i]]))
			# print(f'combined list loooks like {combinedlist}')
			# mask1 = np.invert(np.isnan(depth3_decode_measurements[l2i]))
			mask1 = np.invert(np.isnan(combinedlist))
			non_nan_results_only = switch_notation(combinedlist[mask1])
			# print(f'DECODING: The list we count the majority votes of is {non_nan_results_only}')
			votes_list = Majoritycount(non_nan_results_only).most_common()

			# print(f'Second row: {[x[1] for x in votes_list]}')
			# print(f'Check statement {np.all([x[1] for x in votes_list] == votes_list[0][1])}')
			# print(f'Check statement {votes_list[0][1]}')
			if len(votes_list) > 1 and np.all(np.array([x[1] for x in votes_list]) == votes_list[0][1]):
			# 	print("I'm just going to select a random outcome, since we have a tie in the vote. Should this be possible?")
				l2majority_vote_result.append(random.choices([1, -1], [0.5, 0.5])[0])
				continue

			# Count majority votes
			if votes_list[0][0] == 1:
				l2majority_vote_result.append(1)
			elif votes_list[0][0] == -1:
				l2majority_vote_result.append(-1)
			else:
				raise ValueError(f'This shouldnt be possible.')

		# print(f'The decoding branch l3 photon measurements suggest that the l2 photons should be: {l2majority_vote_result}')
		return l2majority_vote_result


	def discard_photon_basis(measurement_result, measurement_basis):
		"""
		Function takes the state which remains after either an X or Z measurement has been performed on it, and shrinks
		the state from the basis |photon> |electron> |nuclear> |carbon> into |electron> |nuclear> |carbon>, discarding
		the photon basis.

		Output is the collapsed three photon state mentioned above.
		"""
		qubit_number = int(np.log(measurement_result[0].shape[0]) / np.log(2))
		if qubit_number != 4 and qubit_number != 5:
			raise ValueError("The state you want to discard the photon basis of is not a 4 or 5 qubit state!")

		if qubit_number == 4:
			if measurement_basis == "z":
				if measurement_result[1] == 0:
					return Qobj((measurement_result[0])[:8], dims=[[2, 2, 2], [1, 1, 1]])

				elif measurement_result[1] == 1.0:
					return Qobj((measurement_result[0])[8:], dims=[[2, 2, 2], [1, 1, 1]])

				else:
					raise ValueError("Your measurement result is not one of two possible values: 0, or 1.")

			elif measurement_basis == "x":

				arr = np.array(measurement_result[0]).T[0]
				assert np.sum(abs(arr[:8]) - abs(arr[8:])) == 0, "Your assumption that the first half and second half values have equal abs value is false"

				new_arr = np.sqrt(2)*arr[:8]

				return Qobj(new_arr, dims=[[2, 2, 2], [1, 1, 1]])

			else:
				raise ValueError("Please give either 'x' or 'z' for the parameter measurement_basis")

		else:
			if measurement_basis == "z":
				if measurement_result[1] == 0:
					return Qobj((measurement_result[0])[:16], dims=[[2, 2, 2, 2], [1, 1, 1, 1]])

				elif measurement_result[1] == 1.0:
					return Qobj((measurement_result[0])[16:], dims=[[2, 2, 2, 2], [1, 1, 1, 1]])

				else:
					raise ValueError("Your measurement result is not one of two possible values: 0, or 1.")

			elif measurement_basis == "x":

				arr = np.array(measurement_result[0]).T[0]
				assert np.sum(abs(arr[:16]) - abs(arr[16:])) == 0, "Your assumption that the first half and second half values have equal abs value is false"

				new_arr = np.sqrt(2) * arr[:16]

				return Qobj(new_arr, dims=[[2, 2, 2, 2], [1, 1, 1, 1]])

			else:
				raise ValueError("Please give either 'x' or 'z' for the parameter measurement_basis")


	def discard_electron_nuclear_spins(state):
		"""
		Function is used at the very end of tree generation in order to "cut off" the now stray electron and nuclear
		spins. The function returns the basis |photon> |carbon>, where photon is the decoding photon and carbon
		is the carbon memory spin (NOT Carbon memory photon), ready for encoding and decoding.
		"""
		# print(f'Trying to cut off electron and nuclear from the following state: {state}')

		arr = np.array(state).T[0]
		e_zero = np.concatenate((arr[:4], arr[8:12]))
		e_one = np.concatenate((arr[4:8], arr[12:]))

		if not (abs(e_zero) == abs(e_one)).all():
			raise AssertionError

		arr1 = np.sqrt(2) * e_zero
		n_zero = np.concatenate((arr1[:2], arr1[4:6]))
		n_one = np.concatenate((arr1[2:4], arr1[6:]))

		# print(f'Comparison is {n_zero, n_one}')
		# print(f'Debug: What is the assertion error? {state}')
		if not (abs(n_zero) == abs(n_one)).all():
			raise AssertionError

		two_qubit_state = np.sqrt(2) * Qobj(n_zero, dims=[[2, 2], [1, 1]])
		return two_qubit_state

	def reprepare(qstate, level):
		"""
		Function attempts to prepare a 3 qubit state consisting of |electron> |nuclear> and |carbon> for entangling and
		subsequent generation of the next branch by re-preparing the electron nuclear states in the |+> state. This involves
		correcting them from the |-> state if they are found to be in this state.
		"""
		qubit_number = int(np.log(qstate.shape[0])/np.log(2))

		if qubit_number != 3 and qubit_number != 4:
			raise ValueError("The state you want to reprepare isn't either a 3 qubit state or a 4 qubit state!")

		X0, X1 = ket2dm((basis(2, 0) + basis(2, 1)).unit()), ket2dm((basis(2, 0) - basis(2, 1)).unit())
		H = errored_H(draw_errvals(theta_sd=error_single, zero_error=True))

		if qubit_number == 3:
			correction_e = tensor(sigmaz(), qeye(2), qeye(2))
			correction_n = tensor(qeye(2), sigmaz(), qeye(2))
		else:
			correction_e = tensor(qeye(2), sigmaz(), qeye(2), qeye(2))
			correction_n = tensor(qeye(2), qeye(2), sigmaz(), qeye(2))

		# Spontaneous emission could leave the electron in the |0> or |1> state. Apply a H in this case to switch the
		# state into either |+> or |->
		if qubit_number == 3:
			arr = np.array(qstate)
			if np.all(arr[:4] == 0) or np.all(arr[4:] == 0):
				qstate = tensor(H, qeye(2), qeye(2))*qstate
			measurement_copy1 = Qobj(np.copy(np.array(qstate)), dims=[[2, 2, 2], [1, 1, 1]])
			measurement_copy2 = Qobj(np.copy(np.array(qstate)), dims=[[2, 2, 2], [1, 1, 1]])
		else:
			arr = np.array(qstate)
			if np.all(arr[:4] == 0) and np.all(arr[8:12] == 0):
				qstate = tensor(qeye(2), H, qeye(2), qeye(2)) * qstate
			elif np.all(arr[4:8] == 0) and np.all(arr[12:] == 0):
				qstate = tensor(qeye(2), H, qeye(2), qeye(2)) * qstate
			measurement_copy1 = Qobj(np.copy(np.array(qstate)), dims=[[2, 2, 2, 2], [1, 1, 1, 1]])
			measurement_copy2 = Qobj(np.copy(np.array(qstate)), dims=[[2, 2, 2, 2], [1, 1, 1, 1]])


		if level == 1:
			if qubit_number == 3:
				target1, target2 = 0,1
			else:
				target1, target2 = 1,2

			measure_result_1 = measure(measurement_copy1, [X0, X1], targets=[target1])[0]
			measure_result_2 = measure(measurement_copy2, [X0, X1], targets=[target2])[0]
			if measure_result_1 == 1 and measure_result_2 == 0:
				return correction_e*qstate
			elif measure_result_1 == 0 and measure_result_2 == 1:
				return correction_n*qstate
			elif measure_result_1 == 1 and measure_result_2 == 1:
				return correction_n*correction_e*qstate
			else:
				return qstate

		elif level == 2:
			# print(f'Is this state normalized?: {measurement_copy1}')
			if qubit_number == 3:
				target1 = 0
			else:
				target1 = 1

			measure_result_1 = measure(measurement_copy1, [X0, X1], targets=[target1])[0]
			if measure_result_1 == 0:
				return qstate
			elif measure_result_1 == 1:
				return correction_e*qstate
			else:
				raise NotImplementedError("Somehow youve ended up with a nonexistent measurement result")
		else:
			raise ValueError("Your choice of level should either be 1 or 2.")


	def correct_spin_measurement(total_state, spin_x_result):
		"""
		Function is part of the spin-photon swapping operation. It applies a phase correction to
		the photon depending on the outcome of the X measurement on the spin.
		"""
		qubit_number = int(np.log(total_state.shape[0]) / np.log(2))
		if qubit_number != 4 and qubit_number != 5:
			raise ValueError("The state you want correct (teleportation step) must be either 4 or 5 qubits!")

		if qubit_number == 4:
			if spin_x_result == 1.0:
				corr_state = tensor(sigmaz(), qeye(2), qeye(2), qeye(2)) * total_state
			else:
				corr_state = total_state
		else:
			if spin_x_result == 1.0:
				corr_state = tensor(sigmaz(), qeye(2), qeye(2), qeye(2), qeye(2)) * total_state
			else:
				corr_state = total_state

		return corr_state

	def ideal_case_checker(aux_d2, aux_d3, aux_d1, dec_d2, dec_d3):
		"""
		Purely used for debugging, function will check that all measurement results agree with each other.
		"""
		n1, n2, n3 = aux_d3.shape[0], aux_d3.shape[1], aux_d3.shape[2]

		# Auxiliary branch examination
		for l1branch in range(n1-1):
			implied_state = None
			vote1 = (aux_d2[l1branch][0] - 0.5) * -2
			vote2 = np.product((aux_d3[l1branch][0] - 0.5) * -2)
			if vote2 == 1:
				if vote1 == 1:
					implied_state = 1
				elif vote1 == -1:
					implied_state = -1
				else:
					raise NotImplementedError
			elif vote2 == -1:
				if vote1 == 1:
					implied_state = -1
				elif vote1 == -1:
					implied_state = 1
				else:
					raise NotImplementedError
			else:
				raise NotImplementedError

			for l2branch in range(n2):
				implied_state1 = None
				vote3 = (aux_d2[l1branch][l2branch] - 0.5) * -2
				vote4 = np.product((aux_d3[l1branch][l2branch] - 0.5) * -2)
				if vote4 == 1:
					if vote3 == 1:
						implied_state1 = 1
					elif vote3 == -1:
						implied_state1 = -1
					else:
						raise NotImplementedError
				elif vote4 == -1:
					if vote3 == 1:
						implied_state1 = -1
					elif vote3 == -1:
						implied_state1 = 1
					else:
						raise NotImplementedError
				else:
					raise NotImplementedError

				if not implied_state1 == implied_state:
					raise AssertionError("Something is wrong! There are auxiliary measurements that do not agree!")

		# Decoding branch examination
		nd2 = dec_d3.shape[0]
		nd3 = dec_d3.shape[1]
		for l2branch in range(nd2):
			for l3ph in range(nd3):
				if not dec_d3[l2photon][l3ph] == dec_d2[l2photon]:
					raise AssertionError("Something is wrong! There are decode branch measurements that do not agree!")

		return None

	def draw_nuclear_dephasing_error(standard_dev):
		"""
		Draws the theta values theta_00, theta_01, theta_10 and theta_11 (nuclear carbon basis) that control the nuclear
		dephasing as part of early and late scattering. Inputs a standard deviation for the gaussian distribution and
		outputs an array of the 4 theta values.
		"""
		theta00 = np.random.normal(0, standard_dev)
		theta01 = np.random.normal(0, standard_dev)
		theta10 = np.random.normal(0, standard_dev)
		theta11 = np.random.normal(0, standard_dev)

		return np.array([theta00, theta01, theta10, theta11])

	def rearrange_4q_basis(qstate, invert=False):
		"""
		Function takes an input 4 qubit state written in |photon>|electron>|nuclear>|carbon> and rewrites it in the
		|electron>|nuclear>|carbon>|photon> basis. If the invert parameter is enabled, we do the inverse operation,
		going from |electron>|nuclear>|carbon>|photon> to |photon>|electron>|nuclear>|carbon>.
		"""
		ar = np.array(qstate).T[0]

		if not invert:
			returnstate = Qobj(np.array([ar[0],ar[8],ar[1],ar[9],ar[2],ar[10],ar[3],ar[11],ar[4],ar[12],ar[5],ar[13],ar[6],ar[14],ar[7],ar[15]]),dims=[[2,2,2,2],[1,1,1,1]])
		elif invert:
			returnstate = Qobj(np.array([ar[0],ar[2],ar[4],ar[6],ar[8],ar[10],ar[12],ar[14],ar[1],ar[3],ar[5],ar[7],ar[9],ar[11],ar[13],ar[15]]),dims=[[2,2,2,2],[1,1,1,1]])
		else:
			raise ValueError("Invert needs to be a boolean")
		return returnstate

	####################################################################################################################

	if len(branching_vector) > 3:
		raise ValueError("Your branching vector is too large")
	elif len(branching_vector) != 3:
		raise ValueError("Your tree is of the wrong size")

	# Extract and set error parameters
	error_emitter, error_single, error_multi = coeffs[7], coeffs[8], coeffs[9]
	nuclear_dephasing_sd = coeffs[10]

	H_for_swap = errored_H(draw_errvals(theta_sd=error_single, zero_error=dont_include_error))   # Used later for photon spin swap

	# Transmission loss probability
	prrtl = coeffs[5]
	p_loss = 1-((1-coeffs[4]) * (np.abs(coeffs[0])) * (1 - prrtl) * coeffs[11])

	# Measurement containers
	d3_aux_measurements = 99*np.ones([branching_vector[0], branching_vector[1], branching_vector[2]])
	d2_aux_measurements = 99*np.ones([branching_vector[0], branching_vector[1]])
	d1_aux_measurements = 99*np.ones([branching_vector[0]])
	d3_decode_measurements = 99*np.ones([branching_vector[1], branching_vector[2]])
	d2_decode_measurements = 99*np.ones([branching_vector[1]])


	# Draw the nuclear dephasing error:
	if not dont_include_error:
		nuclear_dephasing_thetas = draw_nuclear_dephasing_error(nuclear_dephasing_sd)
	else:
		nuclear_dephasing_thetas = None

	decode_photon_found = False				# Init the decoding branch pointer
	decode_marker = None

	for l1photon in range(branching_vector[0]):

		# Floating measurement storage. Measurements are stored here before it is decided if the branch is decoding or aux
		# and the results are appended to above containers at the end of a branch
		d3_temp_measurements = 99*np.ones([branching_vector[1], branching_vector[2]])
		d2_temp_measurements = 99*np.ones([branching_vector[1]])

		# First step: We roll for level 1 photon in this branch being lost or not, and tag the branch accordingly.
		tag = "aux branch"
		decode_this_branch = False
		# print(f'l1photon {l1photon}')

		if not decode_photon_found:
			# print(f'Looking for decoding photon: Rolling the mu probabilities')
			if dont_include_loss:
				decode_this_branch = True
			else:
				decode_this_branch = random.choices([True, False], [1- p_loss, p_loss])[0]

			if decode_this_branch:
				decode_photon_found = True
				tag = "decoding branch"
				decode_marker = np.copy(l1photon)
			# else:
				# print(f'level 1 photon {l1photon} has just been designated as "lost"')

		print(f'level 1 photon {l1photon} has been designated as {tag}')


		# Initialize and reprepare
		if l1photon == 0:
			state = init_ptcs(coeffs)
			# print(f'Init state is {state}')
		else:
			# We need to re-prepare the nuclear and electron states before entangling them again.
			# en_detached_checker(finalbranchstate)
			reprepared_state = reprepare(finalbranchstate, 1)
			# reprepared_state = finalbranchstate
			state = init_ptcs(coeffs, custom_state=reprepared_state)

		for l2photon in range(branching_vector[1]):

			# print(f'l2photon {l2photon}')

			if not l2photon == 0:
				# Reentangle the now "stray" electron spin with the nuclear spin.
				NECPhase = electron_nuclear_cphase(errored_H(draw_errvals(theta_sd=error_multi, zero_error=dont_include_error)), draw_errvals(theta_sd=error_multi, zero_error=dont_include_error))

				# print(f'debug checkpoint +===========================================================')
				# print(f'debugging {state}')
				# Reprepare the electron back into |+> if it has changed into |->
				state = reprepare(state,2)
				# print(f'Is the electron even stray rn? {state}')

				# Make sure the nuclear electron cphase is actually applying on the correct qubits
				qn = int(np.log(state.shape[0])/np.log(2))
				if qn != 3 and qn != 4:
					raise ValueError("The state here is not either 3 or 4 qubits!")
				if qn == 3:
					state = tensor(NECPhase, qeye(2)) * state
				else:
					state = tensor(qeye(2), NECPhase, qeye(2)) * state
					# state = tensor(NECPhase, qeye(2)) * state

				if state is None:
					raise NotImplementedError("Somehow there's no reference here?")

			for l3photon in range(branching_vector[2]):

				# print(f'level 1 photon {l1photon}, level 2 photon {l2photon}, looking at l3 photon {l3photon}')
				# Generate level 3 photons
				# print(f'isnorm result before: {isnorm(state)}')
				result = scatter(state, coeffs, nuclear_dephasing_thetas)

				# print(f'We have completed scattering. Our state now looks like {result}')
				# print(f'isnorm result after: {isnorm(result)}')

				# Roll probability for transmission loss. If successful, measurement result is saved as None
				if not dont_include_loss:
					repeater_transmission_loss = random.choices([True, False], [prrtl, 1 - prrtl])[0]
				else:
					repeater_transmission_loss = False

				# Possible scenarios: 3 qubit state = lost photon, 5 qubit state = successfully transmitted photon
				# after decoding photon has been found, 4 qubit state = either successfully transmitted photon before
				# decoding photon has been found or lost photon after decoding photon has been found
				if result.shape[0] == 32:

					if tag == "decoding branch":
						photon_measure_result = measure_photon(result, "x")

						# print(f'The result of my photon measurement is {photon_measure_result}')
						if repeater_transmission_loss:
							d3_temp_measurements[l2photon, l3photon] = None
						else:
							d3_temp_measurements[l2photon, l3photon] = photon_measure_result[1]
						# Shrink the basis back to 3 qubits, since the photon state doesn't matter anymore
						state = discard_photon_basis(photon_measure_result, "x")

					else:
						photon_measure_result = measure_photon(result, "z")

						# print(f'The result of my photon measurement is {photon_measure_result}')
						if repeater_transmission_loss:
							d3_temp_measurements[l2photon, l3photon] = None
						else:
							d3_temp_measurements[l2photon, l3photon] = photon_measure_result[1]
						# Shrink the basis back to 3 qubits, since the photon state doesn't matter anymore
						state = discard_photon_basis(photon_measure_result, "z")
						# print(f'Normally the state is {state}')


				elif result.shape[0] == 16:

					if tag == "decoding branch":
						photon_measure_result = measure_photon(result, "x")
						# print(f'The result of my photon measurement is {photon_measure_result}')
						if repeater_transmission_loss:
							d3_temp_measurements[l2photon, l3photon] = None
						else:
							d3_temp_measurements[l2photon, l3photon] = photon_measure_result[1]
						# Shrink the basis back to 3 qubits, since the photon state doesn't matter anymore
						state = discard_photon_basis(photon_measure_result, "x")

					elif tag == "aux branch":
						if decode_photon_found:
							# This is a special case. If the decode photon has been found and this current branch is
							# an aux branch, this case describes the case where the photon has been lost. We need thus
							# to record a None measurement.
							# print(f'DEBUG SIGNATURE |+|+++++++++++|+|+|+|+|+|+|+|+|+|+')
							d3_temp_measurements[l2photon, l3photon] = None
							state = result
						else:
							photon_measure_result = measure_photon(result, "z")

							# print(f'The result of my photon measurement is {photon_measure_result}')
							if repeater_transmission_loss:
								d3_temp_measurements[l2photon, l3photon] = None
							else:
								d3_temp_measurements[l2photon, l3photon] = photon_measure_result[1]
							# Shrink the basis back to 3 qubits, since the photon state doesn't matter anymore
							state = discard_photon_basis(photon_measure_result, "z")
							# print(f'Normally the state is {state}')

					else:
						raise NotImplementedError("Some other branch? typo?")

				elif result.shape[0] == 8:
					if tag == "decoding branch":
						# print(f'Is it here?')
						d3_temp_measurements[l2photon, l3photon] = None
					else:
						d3_temp_measurements[l2photon, l3photon] = None
					state = result

				else:
					raise NotImplementedError("The output state for scatter seems to not have the correct shape.")


			# print(f'We have completed running across the depth 3 branch belonging to l1 photon {l1photon} and l2photon {l2photon}. The state looks like  {state}')

			"""
			Completed loop over level 3 photons, now moving to the head level 2 photon of the level 2 branch. 
			"""

			# Complete the l2 photon attached to these l3 photons
			# print(f'We are now trying to scatter and measure the l2 photon number {l2photon}')
			first_l2_photon = scatter(state, coeffs, nuclear_dephasing_thetas)
			# print(f'Quick checkpoint: {isnorm(first_l2_photon)}')

			# print(f'Consider implementing here a sanity check, that the two states have indeed been swapped')
			if not dont_include_loss:
				repeater_transmission_loss1 = random.choices([True, False], [prrtl, 1-prrtl])[0]
			else:
				repeater_transmission_loss1 = False
			# print(f'is it a tuple here?{state}')

			# Same pipeline as for the level 3 photons. The overlap case occurs when we have found the decode photon
			# and are in the aux branch.
			if first_l2_photon.shape[0] == 32:

				state = tensor(H_for_swap,qeye(2),qeye(2),qeye(2),qeye(2)) * first_l2_photon
				spin_measure_result = measure_spin(state)
				state = correct_spin_measurement(spin_measure_result[0], spin_measure_result[1])

				if tag == "decoding branch":
					photon_measure_result_d2 = measure_photon(state, "z")
					# print(f'The decoding branch measurement result for the l2 photon is {photon_measure_result_d2}')
					if repeater_transmission_loss1:
						d2_temp_measurements[l2photon] = None
					else:
						d2_temp_measurements[l2photon] = photon_measure_result_d2[1]
					# Shrink the basis back to 3 qubits, since the photon state doesn't matter anymore
					state = discard_photon_basis(photon_measure_result_d2, "z")
				else:
					photon_measure_result_d2 = measure_photon(state, "x")
					if repeater_transmission_loss1:
						d2_temp_measurements[l2photon] = None
					else:
						d2_temp_measurements[l2photon] = photon_measure_result_d2[1]
					# Shrink the basis back to 3 qubits, since the photon state doesn't matter anymore
					state = discard_photon_basis(photon_measure_result_d2, "x")
					# print(f'Debug 4| I predict that the electron is detached here. {state}')

			elif first_l2_photon.shape[0] == 16:
				# print(f'The level 2 photon {l2photon} was successfully entangled')

				if decode_photon_found and not decode_this_branch:
					state = measure_spin(state)[0]
					if tag == "decoding branch":
						d2_temp_measurements[l2photon] = None
					else:
						d2_temp_measurements[l2photon] = None

				else:
					state = tensor(H_for_swap,qeye(2),qeye(2),qeye(2)) * first_l2_photon
					spin_measure_result = measure_spin(state)
					state = correct_spin_measurement(spin_measure_result[0],spin_measure_result[1])

					# print(f'Looking at level 2 photon number {l2photon}')
					# Time to measure the l2 photon and complete the triplet.
					if tag == "decoding branch":
						photon_measure_result_d2 = measure_photon(state, "z")
						# print(f'The decoding branch measurement result for the l2 photon is {photon_measure_result_d2}')
						if repeater_transmission_loss1:
							d2_temp_measurements[l2photon] = None
						else:
							d2_temp_measurements[l2photon] = photon_measure_result_d2[1]
						# Shrink the basis back to 3 qubits, since the photon state doesn't matter anymore
						state = discard_photon_basis(photon_measure_result_d2, "z")
					else:
						photon_measure_result_d2 = measure_photon(state, "x")
						if repeater_transmission_loss1:
							d2_temp_measurements[l2photon] = None
						else:
							d2_temp_measurements[l2photon] = photon_measure_result_d2[1]
						# Shrink the basis back to 3 qubits, since the photon state doesn't matter anymore
						state = discard_photon_basis(photon_measure_result_d2, "x")
						# print(f'Debug 4| I predict that the electron is detached here. {state}')

			elif first_l2_photon.shape[0] == 8:
				# print(f'The level 2 photon {l2photon} was most likely lost')
				state = measure_spin(state)[0]

				if tag == "decoding branch":
					d2_temp_measurements[l2photon] = None
				else:
					d2_temp_measurements[l2photon] = None
					# print(f'We have gone through the following route')

				# print(f'Debug 4| I predict that the electron is not detached here. {state}')
			else:
				raise NotImplementedError("I'm not sure how you ended up here, but it's definitely some sort of magic")

			# print(f'Temporary d2 results: {d2_temp_measurements}')
			# print(f'Temporary d3 results: {d3_temp_measurements}')

			# If designated as decoding branch, check after each l2 branch is done whether the branch is actually decodable. If not,
			# switch the tag and look for another decoding branch. Dump existing measurements.
			# print(f'd2 temp: {d2_temp_measurements}')
			# print(f'd3 temp: {d3_temp_measurements}')

			# if tag == "decoding branch":
			# 	if np.isnan(d2_temp_measurements[l2photon]) and np.all(np.isnan(d3_temp_measurements[l2photon])):
			# 		# print(f'DEBUG: This branch is undecodable after all, keep looking!')
			# 		tag = "decoding branch"
			# 		decode_photon_found = False
			# 		decode_this_branch = False
			#
			# 		d3_temp_measurements = 99 * np.ones([branching_vector[1], branching_vector[2]])
			# 		d2_temp_measurements = 99 * np.ones([branching_vector[1]])
			#
			# 		for d2ph in range(l2photon+1):
			# 			for d3ph in range(branching_vector[2]):
			# 				d3_temp_measurements[d2ph][d3ph] = None
			# 			d2_temp_measurements[d2ph] = None






		"""
		Looped over all level 2 branches. Now moving to final level 1 photon of this level 1 branch
		"""
		# Nuclear electron swap
		# print(f'Is the electron state separate here {state}')
		if int(np.log(state.shape[0])/np.log(2)) == 3:
			state = tensor(electron_nuclear_swap(draw_errvals(zero_error=True)), qeye(2)) * state
		elif int(np.log(state.shape[0])/np.log(2)) == 4:
			state = tensor(qeye(2), electron_nuclear_swap(draw_errvals(zero_error=True)), qeye(2)) * state
		else:
			raise NotImplementedError("Error occurs at nuclear electron swap for level 1 photon")

		# Move the d3 and d2 measurements from temporary containers to permanent containers depending on branch tag
		# print("debugging AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA")
		if tag == "decoding branch":
			# print(f'd3 dec before: {d3_decode_measurements}')
			# print(f'd2 dec before: {d2_decode_measurements}')
			for l2p in range(d3_temp_measurements.shape[0]):
				for l3p in range(d3_temp_measurements.shape[1]):
					d3_decode_measurements[l2p][l3p] = np.copy(d3_temp_measurements[l2p][l3p])
				d2_decode_measurements[l2p] = np.copy(d2_temp_measurements[l2p])
			# print(f'd3 dec after: {d3_decode_measurements}')
			# print(f'd2 dec after: {d2_decode_measurements}')
		else:
			# print(f'd3 aux before: {d3_aux_measurements}')
			# print(f'd2 aux before: {d2_aux_measurements}')
			for l2p in range(d3_temp_measurements.shape[0]):
				for l3p in range(d3_temp_measurements.shape[1]):
					d3_aux_measurements[l1photon][l2p][l3p] = np.copy(d3_temp_measurements[l2p][l3p])
				d2_aux_measurements[l1photon][l2p] = np.copy(d2_temp_measurements[l2p])
			# print(f'd3 aux after: {d3_aux_measurements}')
			# print(f'd2 aux after: {d2_aux_measurements}')

		if tag == "decoding branch":
			finalbranchstate = scatter(state, coeffs, None, force_noloss=True)
			# print(f'l1photon {l1photon}, final branch state {finalbranchstate}')
			state = tensor(H_for_swap,qeye(2),qeye(2),qeye(2)) * finalbranchstate

			spin_measure_result = measure_spin(state)
			finalbranchstate = correct_spin_measurement(spin_measure_result[0],spin_measure_result[1])

			# print(f'Debug 2: auxiliary results are {d2_aux_measurements}, {d3_aux_measurements}')
			# print(f'Debug 2: Our decoding results are {d3_decode_measurements}')

			# print(f'Are the nuclear and electron spins detached? {final_state}')
			continue

		if not decode_photon_found and not decode_this_branch:

			# print(f'To complete the analysis of this branch, we are now trying to complete the triplet with a l1 photon')
			first_l1_photon = scatter(state, coeffs, nuclear_dephasing_thetas, force_loss=True)

			if not dont_include_loss:
				repeater_transmission_loss2 = random.choices([True, False], [prrtl, 1-prrtl])[0]
			else:
				repeater_transmission_loss2 = False
			# print(f'Consider implementing here a sanity check, that the two states have indeed been swapped')

			if first_l1_photon.shape[0] == 16:
				state = tensor(H_for_swap,qeye(2),qeye(2),qeye(2)) * first_l1_photon
				spin_measure_result = measure_spin(state)
				state = correct_spin_measurement(spin_measure_result[0],spin_measure_result[1])

				# Time to measure the l1 photon.
				# print(f'The input to the measurement on the final photon of the triplet is {state}')
				photon_measure_result_d1 = measure_photon(state, "z")

				if repeater_transmission_loss2:
					d1_aux_measurements[l1photon] = None
				else:
					d1_aux_measurements[l1photon] = photon_measure_result_d1[1]

				# print(f'The result of measurement of the final photon of the branch {photon_measure_result_d1[1]}')
				# Shrink the basis back to 3 qubits, since the photon state doesn't matter anymore
				# print(f'Discard photon basis might produce problems...')
				state = discard_photon_basis(photon_measure_result_d1, "z")
				# print(f'Debug 5: I predict that here the electron and nuclear are both detached. {state}')

			elif first_l1_photon.shape[0] == 8:
				d1_aux_measurements[l1photon] = None
				state = first_l1_photon
				# print(f'Debug 5: I predict that here the electron and nuclear are NOT detached. {state}')

			else:
				raise NotImplementedError("Your scatter state output does not have the correct shape")

			finalbranchstate = Qobj(np.copy(state), dims=[[2,2,2],[1,1,1]])

		else:

			# print(f'To complete the analysis of this branch, we are now trying to complete the triplet with a l1 photon')
			first_l1_photon = scatter(state, coeffs, nuclear_dephasing_thetas)

			if not dont_include_loss:
				repeater_transmission_loss2 = random.choices([True, False], [prrtl, 1 - prrtl])[0]
			else:
				repeater_transmission_loss2 = False
			# print(f'Consider implementing here a sanity check, that the two states have indeed been swapped')

			if first_l1_photon.shape[0] == 32:
				state = tensor(H_for_swap, qeye(2), qeye(2), qeye(2), qeye(2)) * first_l1_photon
				spin_measure_result = measure_spin(state)
				state = correct_spin_measurement(spin_measure_result[0], spin_measure_result[1])

				# Time to measure the l1 photon.
				# print(f'The input to the measurement on the final photon of the triplet is {state}')
				photon_measure_result_d1 = measure_photon(state, "z")

				if repeater_transmission_loss2:
					d1_aux_measurements[l1photon] = None
				else:
					d1_aux_measurements[l1photon] = photon_measure_result_d1[1]

				# print(f'The result of measurement of the final photon of the branch {photon_measure_result_d1[1]}')
				# Shrink the basis back to 3 qubits, since the photon state doesn't matter anymore
				# print(f'Discard photon basis might produce problems...')
				state = discard_photon_basis(photon_measure_result_d1, "z")
			# print(f'Debug 5: I predict that here the electron and nuclear are both detached. {state}')

			elif first_l1_photon.shape[0] == 16:
				d1_aux_measurements[l1photon] = None
				state = first_l1_photon
			# print(f'Debug 5: I predict that here the electron and nuclear are NOT detached. {state}')

			else:
				raise NotImplementedError("Your scatter state output does not have the correct shape")

			finalbranchstate = Qobj(np.copy(state), dims=[[2, 2, 2, 2], [1, 1, 1, 1]])

	"""
	Complete tree has been generated. We now remove electron and nuclear spins and perform majority voting with the measurements.
	"""
	debugging = np.zeros(4)

	###### Checking indirect probability for decode branch
	debugflag3 = 0
	debugflag4 = 0

	# print(f'Decode d3: {d3_decode_measurements}')
	# print(f'Decode d2: {d2_decode_measurements}')
	for l2decph in range(branching_vector[1]):
		if np.isnan(d2_decode_measurements[l2decph]):
			debugflag3 += 1
			if not np.all(np.isnan(d3_decode_measurements[l2decph])):
				debugflag4 += 1

	debugging[2] = np.copy(debugflag3)
	debugging[3] = np.copy(debugflag4)


	###### Checking indirect probability for aux branch
	debugflag1 = False
	debugflag2 = False

	# print(f'Aux d2: {d2_aux_measurements}')
	# print(f'Aux d3: {d3_aux_measurements}')

	if decode_marker == 0:
		aux_marker = 1
	else:
		aux_marker = 0

	if not decode_photon_found:
		debugging[0] = debugflag1
		debugging[1] = debugflag2
		return finalbranchstate, None, None, debugging
	# if not decode_photon_found:
	# 	for l2pho in range(branching_vector[1]):
	# 		if not np.isnan(d2_aux_measurements[aux_marker][l2pho]) and d3_aux_measurements[aux_marker][l2pho][np.invert(np.isnan(d3_aux_measurements[aux_marker][l2pho]))].shape[0] == d3_aux_measurements[aux_marker][l2pho].shape[0]:
	# 			debugflag2 = True

	# else:
	if np.isnan(np.delete(d1_aux_measurements, decode_marker)):
		debugflag1 = True
		for l2pho in range(branching_vector[1]):
			# print(np.isnan(d2_aux_measurements[aux_marker][l2pho]))
			if not np.isnan(d2_aux_measurements[aux_marker][l2pho]) and d3_aux_measurements[aux_marker][l2pho][np.invert(np.isnan(d3_aux_measurements[aux_marker][l2pho]))].shape[0] == d3_aux_measurements[aux_marker][l2pho].shape[0]:
				debugflag2 = True

	debugging[0] = debugflag1
	debugging[1] = debugflag2












	# print(f'Aux branch results: -------------------------------------')
	# print(f'level 1 aux photons: {d1_aux_measurements}')
	# print(f'level 2: {d2_aux_measurements}')
	# print(f'level 3: {d3_aux_measurements}')
	#
	# print(f'Decode branch results: -------------------------------------')
	# print(f'level 2: {d2_decode_measurements}')
	# print(f'level 3: {d3_decode_measurements}')
	#
	# print(f'the decoding branch is the {decode_marker + 1} branch')

	# print(f'Aux branch results: -------------------------------------')
	# print(f'level 1: {np.delete(d1_aux_measurements, decode_marker)}')
	# print(f'level 2: {np.delete(d2_aux_measurements, decode_marker, axis=0)}')
	# print(f'level 3: {np.delete(d3_aux_measurements, decode_marker, axis=0)}')
	#
	# print(f'Decode branch results: -------------------------------------')
	# print(f'level 2: {d2_decode_measurements}')
	# print(f'level 3: {d3_decode_measurements}')

	d1_aux_measurements = np.delete(d1_aux_measurements,decode_marker)
	d2_aux_measurements = np.delete(d2_aux_measurements,decode_marker,axis=0)
	d3_aux_measurements = np.delete(d3_aux_measurements,decode_marker,axis=0)


	# print(f'We have generated all branches and measured all auxiliary branches. The final l1 photon measurement results are {d1_aux_measurements}')
	# print(f'[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[ We have reached the end of the algorithm. ')
	# en_detached_checker(finalbranchstate)
	finalbranchstate = measure_spin(finalbranchstate)[0]
	# print(f'after')
	# en_detached_checker(finalbranchstate)
	finalbranchstate = measure(finalbranchstate, [ket2dm((basis(2, 0) + basis(2, 1)).unit()), ket2dm((basis(2, 0) - basis(2, 1)).unit())], targets=[2])[1]
	# print(f'after 2')
	# en_detached_checker(finalbranchstate)

	# We didn't find a decoding photon, so the decoding automatically fails.

	final_state1 = discard_electron_nuclear_spins(finalbranchstate)
	# print(f'After shrinking to a two qubit basis, our final state looks like: {final_state1}')

	# print(f'Now we must perform majority voting on these measurement outcomes.')

	# print(f'Our auxiliary results are {d2_aux_measurements}, {d3_aux_measurements}')
	# print(f'Our decoding results are {d3_decode_measurements}')
	# print(f'Our auxiliary level 1 measurements are {(d1_aux_measurements - 0.5) * -2}')

	# raise NotImplementedError("Ping")

	l1photon_results = majority_vote_auxiliary(d3_aux_measurements, d2_aux_measurements, d1_aux_measurements)
	l2photon_results = majority_vote_decode(d3_decode_measurements, d2_decode_measurements)

	# print(f'Our level 2 photons measurement array for auxiliary branches looks like {(d2_aux_measurements - 0.5) * -2}')
	# print(f'Our level 3 photons measurement array for auxiliary branches looks like {(d3_aux_measurements - 0.5) * -2}')
	# print(f'As for the decoding branch, we have the following results for level 2: {(d2_decode_measurements - 0.5) * -2}')
	# print(f'And we have the following results for level 3: {(d3_decode_measurements - 0.5) * -2}')
	# print(f'After voting, we get the following states for level 1 aux photons {l1photon_results} and level 2 decoding photons {l2photon_results}')

	# d3_aux_measurements[2][0][0] = 0

	if dont_include_error and dont_include_loss:
		ideal_case_checker(d2_aux_measurements, d3_aux_measurements, d1_aux_measurements, d2_decode_measurements, d3_decode_measurements)

	# print(f'We have the measurements at the end {l1photon_results, l2photon_results}')
	return final_state1, l1photon_results, l2photon_results, debugging							# Output measurement results are 0 and 1, not -1 and 1.


def encode_decode(m_state, l1photonresults, l2photonresults, msg_qubit, coefficients):
	"""
	Second half of the entire process. Taking the majority voted l1 auxiliary and l2 decoding branch measurements, function will:
	- Construct the suggested two qubit state from the measurements |decoding photon>|top qubit>
	- Encode an input message qubit with the "tree", and correct for Bell measurement
	- Decode the message qubit and calculate fidelity of the entire process.

	"""
	def construct_2q_state(mstate, l1m, l2m):
		"""
		Constructs 2 qubit state from l1 aux and l2 decode measurements and the result of gen_tree. basis given in
		|decoding photon> |top qubit>. The constructed state has been corrected in terms of l1 and l2 errors but
		has not been corrected for bell measurements yet.
		"""
		if l1m is None and l2m is None:
			print(f'EXCEPTION CASE: NO DECODING PHOTON FOUND ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++')
			return None, False, np.array([False, False, 0, 0])

		print(f'level 1 aux measurements {l1m}')
		print(f'level 2 decode measurements {l2m}')
		debugging = np.zeros(4)
		debugging[0] = True

		if not np.isnan(np.array(l1m,dtype=float)[0]):
			debugging[1] = True

		for l2pho in range(len(l2m)):
			debugging[2] += 1
			if l2m[l2pho] is not None:
				debugging[3] += 1


		can_we_decode = True
		non_nan_arr1 = np.array(l1m)[np.invert(np.isnan(np.array(l1m, dtype=float)))]
		non_nan_arr2 = np.array(l2m)[np.invert(np.isnan(np.array(l2m, dtype=float)))]
		# print(f'Testing the decoding condition, l1 measurements are {l1m} and l2 are {l2m}')

		# if np.all(np.isnan(np.array(l1m, dtype=float))):
		if not len(non_nan_arr1) == len(l1m):
			can_we_decode = False
			constructed_2q_state = None

		elif not len(non_nan_arr2) == len(l2m):
			can_we_decode = False
			constructed_2q_state = None

		else:
			arr = np.array(mstate).T[0]
			arr1 = np.abs(arr)

			# mask1 = np.invert(np.isnan(np.array(l1m,dtype=float)))
			# non_nan_arr1 = np.array(l1m)[mask1]
			# mask2 = np.invert(np.isnan(np.array(l2m,dtype=float)))
			# non_nan_arr2 = np.array(l2m)[mask2]

			l1 = np.product(non_nan_arr1)
			l2 = np.product(non_nan_arr2)
			signs_arr = np.array([1, l1, l2, -l1*l2])

			constructed_2q_state = Qobj(np.multiply(arr1, signs_arr),dims=[[2,2],[1,1]])

		return constructed_2q_state, can_we_decode, debugging

	def fidelity(message_qubit, decoded_qubit):
		"""
		Function returns the fidelity of the tree cluster state protection on the message qubit.

		Inputs should be in the 1 qubit basis
		"""
		arr = np.array(decoded_qubit).T[0]
		alph = arr[0]
		bet = arr[1]

		arr1 = np.array(message_qubit).T[0]
		alpha = arr1[0]
		beta = arr1[1]

		return abs(np.conj(alpha) * alph + np.conj(beta) * bet) ** 2

	def selective_bell_measure_alt(state_to_measure, desired_outcome):
		"""
		Function measures state_to_measure with a specific bell measurement, given by desired_outcome
		"""

		bell_roulette_result = 1337
		while bell_roulette_result != desired_outcome:
			bell_roulette_result, measured_state = measure(state_to_measure, [B00, B01, B10, B11], targets=[1, 2])

		return measured_state

	def selective_bell_measure(state_to_measure, desired_outcome):
		"""
		Function measures state_to_measure with a specific bell measurement, given by desired_outcome
		"""
		if desired_outcome == 0:
			to_project_state = sigma_plus
		elif desired_outcome == 1:
			to_project_state = sigma_minus
		elif desired_outcome == 2:
			to_project_state = psi_plus
		elif desired_outcome == 3:
			to_project_state = psi_minus
		else:
			raise ValueError("your desired outcome has the wrong value")

		measured_state = tensor(qeye(2), ket2dm(to_project_state)) * state_to_measure*2

		return measured_state

	error_single = coefficients[8]

	sigma_plus = bell_state(state='00')
	sigma_minus = bell_state(state='01')
	psi_plus = bell_state(state='10')
	psi_minus = bell_state(state='11')

	B00 = ket2dm(sigma_plus)
	B01 = ket2dm(sigma_minus)
	B10 = ket2dm(psi_plus)
	B11 = ket2dm(psi_minus)


	state_to_correct, success_bool, debuggingarr = construct_2q_state(m_state, l1photonresults, l2photonresults)
	if not success_bool:
		return None, debuggingarr

	arr_2q = np.array(state_to_correct).T[0]
	a, b, c, d = np.sign(arr_2q)

	total_state = tensor(state_to_correct, msg_qubit)
	selected_bell_result = measure(total_state, [B00, B01, B10, B11], targets=[1, 2])[0]

	if not abs(a) == 1 and abs(b) == 1 and abs(c) == 1 and abs(d) == 1:
		raise ValueError("Your checking mechanisms are not of unit value!")

	if selected_bell_result == 0:
		if a == -1:
			arr_2q = np.multiply(arr_2q, np.array([-1, 1, 1, 1]))
		if b == -1:
			arr_2q = np.multiply(arr_2q, np.array([1, -1, 1, 1]))
		if c == -1:
			arr_2q = np.multiply(arr_2q, np.array([1, 1, -1, 1]))
		if d == 1:
			arr_2q = np.multiply(arr_2q, np.array([1, 1, 1, -1]))

	elif selected_bell_result == 1:
		if a == -1:
			arr_2q = np.multiply(arr_2q, np.array([-1, 1, 1, 1]))
		if b == 1:
			arr_2q = np.multiply(arr_2q, np.array([1, -1, 1, 1]))
		if c == -1:
			arr_2q = np.multiply(arr_2q, np.array([1, 1, -1, 1]))
		if d == -1:
			arr_2q = np.multiply(arr_2q, np.array([1, 1, 1, -1]))

	elif selected_bell_result == 2:
		if a == -1:
			arr_2q = np.multiply(arr_2q, np.array([-1, 1, 1, 1]))
		if b == -1:
			arr_2q = np.multiply(arr_2q, np.array([1, -1, 1, 1]))
		if c == 1:
			arr_2q = np.multiply(arr_2q, np.array([1, 1, -1, 1]))
		if d == -1:
			arr_2q = np.multiply(arr_2q, np.array([1, 1, 1, -1]))

	elif selected_bell_result == 3:
		if a == -1:
			arr_2q = np.multiply(arr_2q, np.array([-1, 1, 1, 1]))
		if b == 1:
			arr_2q = np.multiply(arr_2q, np.array([1, -1, 1, 1]))
		if c == 1:
			arr_2q = np.multiply(arr_2q, np.array([1, 1, -1, 1]))
		if d == 1:
			arr_2q = np.multiply(arr_2q, np.array([1, 1, 1, -1]))

	else:
		raise NotImplementedError("Bell result can only be 0-3")

	corrected_state = Qobj(arr_2q, dims=[[2,2],[1,1]])
	total_state = tensor(corrected_state, msg_qubit)

	corrected_encoded_state = selective_bell_measure_alt(total_state, selected_bell_result)

	corrected_encoded_state = tensor(errored_H(draw_errvals(theta_sd=error_single, zero_error=True)), qeye(2), qeye(2)) * corrected_encoded_state

	arr2 = np.array(corrected_encoded_state).T[0]
	arr_firsthalf = arr2[:4]
	arr_secondhalf = arr2[4:]

	if np.sum(abs(arr_firsthalf)) == 0:
		alph = 0
	else:
		alph = arr_firsthalf[np.nonzero(arr_firsthalf)[0][0]]

	if np.sum(abs(arr_secondhalf)) == 0:
		bet = 0
	else:
		bet = arr_secondhalf[np.nonzero(arr_secondhalf)[0][0]]

	decoded_state = (alph * basis(2, 0) + bet * basis(2, 1)) * np.sqrt(2)

	return fidelity(msg_qubit, decoded_state), debuggingarr

def gate_coefficients(system_constants, noloss=False):
	"""
	Returns values for the reflection coefficients based on the formula to work them out.

	Parameters:
	-----------
	system_constants: list
		list of coefficients in the following order:
		omega_d / gamma :
		delta1 / gamma :
		cooperativity :
		kappa / gamma :
		kappa alpha / kappa : the ratio of kappa alpha w.r.t kappa
		Delta / gamma :

	Returns:
	--------
	r0: complex float
	r1: complex float
	l0: complex float
	l1: complex float
	"""

	if noloss:
		return -1,1,0,0

	a,b,c,d,e,bigd = system_constants

	d0 = b - bigd

	r_zero = 1-(e/(-1j*a/d + 1/2 + c/(-1j*(a+d0) + 1/2)))
	r_one = 1-(e/(-1j*a/d + 1/2 + c/(-1j*(a+b) + 1/2)))

	l_zero = (np.sqrt(1-e)*np.sqrt(e))/(-1j*a/d + 1/2 + c/(-1j*(a+d0) + 1/2))
	l_one = (np.sqrt(1-e)*np.sqrt(e))/(-1j*a/d + 1/2 + c/(-1j*(a+b) + 1/2))

	return r_zero, r_one, l_zero, l_one

# If running multiprocessing, this has to match the multiprocessing thread.
dont_include_error = False
dont_include_loss = False

# Function Call
# r_0 = -0.9975801700615283-0.06663961543507524j
# r_1 = 0.9915262604647272-0.08250223808938412j

# r_0, r_1, l_0, l_1 = gate_coefficients()
# r_0, r_1 = -1, 1
# l_0, l_1 = 0, 0

# # Given in order of: omega_d/gamma, delta1/gamma, cooperativity, kappa/gamma, kappa alpha / kappa, Delta / gamma
# sysconsts = [119.0, -74.0, 100, 50, 0.95, 20]
#
# # Error categories split into three: Emitter error, Single Qubit Gate Errors, Multi Qubit Gate Errors, Nuclear dephasing error
# error_emitter = 0.1
# error_single = 0.1
# error_multi = 0.1
# n_dephasing_sd = 0.1
#
# # Additional gate parameters
# # if dont_include_error and dont_include_loss:
# # 	r_0, r_1, l_0, l_1 = -1, 1, 0, 0
# # else:
# # 	r_0, r_1, l_0, l_1 = gate_coefficients(sysconsts, dont_include_loss)
#
# r_0, r_1, l_0, l_1 = -1, 1, 0, 0
#
# # Scattering parameters
# prob_egtl = 0
# # prob_rrtl = 0.2
# cross_loss_rate = 0.1
#
# # Detection efficiency etad
# etad = 1.0
#
# # Coefficients are: r0, r1, l0, l1, pegtl, prrtl, eta (cross loss parameter) and error categories as above.
# coeffs = [r_0, r_1, l_0, l_1, prob_egtl, prob_rrtl, cross_loss_rate, error_emitter, error_single, error_multi,
# 		  n_dephasing_sd, etad]
# # print(f'These are our r0: {r_0} and r1: {r_1}')
#
# # Choice of message qubit
# message_qubit = (basis(2, 0) + basis(2, 1)).unit()
# # message_qubit = (np.sqrt(3/10)*basis(2,0) + np.sqrt(7/10)*basis(2,1))
# # message_qubit = (np.sqrt(1/10)*basis(2,0) + np.sqrt(9/10)*basis(2,1))
#
# # Choice of number of Monte Carlo Runs
# N = 10000
# print(f'With this choice of parameters our value for 1-mu is {etad * (1 - prob_egtl) * (np.abs(r_0)) * (1 - prob_rrtl)}')


# if __name__ == '__main__':
# 	processed_tree_state, l1measurements, l2measurements = generate_tree([2,3,2], coeffs)
# 	final_fid = encode_decode(processed_tree_state, l1measurements, l2measurements, message_qubit, coeffs)
# 	print(f'++++++++++++++++++++++++++++++++++++++++++++++++++Our final fidelity is {final_fid}')

def monte_carlo(N, coefficients, branching_vector, msg_qubit):
	fidelity_results = np.zeros(N)
	Nm = 0
	Ns = 0
	Ndm = 0
	Nds = 0

	Nc = 0
	Na = 0
	Ndc = 0
	Nda = 0

	for i in range(N):
		print(f'-------------------------------------------------------------------------------------------Run number {i}')
		processed_tree_state, l1measurements, l2measurements, debugflags = generate_tree(branching_vector, coefficients)
		fidelity_results[i], debugarr = encode_decode(processed_tree_state, l1measurements, l2measurements, msg_qubit, coefficients)
		print(f'We got the result {fidelity_results[i]}')

		if debugarr[0]:
			Nc += 1
		if debugarr[1]:
			Na += 1

		Ndc += debugarr[2]
		Nda += debugarr[3]

		if debugflags[0]:
			Nm += 1
		if debugflags[1]:
			Ns += 1

		Ndm += debugflags[2]
		Nds += debugflags[3]
		print(f'--------------------------------------------------------------------------------------------------------------------------------------------- Nm {Nm} Ns {Ns}')
	# print(f'Fidelity results list looks like {fidelity_results}')

	mask = np.invert(np.isnan(fidelity_results))
	nonnan = fidelity_results[mask]
	# print(f'The shapes of the fidelity results array and the array with nans removed: {fidelity_results.shape[0]}, {nonnan.shape[0]}')

	eps0 = 1-( (1 - prob_egtl) * (np.abs(r_0)) * (1 - prob_rrtl))
	print(f'The theoretical value for successful indirect aux Z measurement is given by: {1-(1-(1-eps0)*(1-eps0)**2)**3}')
	print(f'DEBUGGING[][][[][][][][[][][][][] The probability for successful indirect aux Z measurement is given by: {Ns/Nm}')

	print(
		f'The theoretical value for successful indirect decode Z measurement is given by: {1 - (eps0) ** 2}')
	print(
		f'DEBUGGING[][][[][][][][[][][][][] The probability for successful indirect decode Z measurement is given by: {Nds / Ndm}')

	print(f'The theoretical value for successful Z measurement on an aux level 1 photon is given by: {1-eps0 + eps0*(1-(1-(1-eps0)*(1-eps0)**2)**3)}')
	print(f'We find that our simulation gives the probability for such a successful Z measurement on l1 aux photon to be {Na/Nc}')

	print(
		f'The theoretical value for successful Z measurement on a decoding level 2 photon is given by: {1 - eps0 + eps0 * (1 - (eps0) ** 2)}')
	print(
		f'We find that our simulation gives the probability for such a successful Z measurement on l2 decode photon to be {Nda / Ndc}')

	R1 = 1-(1-(1-eps0)*(1-eps0)**2)**3
	R2 = 1 - (eps0) ** 2
	print(f'Overall we predict a decoding probability of {((1-eps0+eps0*R1)**2 - (eps0*R1)**2)*(1-eps0+eps0*R2)**3}')
	print(f'What we find from the simulation is a decoding probability of {nonnan.shape[0] / fidelity_results.shape[0]}')

	return np.mean(nonnan), np.std(nonnan), nonnan.shape[0] / fidelity_results.shape[0]

if __name__ == '__main__':
	time1 = time.time()
	simulation_result = monte_carlo(N, coeffs, [2,3,2], message_qubit)
	print(f'After {N} runs, we have the following average fidelity, standard deviation and successful decoding probability: {simulation_result}')
	print(f'This run took {(-time1+time.time())/60} minutes or {(-time1+time.time())} seconds to run for {N} times')
# # 	np.savetxt("result.csv", simulation_result, delimiter=",")

# Profiling
# cProfile.run('monte_carlo(N, coeffs, [5,5,5], message_qubit)')
# cProfile.run('encode_process_decode(processed_tree_state, message_qubit, l1measurements, l2measurements)')