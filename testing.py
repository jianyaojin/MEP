import numpy as np
from sympy import *
import random
from collections import Counter as Majoritycount


# print(f'test {random.choices([1,2,3,4],[10,20,10,10])}')

# expression = 5*Symbol("R")*Symbol("B")*Symbol("C") + 6*Symbol("C")*Symbol("G")*Symbol("A") + Symbol("G")*Symbol("R")
# print(expression)
# print(list(expression.free_symbols))
# symbs = list(expression.free_symbols)
# basis_list = ["A", "B", "C", "D", "F", "G", "E", "L"]
# basis_list1 = [symbols(v) for v in basis_list]
# print(list(set(symbs).intersection(basis_list1)))
# print(list(set(symbs).intersection(basis_list1))[0].subs({list(set(symbs).intersection(basis_list1))[0]:1}))
#
# print(f'bablalbalblablalblalbalblalbla')
# twenty = expression.coeff(Symbol("R"),1)
# print(twenty)
# print(twenty.subs({Symbol("G"):1}))
# # vec = Matrix([[6*Symbol("A")*Symbol("E")*Symbol("")]])

# x = Symbol("a")*Symbol("b")
# print(x)
# print(x.subs({Symbol("a"):1, Symbol("b"):2, Symbol("c"):3}))

# H = 1/np.sqrt(2)*np.array([symbols("H(1:5)")]).reshape(2,2)
# H_basis = np.kron(H,np.identity(2))
#
# error = cos(Symbol("T")/2)*np.identity(4) - 1j *sin(Symbol("T")/2)*Matrix([[0,0,1,0],[0,1,0,0],[1,0,0,0],[0,0,0,1]])
# CcNOTe = Matrix([[1,0,0,0],[0,0,0,1],[0,0,1,0],[0,1,0,0]])
# errored_exp = error*CcNOTe
# print(H_basis*errored_exp*H_basis)

# a = np.identity(4)
# b = np.array([[1,2,3,4]]).T
# print(a@b)

# a = np.array([[1,0,1,0],[0,1,0,1],[1,0,-1,0],[0,1,0,-1]])
# b = np.array([[1,0,0,0],[0,0,0,1],[0,0,1,0],[0,1,0,0]])
# c = np.array([1,1,1,1]).T
# print(0.5*0.5*a@b@a@c)

# a = np.array([1,2,3,4,5,6,7])
# print(a[:4])
# print(np.dot(a[:4],a[:4]))
# print(np.sum(np.square(a[:4])))
# print(abs(1+1j))

# a = [None, None, None, 1, 1, 0]
# b = [i[1] for i in Majoritycount(a).most_common()]
#
# print(a)
# print(Majoritycount(a).most_common())
# print(b)

# a = np.array([[0,0,1,1,0,0,2,2]])
# print(np.nonzero(a))
# a = [1,2,4,6]
# for i in enumerate(a):
#     print(i)

# a = np.zeros(4)
# a[2] = 1+1j
# print(a)

# a = [1, -1, 1]
# b = [0 if x ==1 else 1 for x in a]
# print(b)

# M = np.array([[1,0,0,0,0,0,0,0],[0,0,0,0,0,1,0,0],[0,0,1,0,0,0,0,0],[0,0,0,0,0,0,0,1],[0,0,0,0,1,0,0,0,],[0,1,0,0,0,0,0,0],[0,0,0,0,0,0,1,0],[0,0,0,1,0,0,0,0]])
# b = np.array([1,2,3,4,5,6,7,8]).T
# print(M@b)

# a = np.array([0,1,1,1,1,1])
# b = np.array([1,1,1,1,1,-1])
# c = np.array([1,1,-1,1,-1,1])
# d = np.zeros(6)
# print(np.sum(d))
# print(np.nonzero(a)[0][0])

# a = np.array([[-0.35355339], [-0.35355339], [ 0.        ], [ 0.        ], [ 0.35355339], [ 0.35355339], [ 0.        ], [ 0.        ], [ 0.35355339], [ 0.35355339], [ 0.        ], [ 0.        ], [-0.35355339], [-0.35355339], [ 0.        ], [ 0.        ]]).T[0]
# b = np.array([[-0.33897488-0.02866852j], [-0.36257889-0.02171871j], [-0.03051969-0.02224468j], [ 0.02997993+0.00373128j], [-0.33897488-0.02866852j], [-0.36257889-0.02171871j], [-0.03051969-0.02224468j], [ 0.02997993+0.00373128j], [ 0.33897488+0.02866852j], [ 0.36257889+0.02171871j], [ 0.03051969+0.02224468j], [-0.02997993-0.00373128j], [ 0.33897488+0.02866852j], [ 0.36257889+0.02171871j], [ 0.03051969+0.02224468j], [-0.02997993-0.00373128j]]).T[0]
# a=b
# print(a[:8], a[8:])
# print(a[:8] - a[8:])
# # assert np.sum(a[:8] - a[8:]) == 0
# assert np.sum(abs(a[:8])-abs(a[8:])) == 0

# a = np.ones(6)
# b = np.array([1,1,2,2,1,2])
# print(np.multiply(a,b))

# a = [0,0,0,1,0]
# print((np.array(a)-0.5)*-2)

def majority_vote_auxiliary(l3photon_measurements, l2photon_measurements):
    """
    Function performs majority voting on l1 photon of each auxiliary branch (head photon of each auxiliary branch).
    Contributing factors include the parity of the l3 photon measurements, and the x measurement outcome of the
    l2 photon measurements.

    l3 photon measurements should be in format 0 or 1. This function handles converting that to the +1, -1 format.

    Function returns a list of majority voted l1 measurements
    """

    def switch_notation(array):
        """
        Input measurements are written in the 0,1 basis, this function switches it to the 1,-1 basis.
        """
        return (array - 0.5) * -2

    def give_l3_parity(l1p, l2p, l3p_measurements):
        measurements_arr = l3p_measurements[l1p][l2p]
        if np.all(np.isnan(measurements_arr)):
            return np.product(switch_notation(measurements_arr))
        else:
            mask = np.invert(np.isnan(measurements_arr))
            non_nan = measurements_arr[mask]
            return np.product(switch_notation(non_nan))

    nl1 = l3photon_measurements.shape[0]					# Number of level 1 photons
    nl2 = l2photon_measurements.shape[1]					# Number of level 2 photons
    l1majority_vote_result = []

    # print(f'Inputs are {l3photon_measurements, l2photon_measurements}')
    # print(f'we"re gonna loop through {l1photons_number} level 1 photons and {l2photons_number} level 2 photons')
    for l1ph in range(nl1):
        l1photon_votes = []
        for l2ph in range(nl2):
            # l3parity = np.product(switch_notation(l3photon_measurements[l1ph][l2ph]))
            l3parity = give_l3_parity(l1ph,l2ph,l3photon_measurements)
            if l3parity == 1.0:
                if l2photon_measurements[l1ph][l2ph] == 0:
                    l1photon_votes.append(1)
                elif l2photon_measurements[l1ph][l2ph] == 1:
                    l1photon_votes.append(-1)
                else:
                    # In the case where only the l2 photon is lost
                    l1photon_votes.append(1)
            elif l3parity == -1.0:
                if l2photon_measurements[l1ph][l2ph] == 0:
                    l1photon_votes.append(-1)
                elif l2photon_measurements[l1ph][l2ph] == 1:
                    l1photon_votes.append(1)
                else:
                    # In the case where only the l2 photon is lost
                    l1photon_votes.append(-1)
            else:
                # Parity is None
                if np.isnan(l2photon_measurements[l1ph][l2ph]):
                    l1photon_votes.append(None)
                else:
                    l1photon_votes.append(switch_notation(l2photon_measurements[l1ph][l2ph]))

        # 3 possibilities: All None, mostly None with clear majority vote winner, mostly None with majority vote tie
        nan_array = np.array(l1photon_votes, dtype=float)
        if np.all(np.isnan(nan_array)):
            l1majority_vote_result.append(None)
            continue

        mask = np.invert(np.isnan(nan_array))
        nan_excluded_votes_array = nan_array[mask]

        votes_list = Majoritycount(nan_excluded_votes_array).most_common()

        if len(votes_list) > 1 and np.all(np.array([x[1] for x in votes_list]) == votes_list[0][1]):
            print(f'Random condition met')
        # if len(l1photon_votes) == len(votes_list):
            # print("I'm just going to select a random outcome, since we have a tie in the vote. Should this be possible?")
            l1majority_vote_result.append(random.choices([1, -1], [0.5, 0.5])[0])
            continue

        # Count majority votes
        if votes_list[0][0] == 1:
            l1majority_vote_result.append(1)
        elif votes_list[0][0] == -1:
            l1majority_vote_result.append(-1)
        else:
            print(f'This shouldnt be possible.')
            # if votes_list[-1][0] is None:
            # 	l1majority_vote_result.append(None)
            # else:
            # 	if votes_list[-1][0] == 1:
            # 		l1majority_vote_result.append(1)
            # 	elif votes_list[-1][0] == -1:
            # 		l1majority_vote_result.append(-1)

    # print(f'The final result of all that voting is: {l1majority_vote_result}')
    return l1majority_vote_result


# a = np.array([[[1, 1],[1, 0], [1, 1]]], dtype=float)
a = np.array([[[0, 1, None, None],[None, None, None, 0], [1, None, None, 1]]], dtype=float)
b = np.array([[None, 1, None]], dtype=float)
print(majority_vote_auxiliary(a,b))

# def majority_vote_decode(depth3_decode_measurements):
#     """
#     Function performs majority voting on decoding branch.
#     """
#
#     def switch_notation(array):
#         """
#         Input measurements are written in the 0,1 basis, this function switches it to the 1,-1 basis.
#         """
#         return (array - 0.5) * -2
#
#     print(f'Decoding branch needs to majority vote using these measurements: {depth3_decode_measurements}')
#
#     l2photons_number = depth3_decode_measurements.shape[0]
#     l2majority_vote_result = []
#     print(f'Number of photons {l2photons_number}')
#     for l2i in range(l2photons_number):
#         print(f'l2photon {l2i}')
#         print(f'Did the edit go through: {depth3_decode_measurements[l2i]}')
#         print(f'Are the measurements for this l2 photon all nan? {np.all(np.isnan(depth3_decode_measurements[l2i]))}')
#         if np.all(np.isnan(depth3_decode_measurements[l2i])):
#             l2majority_vote_result.append(None)
#             continue
#
#         mask1 = np.invert(np.isnan(depth3_decode_measurements[l2i]))
#         non_nan_results_only = switch_notation(depth3_decode_measurements[l2i][mask1])
#         print(f'Dug out the only votes: {non_nan_results_only}')
#         votes_list = Majoritycount(non_nan_results_only).most_common()
#
#         # l3photon_votes = switch_notation(depth3_decode_measurements[l2i])
#         # votes_list = Majoritycount(l3photon_votes).most_common()
#
#         print(f'List of votes{votes_list}')
#         # print(f'Second row: {[x[1] for x in votes_list]}')
#         if len(votes_list) > 1 and np.all(np.array([x[1] for x in votes_list]) == votes_list[0][1]):
#             # if len(non_nan_results_only) == len(votes_list):
#             print("I'm just going to select a random outcome, since we have a tie in the vote. Should this be possible?")
#             l2majority_vote_result.append(random.choices([1, -1], [0.5, 0.5])[0])
#             continue
#
#         # Count majority votes
#         if votes_list[0][0] == 1:
#             l2majority_vote_result.append(1)
#         elif votes_list[0][0] == -1:
#             l2majority_vote_result.append(-1)
#         else:
#             print(f'This shouldnt be possible.')
#         # if votes_list[-1][0] is None:
#         # 	l2majority_vote_result.append(None)
#         # else:
#         # 	if votes_list[-1][0] == 1:
#         # 		l2majority_vote_result.append(1)
#         # 	elif votes_list[-1][0] == -1:
#         # 		l2majority_vote_result.append(-1)
#
#     return l2majority_vote_result
#
# a = np.array([[None,1,0,None], [None, 1,1,0], [None, None,None,None]], dtype=float)
# print(majority_vote_decode(a))