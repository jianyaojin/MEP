#!/bin/bash
#Set job requirements
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=128
#SBATCH --partition=thin
#SBATCH --time=00:14:00
 
#Loading modules
module load 2022
module load Python/3.10.4-GCCcore-11.3.0-bare
module load SciPy-bundle/2022.05-foss-2022a

#Create output directory on scratch
mkdir "$TMPDIR"/output_dir
 
#Execute a Python program located in $HOME, that takes an input file and output directory as arguments.
python $HOME/Multiprocessing_Monte_Carlo.py "$TMPDIR"/output_dir
 
#Copy output directory from scratch to home
cp -r "$TMPDIR"/output_dir $HOME